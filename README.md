# ポスタン for iPad
ポスタンを使えば、紙にペンで書く様に、あたたかみのあるポスターを誰でも簡単に作成することができます。  
写真を取り込んでさまざまな形に切り抜いたり、図形を追加したり、文字入れしたポスターを作成し、印刷することができます。

## ビルド/実行
Xcode 13.x 以上でプロジェクトを開き、ビルド/実行してください。

## 最小インストールバージョン
iPadOS 14.0 以上の端末にインストール可能です。

## OSS
ポスタンは次の OSS を使用しています。  
・SwiftyJSON (https://cocoapods.org/pods/SwiftyJSON)  
・SwiftyOnboard (https://cocoapods.org/pods/SwiftyOnboard)  
・SwiftGifOrigin (https://cocoapods.org/pods/SwiftGifOrigin)  
・LicensePlist (https://github.com/mono0926/LicensePlist)

## プロジェクトセットアップ手順
1. CocoaPods インストール (https://cocoapods.org/)  
  `$ sudo gem update -n /usr/local/bin xcodeproj`  
  `$ sudo gem install -v 1.10.0 -n /usr/local/bin cocoapods`

2. CocoaPods セットアップ  
  `$ pod setup`

3. ポスタン依存ライブラリインストール  
  `$ cd (ポスタントップディレクトリ)`  
  `$ pod install`

4. プロジェクト起動  
CocoaPods により生成される "Postan.xcworkspace" を起動

## WalkThrough
新しい機能の紹介動画は gif 変換が必要です。  
・mp4 → gif 変換 (e.x. https://ezgif.com/video-to-gif)  
・Gif.bundle 内に "movie0.gif" のような命名規則で連番で配置して下さい  

## License
Copyright (c) 2021 The Postan Authors  
This software is released under the MIT License, see [LICENSE.txt](./LICENSE.txt)
