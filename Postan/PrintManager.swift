//
//  PrintManager.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/07/23.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

class PrintManager{
    
    static func printFunc(url: URL, _ controller: UIViewController?) -> Bool {
        let printController = UIPrintInteractionController.shared
        
        let printInfo = UIPrintInfo(dictionary:nil)
        printInfo.outputType = UIPrintInfo.OutputType.photo
        printInfo.jobName = "Print Job"
        printInfo.orientation = .portrait
        
        printController.printInfo = printInfo
        printController.printingItem = url as NSURL
        
        printController.present(animated: true, completionHandler: {[weak controller] (prCon, isSelected, error) in
            if isSelected == true && error != nil{
                print("Print error!")
                displayAlertMessage(title:Message.PrintError.info.title, message:Message.PrintError.info.message, controller)
            }else if isSelected == false{
                // ステップ数を減らすためコメントアウト
                //displayAlertMessage(title:Message.PrintCancel.info.title, message:Message.PrintCancel.info.message, controller)
            }else{
                displayAlertMessage(title:Message.PrintSuccess.info.title, message:Message.PrintSuccess.info.message, controller)
            }
        })
        
        return true
    }
    
    static private func displayAlertMessage(title: String, message msg: String, _ parent: UIViewController?){
        let alert: UIAlertController = UIAlertController(title: title, message: msg, preferredStyle:  UIAlertController.Style.alert)
        
        let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            print("OK")
        })
        alert.addAction(defaultAction)
        parent!.present(alert, animated: true, completion: nil)
    }
    
}
