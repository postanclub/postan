//
//  CreateFolderViewController.swift
//  Postan
//
//  Created by Takashi Aizawa on 2021/06/11.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

class CreateFolderViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var folderNameInputField: UITextField!
    @IBOutlet weak var createFolderButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    private var m_FolderDataSets: [FolderDataSet]?
    
    public var delegate: ReturnViewContorller!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view
        self.view.layer.borderWidth = 2
        self.view.layer.borderColor = UIColor.gray.cgColor
        self.view.layer.cornerRadius = 13
        self.view.layer.masksToBounds = true
        
        createFolderButton.layer.cornerRadius = 5
        cancelButton.layer.cornerRadius = 5
        
        folderNameInputField.layer.borderWidth = 2
        folderNameInputField.layer.borderColor = UIColor.gray.cgColor
        folderNameInputField.layer.cornerRadius = 5
        folderNameInputField.delegate = self
        
        // 空値のフォルダ名を許容しないように disable とする
        createFolderButton.isEnabled = false
        createFolderButton.layer.backgroundColor = UIColor.darkGray.cgColor
        
        m_FolderDataSets = FolderInfoSrializeObject.share().loadFolderDateSet()
    }
    
    @IBAction func onCreateButtonClicked(_ sender: Any) {
        var duplicatePosterName = false
        
        // ポスター名の重複チェック
        if let folderDataSets = m_FolderDataSets {
            for folder in folderDataSets {
                if folder.displayName == folderNameInputField.text {
                    duplicatePosterName = true
                    // 重複エラーポップアップを表示
                    let alert = UIAlertController(title: Message.DuplicatePoster.info.title, message: Message.DuplicatePoster.info.message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    break
                }
            }
        }
        
        if !duplicatePosterName && delegate != nil {
            delegate.executeReturnCallback(param: folderNameInputField.text as Any)
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func onCancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            // テキストフィールドの文字が更新されたら変更する
            if textField.text!.isEmpty {
                self.createFolderButton.isEnabled = false
                self.createFolderButton.layer.backgroundColor = UIColor.darkGray.cgColor
            }
            else {
                self.createFolderButton.isEnabled = true
                self.createFolderButton.layer.backgroundColor = UIColor.postanPink.cgColor
            }
        }
        
        return !string.contain(pattern: "[.:¥/*?\"<>| ]")
    }
}

extension String {
    func contain(pattern: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options()) else {
            return false
        }
        return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range: NSMakeRange(0, self.count)) != nil
    }
}
