//
//  UpdateInfo.swift
//  Postan
//
//  Created by Takashi Aizawa on 2020/08/10.
//  Copyright © 2020 beginner. All rights reserved.
//

import Foundation

protocol ViewUpdateDelegate {
    func returnedFromOtherViewController( _ param: Any? )
}

class UpdateInfo:ViewUpdateDelegate {
    func returnedFromOtherViewController(_ param: Any?) {
        let client = delegate as? ViewUpdateDelegate
        client?.returnedFromOtherViewController(param)
    }
    
    var delegate: Any?
    var isNew: Bool = false
    var folderPath: String?
    
}

protocol ReturnViewContorller {
    func executeReturnCallback(param obj: Any?)
}
