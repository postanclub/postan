//
//  FolderInfoSrialize.swift
//  Postan
//
//  Created by Takashi Aizawa on 2021/06/13.
//  Copyright © 2021 beginner. All rights reserved.
//

import Foundation

class FolderInfoSrializeObject {
    static var instance:FolderInfoSrializeObject! = nil
    
    static func share() -> FolderInfoSrializeObject {
        if instance == nil {
            instance = FolderInfoSrializeObject()
        }
        return instance
    }
    
    func saveFolderDataSet( dataSet list: [FolderDataSet]) {
        let serializeObj = SerializeObject(folders: list)
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let encoded = try! encoder.encode(serializeObj)
        print(encoded)
        UserDefaults.standard.set(encoded, forKey: UserDefaults.Keys.folderKey)
    }
    
    func saveFolderDataSet( dataSet list: [FolderDataSet], fileName path: String) {
        let serializeObj = SerializeObject(folders: list)
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let encoded = try! encoder.encode(serializeObj)
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        var settingFilePath = paths[0].path
        settingFilePath = settingFilePath + "/" + path
        
        do {
            let path = URL(string: settingFilePath)!
            try encoded.write(to: path, options: .atomic)
        } catch {
            fatalError("Can't write file (\(settingFilePath))")
        }
        print(encoded)
    }
    
    func loadFolderDateSet() -> [FolderDataSet]? {
        if let loadedData = UserDefaults().data(forKey: UserDefaults.Keys.folderKey) {
            let serializeObj = try? JSONDecoder().decode(SerializeObject.self, from: loadedData)
            return serializeObj?.loardObject()
        }
        return nil
    }
    
    func loadFolderDateSet(fileName path: String) -> [FolderDataSet]? {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        var settingFilePath = paths[0].path
        settingFilePath = settingFilePath + "/" + path
        
        guard let string = try? String(contentsOfFile: settingFilePath, encoding: .utf8) else {
            print("Can't read file (\(settingFilePath)")
            return nil
        }
        
        do {
            let data = try JSONDecoder().decode(SerializeObject.self, from: string.data(using: .utf8)!)
            return data.loardObject()
        } catch {
            print("Can't decode file (\(string)")
            return nil
        }
    }
}
