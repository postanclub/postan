//
//  MovePosterViewController.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2021/07/13.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

class MovePosterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var moveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    private static var HOME_FOLDER_NAME = "ホーム"
    
    private var m_FolderDataSets: [FolderDataSet]?
    private var m_tagetFolder: FolderDataSet?
    
    public var delegate: ReturnViewContorller!
    public var currentFolder: FolderDataSet!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layer.borderWidth = 2
        self.view.layer.borderColor = UIColor.gray.cgColor
        self.view.layer.cornerRadius = 13
        self.view.layer.masksToBounds = true
        
        moveButton.layer.cornerRadius = 5
        cancelButton.layer.cornerRadius = 5
        
        // 空値のフォルダ名を許容しないように disable とする
        moveButton.isEnabled = false
        moveButton.layer.backgroundColor = UIColor.darkGray.cgColor
        
        m_FolderDataSets = FolderInfoSrializeObject.share().loadFolderDateSet()
    }
    
    /**
     * 読込み直後のセル順序を入れ替えた後にセル数を返却します
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // 表示中の画面がフォルダ内かチェック
        if currentFolder != nil {
            if let folderDataSets = m_FolderDataSets {
                var index = 0
                for folder in folderDataSets {
                    if currentFolder != nil && currentFolder.compareFolder(folder) {
                        // フォルダ内で移動操作をおこなう場合、表示中のフォルダを移動先の対象外とする
                        m_FolderDataSets?.remove(at: index)
                        break
                    }
                    index += 1
                }
                if folderDataSets.count != m_FolderDataSets?.count {
                    // コレクションの先頭に表示中のフォルダを挿入する（ホームフォルダと置換するため）
                    m_FolderDataSets?.insert(currentFolder, at: 0)
                }
            }
        }
        
        return m_FolderDataSets?.count ?? 0
    }
    
    /**
     * セルの内容を返します
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "folderCell", for: indexPath)
        cell = cellSetting(cell)
        
        let folderData = m_FolderDataSets![indexPath.row]
        cell.textLabel?.text = folderData.displayName
        
        // 表示中の画面がフォルダ内の場合、ホームフォルダを設定
        if currentFolder != nil && currentFolder.compareFolder(m_FolderDataSets![indexPath.row]) {
            cell.imageView!.image = UIImage(named: "HomeFolderIcon")
            cell.textLabel?.text = MovePosterViewController.HOME_FOLDER_NAME
        }
        
        return cell
    }
    
    /**
     * セル選択時に実行される処理です
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        m_tagetFolder = m_FolderDataSets![indexPath.row]
        self.moveButton.isEnabled = true
        self.moveButton.layer.backgroundColor = UIColor.postanPink.cgColor
    }
    
    /**
     * フォルダ移動ポップアップ内のセル設定です
     */
    private func cellSetting(_ cell: UITableViewCell) -> UITableViewCell {
        // カラー設定
        let cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = .darkGray
        cell.selectedBackgroundView = cellSelectedBgView
        cell.textLabel?.textColor = .white
        cell.imageView!.tintColor = .postanPink
        
        // アイコン設定
        cell.imageView!.image = UIImage(named: "SubFolderIcon")
        
        return cell
    }
    
    @IBAction func onMoveButtonClicked(_ sender: UIButton) {
        if delegate != nil {
            delegate.executeReturnCallback(param: m_tagetFolder)
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func onCancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
