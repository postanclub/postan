//
//  ContentsManagememtViewController.swift
//  Postan
//
//  Created by Takashi Aizawa on 2020/07/25.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

class ContentsManagememtViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ViewUpdateDelegate,ProjectCollectionCellDelegate,CellMenuDelegate {
    
    private let kCellMenuWidth = 150
    private let kCellMenuHeight = 55
    private let kCellMenuHeightSpace = 20
    private let kCellWidthMin = 330
    private let kCellWidthMax = 400
    private let kOffsrt = 30
    private let kSpaceBetweenItems = 20
    private var canvasController: CanvasViewController!
    private var cellMenuController: CellMenuViewController!
    private var m_loardData: [DataSetBase]!
    private var m_ViewInfo: UpdateInfo = UpdateInfo()
    private var m_CurrentIndex: Int = 0
    private var m_isVertical = true
    private var m_isTouched = false
    private var createFolderController: CreateFolderViewController!
    private var m_createFolderName: String!
    private var m_parentFolder: FolderDataSet!
    private var m_topMode: Bool = true
    private var m_enableFolderManagement: Bool!
    private var movePosterController: MovePosterViewController!
    
    @IBOutlet weak var FolderPanel: UIView!
    @IBOutlet weak var thumbailBaseView: UIView!
    @IBOutlet weak var headerPanel: UIView!
    @IBOutlet weak var folderCloseButton: UIButton!
    @IBOutlet weak var createNewFolder: UIButton!
    
    @IBOutlet weak var m_collectionView: UICollectionView!
    @IBOutlet weak var folderNameLabel: UILabel!
    
    @IBOutlet weak var createNewButton: UIButton!
    
    @IBOutlet weak var collectionLead: NSLayoutConstraint!
    @IBOutlet weak var collectionTail: NSLayoutConstraint!
    
    @IBOutlet weak var versionBuildButton: UIButton!
    @IBOutlet weak var collectionViewBottom: NSLayoutConstraint!
    @IBOutlet weak var folderPanelHeight: NSLayoutConstraint!
    @IBOutlet weak var folderPanelTop: NSLayoutConstraint!
    
    @IBAction func pushVersionBuildButton(_ sender: Any) {
        m_isTouched.toggle()
        //「バージョン ↔︎ ビルド」番号をタップのたびに切り替えて表示します。
        if m_isTouched {
            let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
            versionBuildButton.setTitle("Build: \(build)", for: .normal)
        } else {
            let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            versionBuildButton.setTitle("Version: \(version)", for: .normal)
        }
    }
    
    @IBAction func onCreateNewProject(_ sender: Any) {
        createNewProject(canvasSize: CreateNewPosterController.defaultCanvasSize)
    }
    
    @IBAction func onCloseFolderPanel(_ sender: Any) {
        m_topMode = true
        m_parentFolder = nil
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       options: [.curveLinear],
                       animations: { [self] in
            setLayoutToHomeFolderMode()
        }, completion: nil)
        self.loadProjectData()
        self.updateView()
        m_collectionView.scroll(to: .top, animated: false)
    }
    
    @objc func onCreateNew() {  // [新規作成] 長押し
        let storyboard = UIStoryboard(name: "CreateNew", bundle: nil)
        if let createPosterController = storyboard.instantiateViewController(withIdentifier: "CreateNewPosterController") as? CreateNewPosterController {
            createPosterController.parentViewControllerInfo = m_ViewInfo
            createPosterController.modalPresentationStyle = .formSheet
            createPosterController.preferredContentSize = CGSize(width: 440, height: 280)
            present(createPosterController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onCreateFolderButtonClicked(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "ContentsManagment", bundle: nil)
        createFolderController = storyboard.instantiateViewController(withIdentifier: "CreateFolderController") as? CreateFolderViewController
        createFolderController.modalPresentationStyle = .popover // .pageSheet, .fullScreen, .overFullScreen, .formSheet, .popover
        createFolderController.preferredContentSize = CGSize(width: 500, height: 200)
        createFolderController.popoverPresentationController!.permittedArrowDirections = UIPopoverArrowDirection(rawValue:0)
        createFolderController.popoverPresentationController!.sourceView = self.view
        createFolderController.popoverPresentationController!.sourceRect = CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2, width: 0, height: 0)
        createFolderController.delegate = self
        present(createFolderController, animated: true, completion: nil)
    }
    
    private func createNewProject(canvasSize: CGSize) {
        let DataObject = DataManager.share()
        let projectName = DataObject.createNewProject()
        let storyboard: UIStoryboard = UIStoryboard(name: "CanvasEditor", bundle: nil)
        canvasController = storyboard.instantiateViewController(withIdentifier: "CanvasViewController") as? CanvasViewController
        canvasController.modalPresentationStyle = .fullScreen
        let subStrings = projectName.components(separatedBy: "/")
        let targetFolderName = subStrings[subStrings.count-1]
        
        print(DataManager.share().getPrintDataPath()!)
        canvasController.printDataUrl = DataManager.share().getPrintDataPath()!.appendingPathComponent(targetFolderName + ".jpg")
        canvasController.isNewCanvas = true
        canvasController.canvasSize = canvasSize
        print(projectName)
        let str:URL = URL(fileURLWithPath: projectName, isDirectory: true)
        print(str)
        canvasController.folderUrl = str
        m_ViewInfo.isNew = true
        m_ViewInfo.folderPath = projectName
        canvasController.parentViewControllerInfo = m_ViewInfo
        present(canvasController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ProjectCollectionViewCell", bundle: nil)
        m_collectionView!.register(nib, forCellWithReuseIdentifier: "PosterProjectCell")
        let nibFolder = UINib(nibName: "FolderCollectionViewCell", bundle: nil)
        m_collectionView!.register(nibFolder, forCellWithReuseIdentifier: "FolderProjectCell")
        
        self.loadProjectData()
        setupLayout(self.view.frame.size)
        
        m_ViewInfo.delegate = self
        
        // バージョンを表示します。
        let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        versionBuildButton.setTitle("Version: \(version)", for: .normal)
        
        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(onCreateNew))
        createNewButton.addGestureRecognizer(recognizer)
        
        // 設定アプリに応じたフォルダ管理機能の on/off 対応
        m_enableFolderManagement = getAppSettingWithFolderManagement()
        if m_enableFolderManagement == false {
            createNewFolder.isHidden = true
            m_topMode = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // iPad を水平で使用する場合、UIDevice から取得可能な orientation は、unknown となる
        // サムネイルサイズ設定時に不都合があるため UIWindowScene から取得する
        var statusBarOrientation: UIInterfaceOrientation? {
            get {
                guard let orientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation else {
                    return nil
                }
                return orientation
            }
        }
        
        // 縦持ち、横持ちの切り替え対応
        if (statusBarOrientation == .portrait || statusBarOrientation == .portraitUpsideDown) {
            m_isVertical = true
        } else {
            m_isVertical = false
        }
        
        // 設定アプリに応じたフォルダ管理機能の on/off 対応
        m_enableFolderManagement = getAppSettingWithFolderManagement()
        if m_enableFolderManagement {
            createNewFolder.isHidden = false
            if m_topMode {
                setLayoutToHomeFolderMode()
            } else {
                setLayoutToSubFolderMode()
            }
        } else {
            createNewFolder.isHidden = true
        }
        
        // アプリがフォアグラウンドへ移行するタイミングの通知を受信するオブザーバの設定
        NotificationCenter.default.addObserver(self, selector: #selector(viewWillEnterForeground(
                                                notification:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        stopIndicator()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        // UIDevice から取得可能な isLandscape で判定するとマルチタスクを正しく検知できないため size で判定する
        if size.width < size.height {
            // Portrait
            m_isVertical = true
        } else {
            // Landscape
            m_isVertical = false
        }
        setupLayout(size)
        self.updateView()
    }
    
    // AppDelegate -> applicationWillEnterForeground の通知
    @objc func viewWillEnterForeground(notification: Notification) {
        // 設定アプリに応じたフォルダ管理機能の on/off 対応
        let temp = getAppSettingWithFolderManagement()
        if m_enableFolderManagement != temp {
            // 設定が変更されていれば更新
            m_enableFolderManagement = temp
            // 設定が変更されている場合ホームフォルダ画面に更新する
            setLayoutToHomeFolderMode()
            self.loadProjectData()
            self.updateView()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        m_collectionView.collectionViewLayout.invalidateLayout()
    }
    
    private func getSampleThumbnail(_ count: Int, _ currentFolder: FolderDataSet) -> [UIImage]? {
        var thumbnails: [UIImage]? = nil
        if let children = currentFolder.getChildrenProjectPathList() {
            thumbnails = []
            let dataMgr = DataManager.share()
            var numOfImage = 0
            for imagePath in children {
                var filePath = dataMgr.getRealFile(targetPath: imagePath, withFile: false)
                if filePath != "" {
                    filePath = filePath + "/thumbnail.jpg"
                    let image = UIImage(contentsOfFile: filePath)
                    thumbnails?.append(image!)
                    if count <= numOfImage {
                        break
                    }
                    numOfImage += 1
                }
            }
        }
        return thumbnails
    }
    
    /* ----- UICollectionView Delegate functions ----- */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let dataArray = m_loardData{
            return dataArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PosterProjectCell", for: indexPath)
        let kind = m_loardData[indexPath.row].kindOfDataset
        if kind == false {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PosterProjectCell", for: indexPath)
            if let cell = cell as? ProjectCollectionViewCell {
                let item = m_loardData[indexPath.row] as? PosterDataSet
                // キャッシュ削除
                cell.thumbnail.image = nil
                cell.set(image: item!.thumbnail)
                cell.set(name: item!.displayName!)
                cell.delegate = self
                cell.tag = indexPath.row
                return cell
            }
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FolderProjectCell", for: indexPath)
            if let cell = cell as? FolderCollectionViewCell {
                let item = m_loardData[indexPath.row] as? FolderDataSet
                // キャッシュ削除
                cell.set(name: item!.displayName!)
                cell.delegate = self
                cell.exampleImage1.image = nil
                cell.exampleImage2.image = nil
                cell.exampleImage3.image = nil
                cell.exampleImage4.image = nil
                cell.name = item?.name
                cell.dateTime = item?.dateTime
                
                if let thumbs = getSampleThumbnail(4,item!) {
                    if thumbs.count > 0 {
                        cell.exampleImage1.image = thumbs[0]
                    }
                    if thumbs.count > 1 {
                        cell.exampleImage2.image = thumbs[1]
                    }
                    if thumbs.count > 2 {
                        cell.exampleImage3.image = thumbs[2]
                    }
                    if thumbs.count > 3 {
                        cell.exampleImage4.image = thumbs[3]
                    }
                }
                
                cell.tag = indexPath.row
                return cell
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        startIndicator()
        
        let kind = m_loardData[indexPath.row].kindOfDataset
        if kind == false {
            let item = m_loardData[indexPath.row] as? PosterDataSet
            // [indexPath.row] から画像名を探し、UImage を設定
            let targetNeme = item!.projectFolder
            
            let storyboard: UIStoryboard = UIStoryboard(name: "CanvasEditor", bundle: nil)
            canvasController = storyboard.instantiateViewController(withIdentifier: "CanvasViewController") as? CanvasViewController
            canvasController.modalPresentationStyle = .fullScreen
            
            canvasController.isNewCanvas = false
            print(targetNeme!)
            let str:URL = URL(fileURLWithPath: targetNeme!, isDirectory: true)
            
            print(str)
            canvasController.folderUrl = str
            let subStrings = targetNeme!.components(separatedBy: "/")
            let targetFolderName = subStrings[subStrings.count-1]
            
            print(DataManager.share().getPrintDataPath()!)
            canvasController.printDataUrl = DataManager.share().getPrintDataPath()!.appendingPathComponent(targetFolderName + ".jpg")
            m_ViewInfo.isNew = false
            canvasController.parentViewControllerInfo = m_ViewInfo
            present(canvasController, animated: true, completion: nil)
        }else{
            // 記入
            stopIndicator()
            let item = m_loardData[indexPath.row] as? FolderDataSet
            m_loardData.removeAll()
            if let children = item?.getChildrenProjectPathList() {
                for path in children {
                    
                    
                    let dataMgr = DataManager.share()
                    let realFilePath = dataMgr.getRealFile(targetPath: path, withFile: false)
                    if realFilePath != "" {
                        let dataSet = dataMgr.createtProjectDates(target: realFilePath)
                        m_loardData.append(dataSet)
                        m_loardData.sort { (PosterDataSet, PosterDataSet2) -> Bool in
                            PosterDataSet.dateTime! > PosterDataSet2.dateTime!
                        }
                    }
                }
            }
            UIView.animate(withDuration: 0.3,
                           delay: 0.0,
                           options: [.curveLinear],
                           animations: { [self] in
                setLayoutToSubFolderMode()
            }, completion: nil)
            //setLayoutToItemMode()
            self.updateView()
            m_collectionView.scroll(to: .top, animated: false)
            m_parentFolder = item
            m_topMode = false
            m_ViewInfo.isNew = false
            folderNameLabel.text = item?.displayName
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return getCellSize()
    }
    
    /* ----- called from OtherViewController dismiss ----- */
    func returnedFromOtherViewController(_ param: Any?) {
        
        if let projectCollectionViewCell = param as? ProjectCollectionViewCell {
            
            // メニュー表示
            let storyboard: UIStoryboard = self.storyboard!
            cellMenuController = storyboard.instantiateViewController(withIdentifier: "CellMenuViewController") as? CellMenuViewController
            cellMenuController.modalPresentationStyle = .popover // .pageSheet, .fullScreen, .overFullScreen, .formSheet, .popover
            cellMenuController.popoverPresentationController!.permittedArrowDirections = UIPopoverArrowDirection(rawValue:0)
            cellMenuController.popoverPresentationController!.sourceView = projectCollectionViewCell.title
            
            // フォルダ移動ボタンの調整
            var cellMenuHeight = kCellMenuHeight * 4
            if !m_enableFolderManagement {
                cellMenuHeight = kCellMenuHeight * 3
            }
            
            cellMenuController.preferredContentSize = CGSize(width: kCellMenuWidth, height: cellMenuHeight+kCellMenuHeightSpace)
            cellMenuController.popoverPresentationController!.sourceRect = CGRect(x: 0, y: 0, width: Int(projectCollectionViewCell.bounds.width)*2-kCellMenuWidth,height:-cellMenuHeight-kCellMenuHeightSpace)
            cellMenuController.delegate = self
            
            m_CurrentIndex = projectCollectionViewCell.tag
            present(cellMenuController, animated: true, completion: nil)
            return
            
        } else if let cellMenuViewController = param as? CellMenuViewController {
            // メニュー内の各ボタンの処理
            let kind = m_loardData[m_CurrentIndex].kindOfDataset
            if kind == false {
                let item = m_loardData[m_CurrentIndex] as? PosterDataSet
                let targetName = item!.projectFolder!
                let targetURL:URL = URL(fileURLWithPath: targetName, isDirectory: true)
                
                cellMenuViewController.dismiss(animated: true, completion: nil)
                
                if cellMenuViewController.operation == CellMenuOperation.delete {
                    // 削除
                    let alert = UIAlertController(title: Message.DeletePoster.info.title, message: Message.DeletePoster.info.message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "いいえ", style: .cancel, handler: nil))
                    alert.addAction(UIAlertAction(title: "はい", style: .default, handler: { [self] (UIAlertAction) in
                        let dataMgr = DataManager.share()
                        // 削除処理
                        dataMgr.deleteProjectData(targetURL.path)
                        dataMgr.deleteBackupData(targetURL.path)
                        // 画面更新
                        if self.m_topMode == false {
                            let index = dataMgr.indexOfFolder(target: m_parentFolder)
                            dataMgr.removeProjetChildItem(targetIndex: index!, childName: item!.projectFolder)
                        }
                        self.loadProjectData()
                        self.updateView()
                    }))
                    self.present(alert, animated: true)
                    
                } else if cellMenuViewController.operation == CellMenuOperation.print {
                    // 印刷
                    let subStrings = targetName.components(separatedBy: "/")
                    let targetFolderName = subStrings[subStrings.count-1]
                    let printDataUrl = DataManager.share().getPrintDataPath()!.appendingPathComponent(targetFolderName + ".jpg")
                    _ = PrintManager.printFunc(url: printDataUrl, self)
                    
                } else if cellMenuViewController.operation == CellMenuOperation.copy {
                    // コピー
                    let alert = UIAlertController(title: Message.CopyPoster.info.title, message: Message.CopyPoster.info.message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "いいえ", style: .cancel, handler: nil))
                    alert.addAction(UIAlertAction(title: "はい", style: .default, handler: { [self] (UIAlertAction) in
                        let dataMgr = DataManager.share()
                        // 新規ポスター作成
                        let newUrlPath = dataMgr.createNewProject()
                        // プロジェクトデータコピー
                        dataMgr.copyProjectData(targetURL, URL(fileURLWithPath: newUrlPath))
                        // info_data, print_data コピー
                        let newPosterTitle = newUrlPath.subString("/", 1, false).replacingOccurrences(of:"/", with:"")
                        dataMgr.copyBackupData(targetURL.path, newPosterTitle)
                        // フォルダー属性コピー
                        if m_parentFolder != nil {
                            if let index = dataMgr.indexOfFolder(target: m_parentFolder) {
                                dataMgr.addProjectString(newProject: newUrlPath, index: index)
                            }
                        }
                        // 画面更新
                        self.loadProjectData()
                        self.updateView()
                        
                        // 画面内一番上までスクロール（最新を表示）
                        m_collectionView.scroll(to: .top, animated: true)
                    }))
                    self.present(alert, animated: true)
                    
                } else if cellMenuViewController.operation == CellMenuOperation.move {
                    // 移動
                    // メニュー表示
                    let storyboard: UIStoryboard = UIStoryboard(name: "ContentsManagment", bundle: nil)
                    movePosterController = storyboard.instantiateViewController(withIdentifier: "MovePosterViewController") as? MovePosterViewController
                    movePosterController.modalPresentationStyle = .popover // .pageSheet, .fullScreen, .overFullScreen, .formSheet, .popover
                    movePosterController.preferredContentSize = CGSize(width: 500, height: 400)
                    movePosterController.popoverPresentationController!.permittedArrowDirections = UIPopoverArrowDirection(rawValue:0)
                    movePosterController.popoverPresentationController!.sourceView = self.view
                    movePosterController.popoverPresentationController!.sourceRect = CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2, width: 0, height: 0)
                    movePosterController.delegate = self
                    movePosterController.currentFolder = m_parentFolder
                    
                    self.present(movePosterController!, animated: true, completion: nil)
                    
                } else if cellMenuViewController.operation == CellMenuOperation.fileExport {
                    // ポスターエクスポート
                }
                
                cellMenuViewController.operation = CellMenuOperation.noOperation
                return
            }
        } else if let createNewPosterController = param as? CreateNewPosterController {
            if createNewPosterController.isOK {
                let canvasSize = createNewPosterController.canvasSize!
                createNewPosterController.dismiss(animated: false, completion: nil)
                createNewProject(canvasSize: canvasSize)
            } else {
                createNewPosterController.dismiss(animated: true, completion: nil)
            }
            return
        } else if let folderViewCell = param as? FolderCollectionViewCell {
            // フォルダーメニュー表示
            let storyboard: UIStoryboard = self.storyboard!
            let folderCellMenuController = storyboard.instantiateViewController(withIdentifier: "FolderMenuViewController") as? FolderVellMenuViewController
            folderCellMenuController?.modalPresentationStyle = .popover // .pageSheet, .fullScreen, .overFullScreen, .formSheet, .popover
            folderCellMenuController?.preferredContentSize = CGSize(width: kCellMenuWidth, height: kCellMenuHeight+kCellMenuHeightSpace)
            folderCellMenuController?.popoverPresentationController!.permittedArrowDirections = UIPopoverArrowDirection(rawValue:0)
            folderCellMenuController?.popoverPresentationController!.sourceView = folderViewCell.titleLabel
            folderCellMenuController?.popoverPresentationController!.sourceRect = CGRect(x: 0, y: 0, width:
                                                                                            Int(folderViewCell.bounds.width)*2-kCellMenuWidth,height:-kCellMenuHeight-kCellMenuHeightSpace)
            folderCellMenuController?.delegate = self
            folderCellMenuController?.folderName = folderViewCell.name
            folderCellMenuController?.dateTime = folderViewCell.dateTime
            
            present(folderCellMenuController!, animated: true, completion: nil)
            return
            
        } else if let folderCellController = param as? FolderVellMenuViewController {
            folderCellController.dismiss(animated: true, completion: nil)
            
            if folderCellController.operation == CellMenuOperation.delete {
                // フォルダー削除
                let alert = UIAlertController(title: Message.DeleteFolder.info.title, message: Message.DeleteFolder.info.message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "いいえ", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "はい", style: .default, handler: { [self] (UIAlertAction) in
                    if folderCellController.folderName != nil {
                        // 削除処理
                        let dataMgr = DataManager.share()
                        
                        if let data = dataMgr.getDataSetItem(target: folderCellController.dateTime) {
                            if let children = data.getChildrenProjectPathList() {
                                for item in children {
                                    let realPath = dataMgr.getRealFile(targetPath: item, withFile: false)
                                    dataMgr.deleteProjectData(realPath)
                                    dataMgr.deleteBackupData(realPath)
                                }
                                if let indexOfFolder = getIndex(data.dateTime!) {
                                    m_loardData.remove(at: indexOfFolder)
                                }
                                if let index = dataMgr.indexOfFolder(target: data) {
                                    dataMgr.removeProjectString(index: index)
                                }
                                if self.m_topMode == false {
                                    let index = dataMgr.indexOfFolder(target: data)
                                    dataMgr.removeProjetChildItem(targetIndex: index!, childName: data.name!)
                                }
                                self.loadProjectData()
                                self.updateView()
                            }
                        }
                    }
                }))
                self.present(alert, animated: true)
                
            } else if folderCellController.operation == CellMenuOperation.copy {
                // フォルダーコピー
            } else if folderCellController.operation == CellMenuOperation.fileExport {
                // フォルダーエクスポート
            }
        } else {
            if m_ViewInfo.isNew {
                self.loadProjectData()
                m_CurrentIndex = 0
                if (m_collectionView.numberOfItems(inSection: 0) != 0) {
                    m_collectionView.scrollToItem(at: IndexPath(item: m_CurrentIndex, section: 0), at: UICollectionView.ScrollPosition.init(rawValue: UInt(m_CurrentIndex)), animated: true)
                }
                let dataMgr = DataManager.share()
                if m_topMode == false {
                    if m_parentFolder != nil {
                        if let index = dataMgr.indexOfFolder(target: m_parentFolder) {
                            dataMgr.addProjectString(newProject: m_ViewInfo.folderPath!, index: index)
                            let dataSet = dataMgr.createtProjectDates(target: m_ViewInfo.folderPath!)
                            m_loardData.append(dataSet)
                            m_loardData.sort { (PosterDataSet, PosterDataSet2) -> Bool in
                                PosterDataSet.dateTime! > PosterDataSet2.dateTime!
                            }
                        }
                    }
                }
            } else {
                if let targetURL = (param as? URL){
                    if m_topMode == false {
                        let dataMgr = DataManager.share()
                        dataMgr.setupDataSetBase(m_parentFolder)
                    }
                    if let index = reLoadImage(targetURL.path){
                        m_CurrentIndex = index
                    }
                }
            }
            canvasController = nil
            
            if let targetURL = (param as? URL){
                let dataMgr = DataManager.share()
                dataMgr.copyBackupData(targetURL.path)
            }
            self.updateView()
        }
    }
    
    private func getIndex(_ dateTime: Date) -> Int? {
        if m_loardData != nil {
            for (i, data) in m_loardData.enumerated() {
                if data.dateTime == dateTime {
                    return i
                }
            }
        }
        return nil
    }
    
    private func setLayoutToSubFolderMode() {
        folderCloseButton.isHidden = false
        FolderPanel.isHidden = false
        collectionViewBottom.constant = 100
        collectionLead.constant = 5
        collectionTail.constant = 5
        createNewFolder.isHidden = true
        if m_enableFolderManagement == false {
            createNewFolder.isHidden = true
            m_topMode = true
        }
    }
    
    private func setLayoutToHomeFolderMode() {
        folderCloseButton.isHidden = true
        FolderPanel.isHidden = true
        collectionViewBottom.constant = 0
        collectionLead.constant = 0
        collectionTail.constant = 0
        createNewFolder.isHidden = false
        if m_enableFolderManagement == false {
            createNewFolder.isHidden = true
            m_topMode = true
        }
    }
    
    /* ----- loard thumbnails from folder ----- */
    private func getAppSettingWithFolderManagement() -> Bool {
        let userDefaults = UserDefaults.standard
        let value = userDefaults.bool(forKey: UserDefaults.Keys.enableFolder)
        return value
    }
    
    private func loadProjectData() {
        let dataMgr = DataManager.share()
        if m_topMode {
            dataMgr.loadFolders()
            m_loardData = dataMgr.getThumbmails()
        }else{
            m_loardData = dataMgr.getFolderDateSets(target: m_parentFolder)
        }
        m_loardData.sort { (PosterDataSet, PosterDataSet2) -> Bool in
            PosterDataSet.dateTime! > PosterDataSet2.dateTime!
        }
    }
    
    /* ----- loard the target thumbnail from folder ----- */
    private func reLoadImage( _ folderName: String ) -> Int?{
        self.loadProjectData()
        let dataMgr = DataManager.share()
        if m_topMode {
            if let index = dataMgr.reloadThumbnailDirectly(folderName){
                m_loardData = nil
                m_loardData = dataMgr.getProjecDatas(target: nil)
                m_loardData.sort { (PosterDataSet, PosterDataSet2) -> Bool in
                    PosterDataSet.dateTime! > PosterDataSet2.dateTime!
                }
                self.updateView()
                return index
            }
        }else{
            if let index = dataMgr.reloadThumbnailDirectly(folderName){
                m_loardData = nil
                m_loardData = dataMgr.getProjecDatas(target: m_parentFolder)
                m_loardData.sort { (PosterDataSet, PosterDataSet2) -> Bool in
                    PosterDataSet.dateTime! > PosterDataSet2.dateTime!
                }
                return index
            }
        }
        return nil
    }
    
    /* ----- calculate the cell size ----- */
    
    private func getCellSize() -> CGSize {
        var cellWidth:Int
        var cellHight:Int
        
        if m_isVertical {
            cellWidth = Int(Int(m_collectionView.bounds.width)/2-kOffsrt)
            cellHight = cellWidth+(kOffsrt*2)+(kOffsrt/2)
        } else {
            cellWidth = Int(Int(m_collectionView.bounds.width)/3-kOffsrt)
            cellHight = cellWidth
        }
        
        // マルチタスク対応
        if cellWidth < kCellWidthMin {
            cellWidth = kCellWidthMin
            cellHight = cellWidth
        }else if cellWidth > kCellWidthMax {
            cellWidth = kCellWidthMax
            cellHight = cellWidth
        }
        
        return CGSize(width: cellWidth, height: cellHight)
    }
    
    private func getCellSize(parentSize: CGSize) -> CGSize {
        var cellWidth:Int
        var cellHight:Int
        
        if m_isVertical {
            cellWidth = Int(Int(parentSize.width)/2-kOffsrt)
            cellHight = cellWidth+(kOffsrt*2)+(kOffsrt/2)
        } else {
            cellWidth = Int(Int(parentSize.width)/3-kOffsrt)
            cellHight = cellWidth
        }
        
        // マルチタスク対応
        if cellWidth < kCellWidthMin {
            cellWidth = kCellWidthMin
            cellHight = cellWidth
        }else if cellWidth > kCellWidthMax {
            cellWidth = kCellWidthMax
            cellHight = cellWidth
        }
        
        return CGSize(width: cellWidth, height: cellHight)
    }
    
    /* ----- Update the collection view ----- */
    private func updateView(){
        m_collectionView.reloadData()
    }
    
    private func setupLayout(_ frameSize: CGSize){
        if m_topMode {
            UIView.animate(withDuration: 0.3,
                           delay: 0.0,
                           options: [.curveLinear],
                           animations: { [self] in
                setLayoutToHomeFolderMode()
            }, completion: nil)
        }else{
            UIView.animate(withDuration: 0.3,
                           delay: 0.0,
                           options: [.curveLinear],
                           animations: { [self] in
                setLayoutToSubFolderMode()
            }, completion: nil)
        }
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.itemSize = getCellSize(parentSize: frameSize)
        collectionViewLayout.sectionInset = UIEdgeInsets(top: CGFloat(kSpaceBetweenItems),
                                                         left: CGFloat(kSpaceBetweenItems),
                                                         bottom: 0,
                                                         right: CGFloat(kSpaceBetweenItems))
        m_collectionView.collectionViewLayout = collectionViewLayout
    }
    
}

extension ContentsManagememtViewController: ReturnViewContorller {
    func executeReturnCallback(param obj: Any?) {
        let dataMgr = DataManager.share()
        
        //TODO: これ以上増える場合、判定方法を変更した方が良い
        if let folderName = obj as? String {
            // 新規フォルダー作成
            m_createFolderName = folderName
            let folderItem = FolderDataSet(stringValue: m_createFolderName)
            dataMgr.addNewFolderDataSet(newFolder: folderItem)
            if m_topMode {
                m_loardData = dataMgr.getThumbmails()
            }else{
                m_loardData = dataMgr.getFolderDateSets(target: folderItem)
            }
            m_loardData.sort { (PosterDataSet, PosterDataSet2) -> Bool in
                PosterDataSet.dateTime! > PosterDataSet2.dateTime!
            }
            
            self.updateView()
        }
        else if let folder = obj as? FolderDataSet {
            // ポスターのフォルダ移動
            let poster = m_loardData[m_CurrentIndex] as? PosterDataSet
            
            if m_parentFolder != nil {
                // ホームフォルダへ移動
                if let index = dataMgr.indexOfFolder(target: m_parentFolder) {
                    dataMgr.removeProjetChildItem(targetIndex: index, childName: poster!.projectFolder)
                }
            }
            
            if m_parentFolder != nil && m_parentFolder.compareFolder(folder) {
                // ホームフォルダを移動先とした場合、処理なし
            }
            else {
                // 指定フォルダの属性を設定
                if let index = dataMgr.indexOfFolder(target: folder) {
                    dataMgr.addProjectString(newProject: poster!.projectFolder, index: index)
                }
            }
            
            self.loadProjectData()
            self.updateView()
        }
    }
}
