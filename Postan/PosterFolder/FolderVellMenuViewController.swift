//
//  FolderVellMenuViewController.swift
//  Postan
//
//  Created by Takashi Aizawa on 2021/06/26.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

class FolderVellMenuViewController: UIViewController {
    var operation: CellMenuOperation = .noOperation
    var folderName: String!
    var dateTime: Date!
    
    weak var delegate:CellMenuDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func deleteFolderButtonClicked(_ sender: Any) {
        operation = .delete
        delegate?.returnedFromOtherViewController(self)
    }
    
    @IBAction func copyFolderButtonClicked(_ sender: Any) {
        operation = .copy
        delegate?.returnedFromOtherViewController(self)
    }
}
