//
//  SerializeObject.swift
//  Postan
//
//  Created by Takashi Aizawa on 2021/06/17.
//  Copyright © 2021 beginner. All rights reserved.
//

import Foundation

class SerializeObject: Codable {
    var m_FolderObjects: [FolderDataSet]?
    init(folders: [FolderDataSet]) {
        self.m_FolderObjects = folders
    }
    func loardObject() -> [FolderDataSet]? {
        return m_FolderObjects
    }
}
