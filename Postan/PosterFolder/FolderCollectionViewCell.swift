//
//  FolderCollectionViewCell.swift
//  Postan
//
//  Created by Takashi Aizawa on 2021/06/06.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

class FolderCollectionViewCell: ProjectBaseCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var exampleImage1: UIImageView!
    @IBOutlet weak var exampleImage2: UIImageView!
    @IBOutlet weak var exampleImage3: UIImageView!
    @IBOutlet weak var exampleImage4: UIImageView!
    private var m_exampleImages: [UIImage]!
    var name: String!
    var dateTime: Date!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backImageView.image = UIImage(named: "FolderImage")
        self.backImageView.contentMode = .scaleAspectFit
    }
    
    @IBAction func onMenueButtonClicked(_ sender: Any) {
        delegate?.returnedFromOtherViewController(self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        name = nil
        if m_exampleImages != nil {
            m_exampleImages.removeAll()
            m_exampleImages = nil
            exampleImage1.image = nil
            exampleImage2.image = nil
            exampleImage3.image = nil
            exampleImage4.image = nil
        }
    }
    
    /* ----- set the date time string to cell title  ----- */
    func set(name:String) {
        titleLabel.text = name
    }
}
