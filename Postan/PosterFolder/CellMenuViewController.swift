//
//  CellMenuViewController.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2020/09/18.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

protocol CellMenuDelegate:AnyObject{
    func returnedFromOtherViewController(_ param: Any?)
}

enum CellMenuOperation {
    case noOperation
    case delete
    case print
    case copy
    case move
    case fileExport
}

class CellMenuViewController: UIViewController {
    @IBOutlet weak var moveButton: UIButton!
    
    private var m_enableFolderManagement: Bool!
    
    weak var delegate:CellMenuDelegate?
    var operation: CellMenuOperation = .noOperation
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let enableFolderManagement = getAppSettingWithFolderManagement()
        moveButton.isEnabled = enableFolderManagement
        moveButton.isHidden = !enableFolderManagement
    }
    
    private func getAppSettingWithFolderManagement() -> Bool  {
        let userDefaults = UserDefaults.standard
        let value = userDefaults.bool(forKey: UserDefaults.Keys.enableFolder)
        return value
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        operation = .delete
        delegate?.returnedFromOtherViewController(self)
    }
    
    @IBAction func printButton(_ sender: Any) {
        operation = .print
        delegate?.returnedFromOtherViewController(self)
    }
    
    @IBAction func copyButton(_ sender: Any) {
        operation = .copy
        delegate?.returnedFromOtherViewController(self)
    }
    
    @IBAction func moveButton(_ sender: Any) {
        operation = .move
        delegate?.returnedFromOtherViewController(self)
    }
    
    @IBAction func fileExportButton(_ sender: Any) {
        operation = .fileExport
        delegate?.returnedFromOtherViewController(self)
    }
}
