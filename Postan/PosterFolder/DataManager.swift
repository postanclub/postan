//
//  DataManager.swift
//  Postan
//
//  Created by Takashi Aizawa on 2020/07/24.
//  Copyright © 2020 beginner. All rights reserved.
//

import Foundation
import UIKit

class DataManager{
    static var instance:DataManager! = nil
    private let kImageBackupFoldeerName = "print_data"
    private let kItemInfoFileFolderName = "info_data"
    private let kJsonFilename = "folderManagement.json"
    private var m_ForlderUILs:[URL]? = nil
    private var m_projectDatas:[DataSetBase] = []
    private var m_FolderDataSets: [FolderDataSet]?
    private var m_ImageBackupFolderURL: URL?
    private var m_InfoBackupFolderURL: URL?
    private var m_selectedFolderIndex: Int?
    private var m_enableFolderManagement: Bool!
    
    /* ----- create or get instance ----- */
    static func share() -> DataManager {
        if instance == nil {
            instance = DataManager()
            instance.loadFolders()
            instance.crateInternalDirectory()
        }
        return instance
    }
    
    /* ----- get the folder path to save the printed images ----- */
    func getPrintDataPath() -> URL?{
        return self.m_ImageBackupFolderURL
    }
    
    /* ----- get the folders ----- */
    func loadFolders() {
        m_enableFolderManagement = getAppSettingWithFolderManagement()
        
        let pathURL = getDocumentsDirectory()
        do {
            let fileAndFolderURLs = try FileManager.default.contentsOfDirectory(at: pathURL, includingPropertiesForKeys: nil)
            print(fileAndFolderURLs)
            m_ForlderUILs = fileAndFolderURLs.filter { url in
                //`url`がファイルかフォルダか判定して、フォルダなら`true`を返す
                var isDirectory: ObjCBool = false
                return FileManager.default.fileExists(atPath: url.path, isDirectory: &isDirectory) && isDirectory.boolValue
            }
            print(m_ForlderUILs!)
        } catch {
            print(error)
        }
    }
    
    /* ----- create the internal folders that are used by special usecase  ----- */
    func crateInternalDirectory(){
        let pathURL = getDocumentsDirectory()
        do {
            m_ImageBackupFolderURL = pathURL.appendingPathComponent( kImageBackupFoldeerName )
            m_InfoBackupFolderURL = pathURL.appendingPathComponent( kItemInfoFileFolderName )
            var isDirectory: ObjCBool = true
            if  FileManager.default.fileExists(atPath: m_ImageBackupFolderURL!.path, isDirectory: &isDirectory) == false {
                try FileManager.default.createDirectory(atPath: m_ImageBackupFolderURL!.path, withIntermediateDirectories: true,attributes: nil)
            }
            
            if  FileManager.default.fileExists(atPath: m_InfoBackupFolderURL!.path, isDirectory: &isDirectory) == false {
                try FileManager.default.createDirectory(atPath: m_InfoBackupFolderURL!.path, withIntermediateDirectories: true,attributes: nil)
            }
        } catch {
            print(error)
        }
    }
    
    /* ----- loard thumbnails from the target folder ----- */
    func getThumbmails() -> [DataSetBase] {
        m_projectDatas.removeAll()
        m_FolderDataSets?.removeAll()
        if m_enableFolderManagement == true {
            m_FolderDataSets = FolderInfoSrializeObject.share().loadFolderDateSet()
            m_projectDatas.removeAll()
            if m_FolderDataSets != nil {
                for folderItem in m_FolderDataSets! {
                    m_projectDatas.append(folderItem)
                }
            }
        }
        if let folders = m_ForlderUILs {
            for folder in folders {
                do{
                    let itemPaths = try FileManager.default.contentsOfDirectory(at: folder, includingPropertiesForKeys: nil)
                    for fileUrl in itemPaths {
                        let subStrings = fileUrl.path.components(separatedBy: "/")
                        if ((checkTargetFolder(subStrings[subStrings.count - 2]) == true) && (subStrings[subStrings.count - 1] == "thumbnail.jpg") ){
                            if inFolder(fileUrl.path) == false {
                                let posterData = PosterDataSet(name: fileUrl.path, image: UIImage(contentsOfFile: fileUrl.path)!)
                                posterData.projectFolder = folder.path
                                m_projectDatas.append(posterData)
                                break
                            }
                        }
                    }
                } catch {
                    print(error)
                    return []
                }
            }
            m_projectDatas.sort { (PosterDataSet, PosterDataSet2) -> Bool in
                PosterDataSet.dateTime! > PosterDataSet2.dateTime!
            }
            return m_projectDatas
            
        }
        return []
    }
    
    func getRealFile(targetPath path: String, withFile flag: Bool) -> String {
        let pathURL = getDocumentsDirectory()
        do {
            let itemPaths = try FileManager.default.contentsOfDirectory(at: pathURL, includingPropertiesForKeys: nil)
            for fileUrl in itemPaths {
                let subStrings = fileUrl.path.components(separatedBy: "/")
                let targetSubstrings = path.components(separatedBy: "/")
                let targetString: String
                if flag == true {
                    targetString = targetSubstrings[targetSubstrings.count - 2]
                }else{
                    targetString = targetSubstrings[targetSubstrings.count - 1]
                }
                if targetString == subStrings[subStrings.count - 1] {
                    return fileUrl.path
                }
            }
        }catch{
            print(error)
            return ""
        }
        return ""
    }
    
    private func inFolder(_ dataPath: String) -> Bool {
        if m_FolderDataSets != nil {
            for folder in m_FolderDataSets! {
                let children = folder.getChildrenProjectPathList()
                if let unwrapChildren = children {
                    for child in unwrapChildren {
                        let subStrings = child.components(separatedBy: "/")
                        let childFilePath = subStrings[subStrings.count-1]
                        let targetPaths = dataPath.components(separatedBy: "/")
                        if childFilePath == targetPaths[targetPaths.count-2] {
                            return true
                        }
                    }
                }
            }
        }
        return false
    }
    
    func getDataSetItem(target name: String) -> FolderDataSet? {
        if m_FolderDataSets != nil {
            for folder in m_FolderDataSets! {
                if folder.name == name {
                    return folder
                }
            }
        }
        return nil
    }
    
    func getDataSetItem(target date: Date) -> FolderDataSet? {
        if m_FolderDataSets != nil {
            for folder in m_FolderDataSets! {
                if folder.dateTime == date {
                    return folder
                }
            }
        }
        return nil
    }
    
    func setupDataSetBase(_ target: FolderDataSet) {
        m_projectDatas.removeAll()
        if m_FolderDataSets != nil {
            for path in target.getChildrenProjectPathList()! {
                var filePath = path
                let realPath = getRealFile(targetPath: filePath, withFile: false)
                if realPath != "" {
                    filePath = realPath + "/thumbnail.jpg"
                    let posterData = PosterDataSet(name: filePath, image: UIImage(contentsOfFile: filePath)!)
                    posterData.projectFolder = realPath
                    m_projectDatas.append(posterData)
                    m_projectDatas.sort { (PosterDataSet, PosterDataSet2) -> Bool in
                        PosterDataSet.dateTime! > PosterDataSet2.dateTime!
                    }
                }
            }
        }
    }
    
    /* ----- create new folder for poster datas ----- */
    func createNewProject() -> String{
        let homeUrl = getDocumentsDirectory()
        let url: URL = homeUrl.appendingPathComponent( nowTimeStampToString() )
        
        do {
            try FileManager.default.createDirectory(atPath: url.path,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
        } catch {
            fatalError("Can't create directory \(url)")
        }
        return url.path
    }
    
    /* -----　replace the thumbnail in the target Folder----- */
    func reloadThumbnailDirectly( _ projectFolder: String ) -> Int?{
        for (index, item ) in m_projectDatas.enumerated() {
            if let projectData = item as? PosterDataSet {
                if projectData.projectFolder == projectFolder {
                    let image = UIImage(contentsOfFile: projectData.projectFolder + "/" + projectData.fileName )
                    projectData.thumbnail = image!
                    m_projectDatas.sort { (PosterDataSet, PosterDataSet2) -> Bool in
                        PosterDataSet.dateTime! > PosterDataSet2.dateTime!
                    }
                    return index
                }
            }
        }
        return nil
    }
    
    /* -----　get project datasets ----- */
    func getProjecDatas(target folder: FolderDataSet?) -> [DataSetBase]{
        if let name = folder {
            setupDataSetBase(name)
        }
        return m_projectDatas
    }
    
    func createtProjectDates(target path: String) -> DataSetBase {
        let filePath = path + "/thumbnail.jpg"
        let posterData = PosterDataSet(name: filePath, image: UIImage(contentsOfFile: filePath)!)
        posterData.projectFolder = path
        m_projectDatas.append(posterData)
        m_projectDatas.sort { (PosterDataSet, PosterDataSet2) -> Bool in
            PosterDataSet.dateTime! > PosterDataSet2.dateTime!
        }
        return posterData
    }
    
    func indexOfFolder(target dataSet: FolderDataSet) -> Int? {
        if m_FolderDataSets != nil {
            for (i, item) in m_FolderDataSets!.enumerated() {
                if item.dateTime == dataSet.dateTime {
                    return i
                }
            }
        }
        return nil
    }
    
    func addProjectString(newProject name: String, index i: Int) {
        if m_FolderDataSets != nil {
            if m_FolderDataSets!.count > i {
                m_FolderDataSets![i].addProjectPath(newProject: name)
                FolderInfoSrializeObject.share().saveFolderDataSet(dataSet: m_FolderDataSets!)
            }
        }
    }
    
    func removeProjetChildItem(targetIndex index: Int, childName name: String) {
        if m_FolderDataSets != nil {
            if m_FolderDataSets!.count > index {
                if let childItem = m_FolderDataSets?[index] {
                    childItem.removeProjectPath(removeProject: name)
                    m_FolderDataSets![index] = childItem
                    FolderInfoSrializeObject.share().saveFolderDataSet(dataSet: m_FolderDataSets!)
                }
            }
        }
    }
    func removeProjectString(index i: Int) {
        if m_FolderDataSets != nil {
            if m_FolderDataSets!.count > i {
                m_FolderDataSets?.remove(at: i)
                FolderInfoSrializeObject.share().saveFolderDataSet(dataSet: m_FolderDataSets!)
            }
        }
    }
    func removeProjectString(targetName name: String) -> Int? {
        if m_FolderDataSets != nil {
            for (i, data) in m_FolderDataSets!.enumerated() {
                let realPath = getRealFile(targetPath: name, withFile: false)
                if data.name == realPath {
                    data.removeAll()
                    m_FolderDataSets?.remove(at: i)
                    FolderInfoSrializeObject.share().saveFolderDataSet(dataSet: m_FolderDataSets!)
                    return i
                }
            }
        }
        return nil
    }
    /* ----- delete folder and copy the JISON file for log info ----- */
    func deleteItem( _ projectFolder: String ) -> (Int, PosterDataSet?){
        let fileManager = FileManager.default
        var delteIndex:Int = -1
        var object:PosterDataSet? = nil
        for (index, item ) in m_projectDatas.enumerated() {
            if let projectData = item as? PosterDataSet {
                if projectData.projectFolder == projectFolder {
                    do {
                        let homeUrl = FileManager.default.urls(for: .documentDirectory,
                                                                  in: .userDomainMask).first!
                        try fileManager.removeItem(atPath: homeUrl.path+"/"+projectData.fileName)
                        try fileManager.copyItem(atPath: projectData.projectFolder+"/"+projectData.fileName, toPath: homeUrl.path+"/"+projectData.fileName)
                        try fileManager.removeItem(atPath: projectFolder)
                    } catch {
                        return (-1, object)
                    }
                    object = projectData.clone()
                    m_projectDatas.remove(at: index)
                    delteIndex = index
                    break
                }
            }
        }
        return ( delteIndex, object )
    }
    
    /* ----- delete poster project ----- */
    func deleteProjectData( _ targetProjectFolder: String ) {
        do {
            let fileManager = FileManager.default
            
            // Check if file exists
            if fileManager.fileExists(atPath: targetProjectFolder) {
                // Delete file
                try fileManager.removeItem(atPath: targetProjectFolder)
            } else {
                print("File does not exist")
            }
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
    
    /* ----- copy poster project ----- */
    func copyProjectData( _ targetProjectFolder: URL, _ newProjectFolder: URL ) {
        do {
            let fileManager = FileManager.default
            
            // Check if file exists
            if fileManager.fileExists(atPath: targetProjectFolder.path) {
                // Copy file
                try fileManager.replaceWithCopyOfFile(at: newProjectFolder, with: targetProjectFolder)
            } else {
                print("File does not exist")
            }
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
    
    /* ----- delete the Backup Data. Print file and Json files ----- */
    func deleteBackupData( _ targetProjectFolder: String ) {
        do {
            let fileManager = FileManager.default
            
            let projectDataName = URL(fileURLWithPath: targetProjectFolder).lastPathComponent
            let printDataFile = m_ImageBackupFolderURL!.appendingPathComponent(projectDataName).appendingPathExtension("jpg")
            
            /*
             分析用途で使う可能性があるため削除せずに残す仕様とする（2021.08.14 決定事項）
             // Check if "info_data" file exists
             if fileManager.fileExists(atPath: infoDataFile.path) {
             try fileManager.removeItem(atPath: infoDataFile.path)
             } else {
             print("info_data File does not exist")
             }
             */
            
            // Check if "print_data" file exists
            if fileManager.fileExists(atPath: printDataFile.path) {
                try fileManager.removeItem(atPath: printDataFile.path)
            } else {
                print("print_data File does not exist")
            }
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
    
    /* ----- copy the Backup Data. Print file and Json files to Stored Folders ----- */
    func copyBackupData( _ targetProjectFolder: String, _ newProjectDisplayName: String = "" ){
        for item in m_projectDatas{
            if let projectData = item as? PosterDataSet {
                if projectData.projectFolder.subString("/", 3, false) == targetProjectFolder.subString("/", 3, false) {
                    let infoDataFile = URL(fileURLWithPath: targetProjectFolder).appendingPathComponent("items").appendingPathExtension("json")
                    
                    // json データのコピー（info_data）
                    if newProjectDisplayName.isEmpty {
                        copyFile( infoDataFile.path, m_InfoBackupFolderURL!.path, projectData.displayName, ".json" )
                    } else {
                        // 引数で指定された表示名称で保存
                        copyFile( infoDataFile.path, m_InfoBackupFolderURL!.path, newProjectDisplayName, ".json" )
                    }
                    
                    // jpeg データのコピー（print_data）
                    if newProjectDisplayName.isEmpty {
                        // 処理なし（CanvasViewController の責務であるため）
                    } else {
                        // 引数で指定された表示名称（作成日時）で保存
                        let srcFileName = URL(fileURLWithPath: targetProjectFolder).lastPathComponent
                        let printDataFile = m_ImageBackupFolderURL!.appendingPathComponent(srcFileName).appendingPathExtension("jpg")
                        copyFile( printDataFile.path, m_ImageBackupFolderURL!.path, newProjectDisplayName, ".jpg" )
                    }
                }
            }
        }
    }
    
    func addNewFolderDataSet(newFolder dataSet: FolderDataSet) {
        if m_FolderDataSets == nil {
            m_FolderDataSets = [FolderDataSet]()
        }
        m_FolderDataSets!.append(dataSet)
        FolderInfoSrializeObject.share().saveFolderDataSet(dataSet: m_FolderDataSets!)
    }
    
    func getFolderDateSets(target item: FolderDataSet) -> [DataSetBase]? {
        if let projectPathList = item.getChildrenProjectPathList() {
            m_projectDatas.removeAll()
            for folder in projectPathList {
                let folderName = getRealFile(targetPath: folder, withFile: false)
                if folderName != "" {
                    do{
                        let itemPaths = try FileManager.default.contentsOfDirectory(at: URL(string: folderName)!
                                                                                    , includingPropertiesForKeys: nil)
                        for fileUrl in itemPaths {
                            let subStrings = fileUrl.path.components(separatedBy: "/")
                            if ((checkTargetFolder(subStrings[subStrings.count - 2]) == true) && (subStrings[subStrings.count - 1] == "thumbnail.jpg") ){
                                let posterData = PosterDataSet(name: fileUrl.path, image: UIImage(contentsOfFile: fileUrl.path)!)
                                posterData.projectFolder = folderName
                                m_projectDatas.append(posterData)
                                break
                            }
                        }
                    } catch {
                        print(error)
                        return []
                    }
                }
            }
            m_projectDatas.sort { (PosterDataSet, PosterDataSet2) -> Bool in
                PosterDataSet.dateTime! > PosterDataSet2.dateTime!
            }
        }
        return m_projectDatas
    }
    
    /* ----- get document folder path ----- */
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    /* ----- get current time as String ----- */
    private func nowTimeStampToString() -> String {
        let formatDateTime = DateFormatter()
        formatDateTime.locale = Locale(identifier: "ja_JP")
        formatDateTime.dateStyle = .medium
        formatDateTime.timeStyle = .medium
        let now = Date()
        let dateTimeString = formatDateTime.string(from: now)
        let replaceString = dateTimeString.replace(":", "_", true).replace(" ", "_", true).replace("/", "_", true)
        return replaceString
    }
    
    /* ----- copy the target file to the specified Folder as original time stamp filename  ----- */
    private func copyFile(_ srcFilePath: String?, _ distDirPath: String?, _ titleName: String?, _ extesion: String? ){
        do{
            var isDirectory: ObjCBool = false
            let distFilePath = distDirPath!.appending("/" + titleName!.replace("年", "_", false).replace("月", "_", false).replace("日", "_", false).replace("時", "_", false).replace("分", "_", false).replace("秒","", false) + extesion!)
            if  FileManager.default.fileExists(atPath: distFilePath, isDirectory: &isDirectory) == true {
                try FileManager.default.removeItem(atPath: distFilePath)
            }
            print("src: " + srcFilePath!)
            print("dest: " + distFilePath)
            try FileManager.default.copyItem(atPath: srcFilePath! , toPath: distFilePath)
        }catch{
            print("File system error")
        }
    }
    
    private func checkTargetFolder( _ folderString: String ) -> Bool {
        let dateformat = DateFormatter()
        dateformat.dateFormat = "yyyy'_'MM'_'dd'_'HH'_'mm_ss'"
        let date = dateformat.date(from: folderString)
        let result:Bool
        if date == nil {
            result = false
        }else{
            result = true
        }
        return result
    }
    
    private func getAppSettingWithFolderManagement() -> Bool  {
        let userDefaults = UserDefaults.standard
        let value = userDefaults.bool(forKey: UserDefaults.Keys.enableFolder)
        return value
    }
}

/* ----- extension for String  ----- */
extension String {
    func replace(_ from: String,_ to: String,_ all: Bool) -> String {
        var replacedString = self
        
        while((replacedString.range(of: from)) != nil){
            if let range = replacedString.range(of: from) {
                replacedString.replaceSubrange(range, with: to)
                if all == false{
                    break
                }
            }
        }
        return replacedString
    }
    
    func subString(_ separater: String,  _ count: Int, _ fromFront: Bool) -> String {
        var newString: String = ""
        let splitsStrings = self.components(separatedBy: "/")
        var startIndex: Int = 0
        if( fromFront == true ){
            startIndex = count-1
        }else{
            startIndex = splitsStrings.count - count-1
        }
        for (index, parts) in splitsStrings.enumerated() {
            if index > startIndex {
                newString.append(contentsOf: parts+"/")
            }
        }
        return newString
    }
}
