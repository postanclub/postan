//
//  PosterDataSet.swift
//  Postan
//
//  Created by Takashi Aizawa on 2021/06/07.
//  Copyright © 2021 beginner. All rights reserved.
//

import Foundation
import UIKit

class PosterDataSet: DataSetBase{
    var thumbnail:UIImage
    var projectFolder:String!
    var fileName:String
    
    init(name:String, image:UIImage){
        self.thumbnail = image
        let subStrings = name.components(separatedBy: "/")
        self.fileName = subStrings[subStrings.count-1]
        let changeString = subStrings[subStrings.count-2] //　後ろから   ２つ目に日付が入っている
        let displayName = changeString.replace("_", "年", false).replace("_", "月", false).replace("_", "日", false).replace("_", "時", false).replace("_", "分", false) + "秒"
        let dateformat = DateFormatter()
        dateformat.dateFormat = "yyyy'_'MM'_'dd'_'HH'_'mm_ss'"
        let date = dateformat.date(from: changeString)
        super.init(name: name, date: date!, display: displayName)
        self.kindOfDataset = false
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    func clone() -> PosterDataSet{
        return PosterDataSet(name: self.name!, image: self.thumbnail)
    }
}
