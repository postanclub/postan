//
//  DataSetBase.swift
//  Postan
//
//  Created by Takashi Aizawa on 2021/06/07.
//  Copyright © 2021 beginner. All rights reserved.
//

import Foundation

class DataSetBase /*: Codable*/ {
    
    var name:String?
    
    var displayName:String?
    
    var dateTime: Date?
    
    var kindOfDataset: Bool?
    
    init(name:String?, date:Date?, display:String?) {
        self.name = name!
        self.dateTime = date!
        self.displayName = display
        self.kindOfDataset = true
    }
    
    init() {}
}
