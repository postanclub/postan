//
//  FolderDataSet.swift
//  Postan
//
//  Created by Takashi Aizawa on 2021/06/07.
//  Copyright © 2021 beginner. All rights reserved.
//

import Foundation

class FolderDataSet: DataSetBase, Codable {
    
    private var m_dataSets: [String]?
    
    enum CodingKeys: CodingKey {
        case m_dataSets
        case name
        case displayName
        case dateTime
        case kindOfDataset
    }
    
    required init(stringValue name:String) {
        super.init(name: name, date: Date(), display: name)
        m_dataSets = []
        self.kindOfDataset = true
    }
    
    required init(from decoder: Decoder) throws {
        super.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)
        displayName = try values.decode(String?.self, forKey: .displayName)
        kindOfDataset = try values.decode(Bool?.self, forKey: .kindOfDataset)
        name = try values.decode(String?.self, forKey: .name)
        dateTime = try values.decode(Date?.self, forKey: .dateTime)
        m_dataSets = try values.decode([String]?.self, forKey: .m_dataSets)
    }
    
    func encode(to encoder: Encoder) throws {
        var containerSingle = encoder.container(keyedBy: CodingKeys.self)
        try containerSingle.encode(self.displayName, forKey: .displayName)
        try containerSingle.encode(self.name, forKey: .name)
        try containerSingle.encode(self.kindOfDataset, forKey: .kindOfDataset)
        try containerSingle.encode(self.dateTime, forKey: .dateTime)
        try containerSingle.encode(self.m_dataSets, forKey: .m_dataSets)
    }
    
    func getChildrenProjectPathList() ->  [String]? {
        return self.m_dataSets
    }
    
    func addProjectPath(newProject path: String) {
        m_dataSets?.append(path)
    }
    
    func removeProjectPath(removeProject path: String) {
        if m_dataSets != nil {
            let dataMgr = DataManager.share()
            //let targetName = dataMgr.getRealFile(targetPath: path, withFile: false)
            for (i, item) in m_dataSets!.enumerated() {
                let folderName = dataMgr.getRealFile(targetPath: item, withFile: false)
                
                if inFolder(path, folderName) == true {
                    m_dataSets?.remove(at: i)
                }
            }
        }
    }
    
    func removeAll() {
        if m_dataSets != nil {
            m_dataSets?.removeAll()
        }
    }
    
    func compareFolder(_ folder: FolderDataSet) -> Bool {
        if (self.name == folder.name && self.dateTime == folder.dateTime
                && self.kindOfDataset == folder.kindOfDataset && self.m_dataSets!.count == folder.m_dataSets!.count) {
            return true
        }
        return false
    }
    
    private func inFolder(_ target: String, _ original: String) -> Bool {
        
        let subStrings = target.components(separatedBy: "/")
        let childFilePath = subStrings[subStrings.count-1]
        let targetPaths = original.components(separatedBy: "/")
        if childFilePath == targetPaths[targetPaths.count-1] {
            return true
        }
        return false
    }
}
