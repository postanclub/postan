//
//  ProjectCollectionViewCell.swift
//  Postan
//
//  Created by Takashi Aizawa on 2020/08/10.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

class ProjectCollectionViewCell: ProjectBaseCell {
    let kCellWidth = 320
    let kCellHight = 320
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        title.text = nil
        thumbnail.image = nil
    }
    
    /* ----- set the thumbnail data to cell image  ----- */
    func set(image:UIImage) {
        thumbnail.image = image
    }
    
    /* ----- set the date time string to cell title  ----- */
    func set(name:String) {
        title.text = name
    }
    
    @IBAction func callbackCellButton(_ sender: Any) {
        delegate?.returnedFromOtherViewController(self)
    }
}
