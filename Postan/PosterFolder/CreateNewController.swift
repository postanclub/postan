//
//  CreateNewController.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2021/02/11.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

enum PaperSize: Int {
    case A3
    case A4
}

enum PaperOrientation: Int {
    case portrait
    case landscape
}

// iPad 10.5  1668x2224 264ppi
// ipad 10.2  1620x2160 264ppi
fileprivate let iPadDpi: CGFloat = 264.0
// A3 200dpi 2339x3307ピクセル　1169.5x1653.5ポイント
fileprivate let fixedPortraitSize = CGSize(width: 1169.5, height: 1653.5)

class CreateNewPosterController: UIViewController {
    var parentViewControllerInfo: UpdateInfo!
    var isOK: Bool = false
    var canvasSize: CGSize!
    private var paperOrientation: PaperOrientation!
    @IBOutlet weak var selectOrientation: UISegmentedControl!
    
    static let oldCanvasDpi: CGFloat = 300.0                                    // 300dpi
    static let oldCanvasSize = CGSize(width: fixedPortraitSize.width * 3 / 2,
                                      height: fixedPortraitSize.height * 3 / 2)
    static let fixedCanvasDpi: CGFloat = 200.0
    static let defaultCanvasColor: UIColor = .white
    static var defaultCanvasSize: CGSize {
        let defaultPaperOrientation = UserDefaults.standard.integer(forKey: "default_paper_orientation")
        if defaultPaperOrientation == PaperOrientation.portrait.rawValue {
            return fixedPortraitSize
        } else {
            return CGSize(width: fixedPortraitSize.height, height: fixedPortraitSize.width)
        }
    }
    static func maximumScale(paperSize: PaperSize) -> CGFloat {
        if paperSize == .A3 {   // for A3
            return (iPadDpi / fixedCanvasDpi)
        } else {                // for A4
            return (iPadDpi / (fixedCanvasDpi * (fixedPortraitSize.height / fixedPortraitSize.width)))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paperOrientation = PaperOrientation(rawValue: UserDefaults.standard.integer(forKey: "default_paper_orientation"))
        selectOrientation.selectedSegmentIndex = paperOrientation.rawValue
        selectOrientation.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)], for: .normal)
    }
    
    @IBAction func changedOrientation(_ sender: Any) {
        paperOrientation = PaperOrientation(rawValue: selectOrientation.selectedSegmentIndex)
    }
    
    @IBAction func okButton(_ sender: Any) {
        isOK = true
        if paperOrientation == .portrait {
            canvasSize = fixedPortraitSize
        } else {
            canvasSize = CGSize(width: fixedPortraitSize.height,
                                height: fixedPortraitSize.width)
        }
        parentViewControllerInfo.returnedFromOtherViewController(self as Any?)
    }
    
    @IBAction func cancelButon(_ sender: Any) {
        isOK = false
        parentViewControllerInfo.returnedFromOtherViewController(self as Any?)
    }
}
