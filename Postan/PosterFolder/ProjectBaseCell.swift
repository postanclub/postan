//
//  ProjectBaseCell.swift
//  Postan
//
//  Created by Takashi Aizawa on 2021/06/12.
//  Copyright © 2021 beginner. All rights reserved.
//

import Foundation
import UIKit

protocol ProjectCollectionCellDelegate:AnyObject{
    func returnedFromOtherViewController(_ param: Any?)
}

class ProjectBaseCell: UICollectionViewCell {
    weak var delegate:ProjectCollectionCellDelegate?
}
