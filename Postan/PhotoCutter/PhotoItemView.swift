//
//  PhotoItemView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/09/01.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

struct PhotoItemData: Codable {
    let photoId: UUID
    let photoSize: UInt64
    let photoScale: CGFloat?
    let cuttingData: CuttingData
}

class PhotoItemView: DrawItemView {
    var cuttingData: CuttingData?
    override var isEditable: Bool { cuttingData != nil }
    private var photoId: UUID?
    private var photoSize: UInt64!
    private var photoScale: CGFloat!
    private var isPhotoChanged: Bool = false
    private var photoImageBuffer: UIImage?
    var photoImage: UIImage? {
        get {
            if photoImageBuffer != nil {
                return photoImageBuffer
            } else {
                if photoId != nil  && dataStore != nil {
                    return dataStore!.loadPhoto(id: photoId!, scale: photoScale)
                } else {
                    return nil
                }
            }
        }
        set(image) {
            photoImageBuffer = image
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, photoImage: UIImage?, cuttingData: CuttingData?, image: UIImage?) {
        super.init(frame: frame, type: .photo)
        self.photoImage = photoImage
        self.cuttingData = cuttingData
        self.image = image
        isPhotoChanged = true
    }
    
    override init(data: DrawItemData, dataStore: CanvasDataStore) {
        if let photoItemData = data.photoItemData {
            photoId = photoItemData.photoId
            photoSize = photoItemData.photoSize
            if photoItemData.photoScale != nil {
                photoScale = photoItemData.photoScale!
            } else {
                photoScale = 1.0
            }
            cuttingData = photoItemData.cuttingData
        }
        
        super.init(data: data, dataStore: dataStore)
        
        if let cuttingData = cuttingData {
            guard let photoId = photoId else {
                self.cuttingData = nil
                self.invalidData()
                return
            }
            
            DispatchQueue.global().async(group: dataStore.dispatchGroup) { [weak self] in
                guard let pImage = dataStore.loadPhoto(id: photoId, scale: self!.photoScale) else {
                    self!.photoId = nil
                    self!.cuttingData = nil
                    DispatchQueue.main.async { [weak self] in
                        self!.invalidData()
                        self!.setNeedsDisplay()
                    }
                    return
                }
                
                let cuttingTool = CuttingTool.create(cuttingData: cuttingData,
                                                     size: pImage.size,
                                                     originalSize: pImage.size)
                let pathInfo = cuttingTool.getPathInfo()
                let image = pImage.clip(frame: pathInfo.frame, path: pathInfo.path, angle: pathInfo.angle)
                
                DispatchQueue.main.async { [weak self] in
                    self!.image = image
                    self!.setNeedsDisplay()
                }
            }
        } else {
            if self.image == nil {
                self.invalidData()
            }
        }
    }
    
    override func convAndSave(dataStore: CanvasDataStore) -> DrawItemData {
        guard let cuttingData = cuttingData else {
            return super.convAndSave(dataStore: dataStore)
        }
        self.dataStore = dataStore
        
        rotateToZero()
        if isPhotoChanged {
            if photoId != nil {
                dataStore.deletePhoto(id: photoId!)
            }
            photoId = UUID()
            isPhotoChanged = false
        }
        if let photoImageBuffer = photoImageBuffer {
            photoSize = dataStore.savePhoto(image: photoImageBuffer, id: photoId!)
            photoScale = photoImageBuffer.scale
            self.photoImageBuffer = nil
        }
        
        let photoData = PhotoItemData(photoId: photoId!,
                                      photoSize: photoSize,
                                      photoScale: photoScale,
                                      cuttingData: cuttingData)
        
        let data = DrawItemData(type: self.type,
                                frame: self.frame,
                                angle: self.angle,
                                imageId: self.imageId,
                                imageSize: self.imageSize,
                                photoItemData: photoData)
        rotateToCurrent()
        return data
    }
    
    override func checkFileData(uuid: UUID) -> Bool {
        return uuid == photoId
    }
    
    override func deleteData() {
        super.deleteData()
        photoImageBuffer = nil
        if photoId != nil {
            self.dataStore!.deletePhoto(id: photoId!)
            photoId = nil
            photoSize = nil
            photoScale = nil
        }
        isPhotoChanged = false
    }
    
    override func copy(center: CGPoint? = nil) -> PhotoItemView {
        rotateToZero()
        let item = PhotoItemView(frame: frame,
                                 photoImage: photoImage,
                                 cuttingData: cuttingData,
                                 image: self.image)
        rotateToCurrent()
        if center != nil {
            item.center = center!
        }
        item.backgroundColor = self.backgroundColor
        item.photoScale = self.photoScale
        item.angle = self.angle
        item.rotateToCurrent()
        item.isPhotoChanged = true
        return item
    }
    
    override func copyForUndo() -> PhotoItemView {
        let item: PhotoItemView = self.copy()
        item.photoId = self.photoId
        item.photoSize = self.photoSize
        item.isPhotoChanged = false
        
        // 再編集不可の古いデータ形式に対応するため
        item.imageId = self.imageId
        item.imageSize = self.imageSize
        return item
    }
    
    func finishEditing(photoImage: UIImage?, cuttingData: CuttingData, image: UIImage) {
        if photoImage != nil {
            isPhotoChanged = true
            self.photoImage = photoImage
        }
        self.cuttingData = cuttingData
        self.image = image
        
        rotateToZero()
        let aspectRatio = self.image!.size.width / self.image!.size.height
        let orgCenter = self.center
        self.frame.size.height = self.frame.size.width / aspectRatio
        self.center = orgCenter
        rotateToCurrent()
        setNeedsDisplay()
    }
}
