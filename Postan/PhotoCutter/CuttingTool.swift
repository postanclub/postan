//
//  CuttingTool.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/06/26.
//  Copyright © 2020 beginner. All rights reserved.
//

import CoreGraphics
import UIKit

fileprivate let cuttingMinSize: CGFloat = 20
fileprivate let pointStride: Int = 2

enum CuttingToolType: Int, Codable {
    case rect
    case roundedRect
    case ellipse
    case star
    case heart
    case freehand
}

enum CuttingState {
    case operated
    case drawing
    case transforming
    case scaling
    case moving
    case rotating
}

enum DragDirection {
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
    case top
    case left
    case right
    case bottom
}

struct IntPoint: Codable {
    let x: Int
    let y: Int
}
struct CuttingData: Codable {
    let type: CuttingToolType
    let frame: CGRect?
    let angle: CGFloat?
    let points: String?
}

struct PathInfo {
    let frame: CGRect
    let path: UIBezierPath
    let angle: CGFloat
}

class CuttingTool {
    var toolType: CuttingToolType
    /*
     var rotationAngle: CGFloat { 0.0 }
     var rotationCenter: CGPoint { CGPoint.zero }
     var cutImageFrame: CGRect { CGRect() }
     */
    var size: CGSize!
    var originalSize: CGSize!
    var state: CuttingState = .operated
    var readyToMask: Bool = false
    var isReadyToMask: Bool { readyToMask }
    let lineWidth: CGFloat = 2
    let dashPattern: [CGFloat] = [5, 5]
    
    class func create(type: CuttingToolType,
                      size: CGSize,
                      originalSize: CGSize) -> CuttingTool {
        switch type {
        case .freehand:
            return CuttingPenTool(size: size, originalSize: originalSize)
        default:
            return CuttingRectTool(type: type, size: size, originalSize: originalSize)
        }
    }
    
    class func create(cuttingData: CuttingData,
                      size: CGSize,
                      originalSize: CGSize) -> CuttingTool {
        switch cuttingData.type {
        case .freehand:
            return CuttingPenTool(cuttingData: cuttingData, size: size, originalSize: originalSize)
        default:
            return CuttingRectTool(cuttingData: cuttingData, size: size, originalSize: originalSize)
        }
    }
    
    init(type: CuttingToolType, size: CGSize, originalSize: CGSize) {
        toolType = type
        self.size = size
        self.originalSize = originalSize
    }
    
    func draw() {
        pathStroke()
    }
    
    func touchEnded(point: CGPoint) {
        touchMoved(point: point)
        state = .operated
    }
    
    func getPathInfo() -> PathInfo {
        return PathInfo(frame: CGRect(), path: UIBezierPath(), angle: 0.0)
    }
    
    func getCuttingData() -> CuttingData {
        return CuttingData(type: self.toolType,
                           frame: CGRect(),
                           angle: nil,
                           points: nil)
    }
    func changeSize(_ newSize: CGSize) {}
    func touchBegan(point: CGPoint) {}
    func touchMoved(point: CGPoint) {}
    func pathStroke() {}
}

class CuttingRectTool: CuttingTool {
    private var lPoint: CGPoint { CGPoint(x: tlPoint.x,
                                          y: (blPoint.y - tlPoint.y) / 2 + tlPoint.y) }
    private var rPoint: CGPoint { CGPoint(x: trPoint.x,
                                          y: (blPoint.y - tlPoint.y) / 2 + tlPoint.y) }
    private var tPoint: CGPoint { CGPoint(x: (trPoint.x - tlPoint.x) / 2 + tlPoint.x,
                                          y: tlPoint.y) }
    private var bPoint: CGPoint { CGPoint(x: (trPoint.x - tlPoint.x) / 2 + tlPoint.x,
                                          y: blPoint.y) }
    private var tlPoint, trPoint, blPoint, brPoint: CGPoint
    private var rectCenter: CGPoint
    private var rectAngle: CGFloat = 0.0
    private var lastPoint: CGPoint!
    private var dragDirection: DragDirection!
    private let radiusRate: CGFloat = 0.10
    private let rectLineWidth: CGFloat = 1.0
    private let touchSpace: CGFloat = 20
    private let rectMargin: CGFloat = 5
    
    override init(type: CuttingToolType, size: CGSize, originalSize: CGSize) {
        rectCenter = CGPoint(x: size.width / 2, y: size.height / 2)
        let width = size.width
        let height = size.height
        tlPoint = CGPoint(x: rectCenter.x - width / 2, y: rectCenter.y - height / 2)
        trPoint = CGPoint(x: rectCenter.x + width / 2, y: rectCenter.y - height / 2)
        blPoint = CGPoint(x: rectCenter.x - width / 2, y: rectCenter.y + height / 2)
        brPoint = CGPoint(x: rectCenter.x + width / 2, y: rectCenter.y + height / 2)
        super.init(type: type, size: size, originalSize: originalSize)
        self.readyToMask = true
    }
    
    init(cuttingData: CuttingData, size: CGSize, originalSize: CGSize) {
        rectCenter = cuttingData.frame!.center
        rectAngle = cuttingData.angle!
        let width = cuttingData.frame!.width
        let height = cuttingData.frame!.height
        tlPoint = CGPoint(x: rectCenter.x - width / 2, y: rectCenter.y - height / 2)
        trPoint = CGPoint(x: rectCenter.x + width / 2, y: rectCenter.y - height / 2)
        blPoint = CGPoint(x: rectCenter.x - width / 2, y: rectCenter.y + height / 2)
        brPoint = CGPoint(x: rectCenter.x + width / 2, y: rectCenter.y + height / 2)
        super.init(type: cuttingData.type,
                   size: originalSize,
                   originalSize: originalSize)
        self.readyToMask = true
        changeSize(size)
    }
    
    override func getPathInfo() -> PathInfo {
        let scale = originalSize.width / size.width
        let rectSize = CGSize(width: abs(trPoint.x - tlPoint.x) * scale,
                              height: abs(blPoint.y - tlPoint.y) * scale)
        let rect = CGRect(origin: CGPoint(x: min(tlPoint.x, trPoint.x) * scale,
                                          y: min(tlPoint.y, blPoint.y) * scale),
                          size: rectSize)
        return PathInfo(frame: rect, path: createPath(toolType: toolType, rect: rect),
                        angle: self.rectAngle)
    }
    
    override func getCuttingData() -> CuttingData {
        let scale = originalSize.width / size.width
        let rectSize = CGSize(width: abs(trPoint.x - tlPoint.x) * scale,
                              height: abs(blPoint.y - tlPoint.y) * scale)
        let rect = CGRect(origin: CGPoint(x: min(tlPoint.x, trPoint.x) * scale,
                                          y: min(tlPoint.y, blPoint.y) * scale),
                          size: rectSize)
        
        return CuttingData(type: self.toolType,
                           frame: rect,
                           angle: self.rectAngle,
                           points: nil)
    }
    
    override func changeSize(_ newSize: CGSize) {
        if size.width != newSize.width {
            let scale = newSize.width / size.width
            size = newSize
            tlPoint = tlPoint.scale(scale)
            trPoint = trPoint.scale(scale)
            blPoint = blPoint.scale(scale)
            brPoint = brPoint.scale(scale)
            rectCenter = rectCenter.scale(scale)
        }
    }
    
    override func touchBegan(point: CGPoint) {
        let rotatedPoint = point.rotate(-rectAngle, center: rectCenter)
        
        if rotatedPoint.isNear(center: tlPoint, space: touchSpace) {
            dragDirection = .topLeft
            state = .scaling
        } else if rotatedPoint.isNear(center: trPoint, space: touchSpace) {
            dragDirection = .topRight
            state = .scaling
        } else if rotatedPoint.isNear(center: blPoint, space: touchSpace) {
            dragDirection = .bottomLeft
            state = .scaling
        } else if rotatedPoint.isNear(center: brPoint, space: touchSpace) {
            dragDirection = .bottomRight
            state = .scaling
        } else if rotatedPoint.isNear(center: rPoint, space: touchSpace) {
            dragDirection = .right
            state = .transforming
        } else if rotatedPoint.isNear(center: lPoint, space: touchSpace) {
            dragDirection = .left
            state = .transforming
        } else if rotatedPoint.isNear(center: tPoint, space: touchSpace) {
            dragDirection = .top
            state = .transforming
        } else if rotatedPoint.isNear(center: bPoint, space: touchSpace) {
            dragDirection = .bottom
            state = .transforming
        } else if rotatedPoint.isInside(rect: CGRect(origin: tlPoint,
                                                     size: CGSize(width: trPoint.x - tlPoint.x,
                                                                  height: blPoint.y - tlPoint.y))) {
            state = .moving
        } else {
            state = .rotating
        }
        lastPoint = point
    }
    
    private func singleDirectionScaling(point: CGPoint) {
        func conv(base: CGPoint) -> Bool {
            let width = trPoint.x - tlPoint.x
            let height = blPoint.y - tlPoint.y
            let old = lastPoint.rotate(-rectAngle, center: rectCenter)
            var newX = abs(base.x - new.x)
            var newY = abs(base.y - new.y)
            var oldX = abs(base.x - old.x)
            var oldY = abs(base.y - old.y)
            if newX / width > newY / height {
                newY = (newX / width) * height
            } else {
                newX = (newY / height) * width
            }
            if oldX / width > oldY / height {
                oldY = (oldX / width) * height
            } else {
                oldX = (oldY / height) * width
            }
            dx = newX - oldX
            dy = newY - oldY
            return (dx > 0) || (min(width + dx, height + dy) > cuttingMinSize * 2)
        }
        
        let new = point.rotate(-rectAngle, center: rectCenter)
        var dx: CGFloat = 0.0
        var dy: CGFloat = 0.0
        switch dragDirection {
        case .topLeft:
            let base = brPoint
            if base.x > new.x && base.y > new.y && conv(base: base) {
                tlPoint = tlPoint.translation(x: -dx, y: -dy)
                trPoint = trPoint.translation(x: 0, y: -dy)
                blPoint = blPoint.translation(x: -dx, y: 0)
            }
        case .topRight:
            let base = blPoint
            if base.x < new.x && base.y > new.y && conv(base: base) {
                tlPoint = tlPoint.translation(x: 0, y: -dy)
                trPoint = trPoint.translation(x: dx, y: -dy)
                brPoint = brPoint.translation(x: dx, y: 0)
            }
        case .bottomLeft:
            let base = trPoint
            if base.x > new.x && base.y < new.y && conv(base: base) {
                tlPoint = tlPoint.translation(x: -dx, y: 0)
                blPoint = blPoint.translation(x: -dx, y: dy)
                brPoint = brPoint.translation(x: 0, y: dy)
            }
        default: // .bottomRight
            let base = tlPoint
            if base.x < new.x && base.y < new.y && conv(base: base) {
                trPoint = trPoint.translation(x: dx, y: 0)
                blPoint = blPoint.translation(x: 0, y: dy)
                brPoint = brPoint.translation(x: dx, y: dy)
            }
        }
        reviseCenter()
    }
    
    private func singleDirectionTransforming(point: CGPoint) {
        let new = point.rotate(-rectAngle, center: rectCenter)
        let old = lastPoint.rotate(-rectAngle, center: rectCenter)
        let x = new.x - old.x
        let y = new.y - old.y
        let width = trPoint.x - tlPoint.x
        let height = blPoint.y - tlPoint.y
        switch dragDirection {
        case .left:
            if width - x > cuttingMinSize * 2 {
                tlPoint = tlPoint.translation(x: x, y: 0)
                blPoint = blPoint.translation(x: x, y: 0)
            }
        case .right:
            if width + x > cuttingMinSize * 2 {
                trPoint = trPoint.translation(x: x, y: 0)
                brPoint = brPoint.translation(x: x, y: 0)
            }
        case .top:
            if height - y > cuttingMinSize * 2 {
                tlPoint = tlPoint.translation(x: 0, y: y)
                trPoint = trPoint.translation(x: 0, y: y)
            }
        default:
            if height + y > cuttingMinSize * 2 {
                blPoint = blPoint.translation(x: 0, y: y)
                brPoint = brPoint.translation(x: 0, y: y)
            }
        }
        reviseCenter()
    }
    
    private func reviseCenter() {
        let hWidth = (trPoint.x - tlPoint.x) / 2
        let hHeight = (blPoint.y - tlPoint.y) / 2
        let newCenter = CGPoint(x: hWidth + tlPoint.x, y: hHeight + tlPoint.y)
        rectCenter = newCenter.rotate(rectAngle, center: rectCenter)
        tlPoint = CGPoint(x: rectCenter.x - hWidth, y: rectCenter.y - hHeight)
        trPoint = CGPoint(x: rectCenter.x + hWidth, y: rectCenter.y - hHeight)
        blPoint = CGPoint(x: rectCenter.x - hWidth, y: rectCenter.y + hHeight)
        brPoint = CGPoint(x: rectCenter.x + hWidth, y: rectCenter.y + hHeight)
    }
    
    override func touchMoved(point: CGPoint) {
        if state == .moving {
            let x = point.x - lastPoint.x
            let y = point.y - lastPoint.y
            tlPoint = tlPoint.translation(x:  x, y:  y)
            trPoint = trPoint.translation(x:  x, y:  y)
            blPoint = blPoint.translation(x:  x, y:  y)
            brPoint = brPoint.translation(x:  x, y:  y)
            rectCenter = rectCenter.translation(x: x, y: y)
        } else if state == .scaling {
            if singleDirectionHandle {
                singleDirectionScaling(point: point)
            } else {
                let lastDist = rectCenter.distance(lastPoint)
                let dist = rectCenter.distance(point)
                let scale: CGFloat = dist / lastDist
                tlPoint = tlPoint.scale(scale, center: rectCenter)
                trPoint = trPoint.scale(scale, center: rectCenter)
                blPoint = blPoint.scale(scale, center: rectCenter)
                brPoint = brPoint.scale(scale, center: rectCenter)
            }
        } else if state == .transforming {
            if singleDirectionHandle {
                singleDirectionTransforming(point: point)
            } else {
                if dragDirection == .left || dragDirection == .right {
                    let x: CGFloat
                    if dragDirection == .left {
                        x = lastPoint.rotate(-rectAngle, center: rectCenter).x
                            - point.rotate(-rectAngle, center: rectCenter).x
                    } else {
                        x = point.rotate(-rectAngle, center: rectCenter).x
                            - lastPoint.rotate(-rectAngle, center: rectCenter).x
                    }
                    tlPoint = tlPoint.translation(x: -x, y:  0)
                    trPoint = trPoint.translation(x:  x, y:  0)
                    blPoint = blPoint.translation(x: -x, y:  0)
                    brPoint = brPoint.translation(x:  x, y:  0)
                } else {
                    let y: CGFloat
                    if dragDirection == .top {
                        y = lastPoint.rotate(-rectAngle, center: rectCenter).y
                            - point.rotate(-rectAngle, center: rectCenter).y
                    } else {
                        y = point.rotate(-rectAngle, center: rectCenter).y
                            - lastPoint.rotate(-rectAngle, center: rectCenter).y
                    }
                    tlPoint = tlPoint.translation(x:  0, y: -y)
                    trPoint = trPoint.translation(x:  0, y: -y)
                    blPoint = blPoint.translation(x:  0, y:  y)
                    brPoint = brPoint.translation(x:  0, y:  y)
                }
            }
        } else { // .rotating
            var x = point.x - rectCenter.x
            var y = point.y - rectCenter.y
            let angle = atan2(y, x) * 360 / (2 * CGFloat.pi)
            x = lastPoint.x - rectCenter.x
            y = lastPoint.y - rectCenter.y
            rectAngle += angle - atan2(y, x) * 360 / (2 * CGFloat.pi)
        }
        lastPoint = point
    }
    
    override func pathStroke() {
        let begin = tlPoint.translation(x: rectMargin, y: rectMargin)
        let end = brPoint.translation(x: -rectMargin, y: -rectMargin)
        var rect = CGRect(x: begin.x, y: begin.y, width: end.x - begin.x, height: end.y - begin.y)
        
        func editRect(center: CGPoint) {
            UIColor.black.setStroke()
            var rect = CGRect(x: center.x - touchSpace / 2, y: center.y - touchSpace / 2,
                              width: touchSpace, height: touchSpace)
            var path = UIBezierPath(rect: rect)
            rotatePath(path)
            path.lineWidth = rectLineWidth
            path.stroke()
            UIColor.white.setStroke()
            rect = CGRect(x: rect.minX + rectLineWidth, y: rect.minY + rectLineWidth,
                          width: rect.width, height: rect.height)
            path = UIBezierPath(rect: rect)
            rotatePath(path)
            path.lineWidth = rectLineWidth
            path.stroke()
        }
        
        editRect(center: rPoint.translation(x: -rectMargin, y: 0))
        editRect(center: lPoint.translation(x: rectMargin, y: 0))
        editRect(center: tPoint.translation(x: 0, y: rectMargin))
        editRect(center: bPoint.translation(x: 0, y: -rectMargin))
        editRect(center: tlPoint.translation(x: rectMargin, y: rectMargin))
        editRect(center: trPoint.translation(x: -rectMargin, y: rectMargin))
        editRect(center: blPoint.translation(x: rectMargin, y: -rectMargin))
        editRect(center: brPoint.translation(x: -rectMargin, y: -rectMargin))
        UIColor.black.setStroke()
        var path = UIBezierPath(rect: rect)
        rotatePath(path)
        path.lineWidth = rectLineWidth
        path.stroke()
        UIColor.white.setStroke()
        rect = CGRect(x: rect.minX + rectLineWidth, y: rect.minY + rectLineWidth,
                      width: rect.width, height: rect.height)
        path = UIBezierPath(rect: rect)
        rotatePath(path)
        path.lineWidth = rectLineWidth
        path.stroke()
        
        func strokePart(dotted: Bool) {
            let path = createPath(toolType: toolType, rect: rect)
            rotatePath(path)
            path.lineWidth = lineWidth
            if dotted {
                path.setLineDash(dashPattern, count: dashPattern.count, phase: 1)
                UIColor.black.setStroke()
            } else {
                UIColor.white.setStroke()
            }
            path.stroke()
        }
        
        strokePart(dotted: false)
        strokePart(dotted: true)
    }
    
    func createPath(toolType: CuttingToolType, rect: CGRect) -> UIBezierPath {
        switch toolType {
        case .roundedRect:
            let radius = min(rect.width, rect.height) * radiusRate
            return UIBezierPath(roundedRect: rect, cornerRadius: radius)
        case .ellipse:
            return UIBezierPath(ovalIn: rect)
        case .star:
            return UIBezierPath(starIn: rect)
        case .heart:
            return UIBezierPath(heartIn: rect)
        default:
            return UIBezierPath(rect: rect)
        }
    }
    
    func rotatePath(_ path: UIBezierPath) {
        path.apply(CGAffineTransform(translationX: rectCenter.x, y: rectCenter.y).inverted())
        path.apply(CGAffineTransform(rotationAngle: (rectAngle * CGFloat.pi / 180.0)))
        path.apply(CGAffineTransform(translationX: rectCenter.x, y: rectCenter.y))
    }
}

class CuttingPenTool: CuttingTool {
    private var points: [CGPoint] = []
    private var counter: Int = 0
    
    init(size: CGSize, originalSize: CGSize) {
        super.init(type: .freehand, size: size, originalSize: originalSize)
    }
    
    init(cuttingData: CuttingData, size: CGSize, originalSize: CGSize) {
        super.init(type: .freehand,
                   size: originalSize,
                   originalSize: originalSize)
        decodePoints(str: cuttingData.points!)
        self.readyToMask = true
        changeSize(size)
    }
    
    override func getPathInfo() -> PathInfo {
        let scale = originalSize.width / size.width
        let path = UIBezierPath()
        path.move(to: points[0].scale(scale))
        if points.count > 1 {
            for i in 1 ..< points.count {
                path.addLine(to: points[i].scale(scale))
            }
        }
        path.close()
        return PathInfo(frame: path.bounds ,path: path, angle: 0.0)
    }
    
    override func getCuttingData() -> CuttingData {
        let scale = originalSize.width / size.width
        return CuttingData(type: .freehand,
                           frame: nil,
                           angle: nil,
                           points: encodePoints(scale: scale))
    }
    
    override func changeSize(_ newSize: CGSize) {
        if newSize.width != size.width {
            let scale = newSize.width / size.width
            size = newSize
            for i in 0 ..< points.count {
                points[i] = points[i].scale(scale)
            }
        }
    }
    
    override func touchBegan(point: CGPoint) {
        points.removeAll()
        counter = 0
        self.readyToMask = false
        touchMoved(point: point)
        state = .drawing
    }
    
    override func touchMoved(point: CGPoint) {
        counter += 1
        if counter % pointStride == 0 {
            points.append(point)
        }
    }
    
    override func pathStroke() {
        func strokePart(dotted: Bool) {
            let path = UIBezierPath()
            path.lineWidth = lineWidth
//            path.lineCapStyle = .round
//            path.lineJoinStyle = .round
            if dotted {
                path.setLineDash(dashPattern, count: dashPattern.count, phase: 1)
                UIColor.black.setStroke()
            } else {
                UIColor.white.setStroke()
            }
            
            let begin = points[0]
            path.move(to: begin)
            
            if points.count > 1 {
                for i in 1 ..< points.count {
                    let end = points[i]
                    path.addLine(to: end)
                }
            }
            if state != .drawing {
                path.close()
            }
            path.stroke()
        }
        
        if points != [] {
            if state != .drawing {
                if checkPoints() {
                    self.readyToMask = true
                } else {
                    points.removeAll()
                    return
                }
            }
            strokePart(dotted: false)
            strokePart(dotted: true)
        }
    }
    
    private func checkPoints() -> Bool {
        if points == [] {
            return false
        }
        var minX, maxX, minY, maxY: CGFloat
        minX = points[0].x; maxX = minX
        minY = points[0].y; maxY = minY
        for point in points {
            if point.x < minX { minX = point.x }
            if maxX < point.x { maxX = point.x }
            if point.y < minY { minY = point.y }
            if maxY < point.y { maxY = point.y }
            if ((maxX - minX) > cuttingMinSize) && ((maxY - minY) > cuttingMinSize) {
                return true
            }
        }
        return false
    }
    
    private func encodePoints(scale: CGFloat) -> String {
        var str: String = ""
        for p in points {
            if p.x < 0 {
                str = str + String(format: "-%x ", abs(Int(p.x * scale)))
            } else {
                str = str + String(format: "%x ", Int(p.x * scale))
            }
            if p.y < 0 {
                str = str + String(format: "-%x ", abs(Int(p.y * scale)))
            } else {
                str = str + String(format: "%x ", Int(p.y * scale))
            }
        }
        return str
    }
    
    private func decodePoints(str: String) {
        points = []
        let compArray = str.split(separator: " ")
        for index in stride(from: 0, to: compArray.count - 1, by: 2)  {
            let x = CGFloat(Int(compArray[index], radix: 16)!)
            let y = CGFloat(Int(compArray[index+1], radix: 16)!)
            points.append(CGPoint(x: x, y: y))
        }
    }
}

extension CGPoint {
    func scale(_ scale: CGFloat) -> CGPoint {
        return self.applying(CGAffineTransform(scaleX: scale, y: scale))
    }
    
    func scale(_ scale: CGFloat, center: CGPoint) -> CGPoint {
        let trans1 = CGAffineTransform(translationX: center.x, y: center.y).inverted()
        let trans2 = CGAffineTransform(scaleX: scale, y: scale)
        let trans3 = CGAffineTransform(translationX: center.x, y: center.y)
        return self.applying(trans1.concatenating(trans2).concatenating(trans3))
    }
    
    func rotate(_ angle: CGFloat, center: CGPoint) -> CGPoint {
        if angle != 0.0 {
            let trans1 = CGAffineTransform(translationX: center.x, y: center.y).inverted()
            let trans2 = CGAffineTransform(rotationAngle: (angle * CGFloat.pi / 180.0))
            let trans3 = CGAffineTransform(translationX: center.x, y: center.y)
            return self.applying(trans1.concatenating(trans2).concatenating(trans3))
        } else {
            return self
        }
    }
    
    func translation(x: CGFloat, y: CGFloat) -> CGPoint {
        return self.applying(CGAffineTransform(translationX: x, y: y))
    }
    
    func isNear(center: CGPoint, space: CGFloat) -> Bool {
        return (center.x - space <= self.x) && (self.x <= center.x + space)
            && (center.y - space <= self.y) && (self.y <= center.y + space)
    }
    
    func isInside(rect: CGRect) -> Bool {
        return (rect.minX <= self.x) && (self.x <= rect.maxX)
            && (rect.minY <= self.y) && (self.y <= rect.maxY)
    }
    
    func distance(_ point: CGPoint) -> CGFloat {
        let dx = point.x - self.x
        let dy = point.y - self.y
        return CGFloat(sqrtf(Float(dx * dx + dy * dy)))
    }
}

extension CGRect {
    func scale(_ scale: CGFloat) -> CGRect {
        let scaledRect = CGRect(x: self.origin.x * scale, y: self.origin.y * scale,
                                width: self.width * scale, height: self.height * scale)
        return scaledRect
    }
    
    func points(angle: CGFloat = 0, center: CGPoint? = nil) -> [CGPoint] {
        var points: [CGPoint] = []
        points.append(CGPoint(x: minX, y: minY))
        points.append(CGPoint(x: minX, y: maxY))
        points.append(CGPoint(x: maxX, y: maxY))
        points.append(CGPoint(x: maxX, y: minY))
        if angle != 0 {
            var rotatedPoints: [CGPoint] = []
            for point in points {
                rotatedPoints.append(point.rotate(angle, center: center!))
            }
            return rotatedPoints
        } else {
            return points
        }
    }
}

extension UIBezierPath {
    convenience init(arrow rect: CGRect, control: CGPoint) {
        self.init()
        let conX: CGFloat = ((control.x - rect.origin.x) / rect.width) * 100
        let upperY: CGFloat = ((control.y - rect.origin.y) / rect.height) * 100
        let lowerY: CGFloat = 100 - upperY
        let points: [CGPoint] = [fitRect(of: (0, upperY), in: rect),
                                 fitRect(of: (0, lowerY), in: rect),
                                 fitRect(of: (conX, lowerY), in: rect),
                                 fitRect(of: (conX, 100), in: rect),
                                 fitRect(of: (100, 50), in: rect),
                                 fitRect(of: (conX, 0), in: rect),
                                 fitRect(of: (conX, upperY), in: rect)]
        
        self.move(to: points[0])
        self.addLine(to: points[1])
        self.addLine(to: points[2])
        self.addLine(to: points[3])
        self.addLine(to: points[4])
        self.addLine(to: points[5])
        self.addLine(to: points[6])
        self.addLine(to: points[0])
        self.close()
    }
    
    convenience init(balloon0In rect: CGRect, base: CGPoint, whisker: CGPoint) {
        self.init()
        let baseX: CGFloat = ((base.x - rect.origin.x) / rect.width) * 100
        let baseY: CGFloat = ((base.y - rect.origin.y) / rect.height) * 100
        let offset0Y: CGFloat = (baseY / 100) * (baseX < 40 ? 40 - baseX : 0)
        let offset1Y: CGFloat = (baseY / 100) * (baseX > 60 ? 40 - (100 - baseX) : 0)
        let points: [CGPoint] = [fitRect(of: (50, 0), in: rect),
                                 fitRect(of: (0, baseY / 2), in: rect),
                                 fitRect(of: (baseX - 6, baseY - offset0Y * 0.4), in: rect),
                                 whisker,
                                 fitRect(of: (baseX + 6, baseY - offset1Y * 0.4), in: rect),
                                 fitRect(of: (100, baseY / 2), in: rect)]
        let controls: [CGPoint] = [fitRect(of: (0, 0), in: rect),
                                   fitRect(of: (0, baseY - offset0Y * 1.6), in: rect),
                                   fitRect(of: (100, baseY - offset1Y * 1.6), in: rect),
                                   fitRect(of: (100, 0), in: rect)]
        self.move(to: points[0])
        self.addQuadCurve(to: points[1], controlPoint: controls[0])
        self.addQuadCurve(to: points[2], controlPoint: controls[1])
        self.addLine(to: points[3])
        self.addLine(to: points[4])
        self.addQuadCurve(to: points[5], controlPoint: controls[2])
        self.addQuadCurve(to: points[0], controlPoint: controls[3])
        self.close()
    }
    
    convenience init(balloon1In rect: CGRect, base: CGPoint, whisker: CGPoint) {
        self.init()
        let baseX: CGFloat = ((base.x - rect.origin.x) / rect.width) * 100
        let baseY: CGFloat = ((base.y - rect.origin.y) / rect.height) * 100
        let points: [CGPoint] = [fitRect(of: (24, 0), in: rect),            // 0
                                 fitRect(of: (0, baseY * 0.24), in: rect),  // 1
                                 fitRect(of: (0, baseY * 0.76), in: rect),  // 2
                                 fitRect(of: (24, baseY), in: rect),        // 3
                                 fitRect(of: (baseX - 6, baseY), in: rect), // 4
                                 whisker,                                   // 5
                                 fitRect(of: (baseX + 6, baseY), in: rect), // 6
                                 fitRect(of: (76, baseY), in: rect),        // 7
                                 fitRect(of: (100, baseY * 0.76), in: rect),// 8
                                 fitRect(of: (100, baseY * 0.24), in: rect),// 9
                                 fitRect(of: (76, 0), in: rect)]            // 10
        let controls: [CGPoint] = [fitRect(of: (0, 0), in: rect),
                                   fitRect(of: (0, baseY), in: rect),
                                   fitRect(of: (100, baseY), in: rect),
                                   fitRect(of: (100, 0), in: rect)]
        self.move(to: points[0])
        self.addQuadCurve(to: points[1], controlPoint: controls[0])
        self.addLine(to: points[2])
        self.addQuadCurve(to: points[3], controlPoint: controls[1])
        self.addLine(to: points[4])
        self.addLine(to: points[5])
        self.addLine(to: points[6])
        self.addLine(to: points[7])
        self.addQuadCurve(to: points[8], controlPoint: controls[2])
        self.addLine(to: points[9])
        self.addQuadCurve(to: points[10], controlPoint: controls[3])
        self.addLine(to: points[0])
        self.close()
    }
    
    convenience init(heartIn rect: CGRect) {
        self.init()
        let bottomCenter: (CGFloat, CGFloat)     = ( 50, 100)
        let topCenter: (CGFloat, CGFloat)        = ( 50,  25)
        let leftSideControl: (CGFloat, CGFloat)  = (-45,  45)
        let leftTopControl: (CGFloat, CGFloat)   = ( 25, -20)
        let rightTopControl: (CGFloat, CGFloat)  = ( 75, -20)
        let rightSideControl: (CGFloat, CGFloat) = (145,  45)
        
        self.move(to: fitRect(of: bottomCenter, in: rect))
        
        self.addCurve(to: fitRect(of: topCenter, in: rect),
                      controlPoint1: fitRect(of: leftSideControl, in: rect),
                      controlPoint2: fitRect(of: leftTopControl, in: rect))
        self.addCurve(to: fitRect(of: bottomCenter, in: rect),
                      controlPoint1: fitRect(of: rightTopControl, in: rect),
                      controlPoint2: fitRect(of: rightSideControl, in: rect))
        self.close()
    }
    
    convenience init(starIn rect: CGRect) {
        let starPoints: [(CGFloat, CGFloat)] = [(50, 2), (35, 34), (0, 40),
                                                (25, 63), (21, 96), (50, 81), (79, 96),
                                                (75, 63), (100, 40), (65, 34),]
        self.init()
        setShape(points: starPoints, rect: rect)
    }
    
    private func setShape(points: [(CGFloat, CGFloat)], rect: CGRect) {
        points.forEach { point in
            if points.first! == point {
                self.move(to: fitRect(of: point, in: rect))
            } else {
                self.addLine(to: fitRect(of: point, in: rect))
            }
        }
        self.close()
    }
    
    private func fitRect(of percentage: (CGFloat, CGFloat), in rect: CGRect) -> CGPoint {
        return CGPoint(
            x: percentage.0 / 100 * rect.size.width + rect.origin.x,
            y: percentage.1 / 100 * rect.size.height + rect.origin.y
        )
    }
}
