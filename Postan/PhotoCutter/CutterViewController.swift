//
//  CutterViewController.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/06/26.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit
import AVFoundation

fileprivate let photoSizeLimit: CGFloat = 2600.0

class CutterViewController: UIViewController,
                            ImagePickerDelegate,
                            PanelControllerDelegate {
    var photoItemView: PhotoItemView?       // In / Out
    weak var delegate: PanelControllerDelegate?
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var toolTypeSelect: UISegmentedControl!
    private var confirmItemView: PhotoItemView!
    private var cuttingView: CuttingView?
    private var isPhotoButton: Bool = false
    private var isPhotoChanged: Bool = false
    private var imagePicker: ImagePicker!
    private let toolTypes: [CuttingToolType] = [.rect, .roundedRect, .ellipse, .star, .heart, .freehand]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photoView.isUserInteractionEnabled = true
        
        photoView.image = photoItemView!.photoImage
        let frame = AVMakeRect(aspectRatio: photoView.image!.size, insideRect: photoView.bounds)
        cuttingView = CuttingView(cuttingData: photoItemView!.cuttingData!,
                                  originalSize: photoView.image!.size)
        cuttingView!.changeFrame(frame)
        photoView.addSubview(cuttingView!)
        
        toolTypeSelect.selectedSegmentIndex = toolTypes.firstIndex(of: photoItemView!.cuttingData!.type)!
        toolTypeSelect.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18)],
                                              for: .normal)
    }
    
    // 回転時の処理
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let cuttingView = self.cuttingView {
            let frame = AVMakeRect(aspectRatio: photoView.image!.size, insideRect: photoView.bounds)
            cuttingView.changeFrame(frame)
        }
    }
    
    @IBAction func photoButton(_ sender: Any) {
        imagePicker = ImagePicker()
        imagePicker.delegate = self
        imagePicker.present(controller: self, count: 1)
    }
    
    func imagePickerFinish(image: UIImage?, type: ImageType) {
        if let image = image {
            if type == .png {
                displayAlert("PNG画像を編集すると透明部分が白色になります。\n編集を続けますか？",
                             cancel: true, okHandler: { (action) in
                                let coloredImage = image.fill(color: .white)
                                self.startPhotoCutting(photoImage: coloredImage)
                             })
            } else {
                if let adjustedImage = image.adjust() {
                    startPhotoCutting(photoImage: adjustedImage)
                }
            }
        }
    }
    
    func imagePickerFinish(image: UIImage?, type: ImageType, count: Int) {
    }
    
    private func startPhotoCutting(photoImage: UIImage) {
        if self.cuttingView != nil {
            self.cuttingView!.removeFromSuperview()
            self.cuttingView = nil
        }
        isPhotoChanged = true
        
        photoView.image = photoImage
        
        // frame : 写真のディスプレイ上の実サイズ（ポイント）
        let frame = AVMakeRect(aspectRatio: photoView.image!.size, insideRect: photoView.bounds)
        cuttingView = CuttingView(frame: frame,
                                  originalSize: photoView.image!.size,
                                  type: toolTypes[toolTypeSelect.selectedSegmentIndex])
        photoView.addSubview(cuttingView!)
    }
    
    
    @IBAction func toolChanged(_ sender: Any) {
        guard let cuttingView = cuttingView else { return }
        cuttingView.setToolType(toolTypes[toolTypeSelect.selectedSegmentIndex])
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        if cuttingView != nil {
            cuttingView!.removeFromSuperview()
            cuttingView = nil
        }
        photoItemView = nil
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmButton(_ sender: Any) {
        guard let cuttingView = cuttingView else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        guard let pathInfo = cuttingView.getPathInfo() else {
            return
        }
        
        let pImage = photoView.image!
        let maskedImage = pImage.clip(frame: pathInfo.frame, path: pathInfo.path, angle: pathInfo.angle)
        
        confirmItemView = PhotoItemView(frame: CGRect(),
                                        photoImage: photoView.image,
                                        cuttingData: cuttingView.getCuttingData(),
                                        image: maskedImage)
        
        let storyboard: UIStoryboard = self.storyboard!
        let confirmView: ConfirmViewController!
        confirmView = storyboard.instantiateViewController(withIdentifier: "ConfirmViewController")
            as? ConfirmViewController
        confirmView.delegate = self
        confirmView.modalPresentationStyle = .formSheet // .pageSheet, .fullScreen, .formSheet
        //      confirmView.isModalInPresentation = true
        confirmView.image = maskedImage
        present(confirmView, animated: true, completion: nil)
    }
    
    func panelControllerDidFinish(_ controller: UIViewController) {
        if let confirmView = controller as? ConfirmViewController {
            let isOK = confirmView.isOK
            confirmView.dismiss(animated: !isOK, completion: {
                if isOK {
                    let photoImage = self.isPhotoChanged ? self.confirmItemView.photoImage : nil
                    self.photoItemView!.finishEditing(photoImage: photoImage,
                                                      cuttingData: self.confirmItemView.cuttingData!,
                                                      image: self.confirmItemView.image!)
                    
                    self.cuttingView!.removeFromSuperview()
                    self.cuttingView = nil
                    self.delegate?.panelControllerDidFinish(self)
                }
            })
        }
    }
}

extension UIImage {
    func adjust() -> UIImage? {
        let sizeLimit: CGFloat = photoSizeLimit / self.scale
        if let image = self.orient() {
            if max(image.size.width, image.size.height) > sizeLimit {
                let width, height: CGFloat
                if image.size.width > image.size.height {
                    width = sizeLimit
                    height = image.size.height * (sizeLimit / image.size.width)
                } else {
                    width = image.size.width * (sizeLimit / image.size.height)
                    height = sizeLimit
                }
                return image.resize(size: CGSize(width: width, height: height))
            }
            return image
        }
        return nil
    }
    
    func resize(size resizedSize: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(resizedSize, false, self.scale)
        draw(in: CGRect(origin: .zero, size: resizedSize))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage
    }
    
    func orient() -> UIImage? {
        let orientation: CGImagePropertyOrientation
        let orientedCGImage: CGImage?
        
        switch (self.imageOrientation) {
        case .down:
            orientation = .down
        case .right:
            orientation = .right
        case .left:
            orientation = .left
        default:
            orientation = .up
        }
        if orientation != .up {
            let ciImage = CIImage(cgImage: self.cgImage!).oriented(orientation)
            let context = CIContext(options: nil)
            orientedCGImage = context.createCGImage(ciImage, from: ciImage.extent)
        } else {
            orientedCGImage = self.cgImage
        }
        if orientedCGImage == nil {
            return nil
        }
        return UIImage(cgImage: orientedCGImage!)
    }
    /*
     func mask(maskImage: UIImage) -> UIImage? {
     let maskCGImage = maskImage.cgImage!
     let mask = CGImage(maskWidth: maskCGImage.width,
     height: maskCGImage.height,
     bitsPerComponent: maskCGImage.bitsPerComponent,
     bitsPerPixel: maskCGImage.bitsPerPixel,
     bytesPerRow: maskCGImage.bytesPerRow,
     provider: maskCGImage.dataProvider!,
     decode: nil, shouldInterpolate: false)!
     
     guard let maskedCGImage = self.cgImage?.masking(mask) else {
     return nil
     }
     return UIImage(cgImage: maskedCGImage)
     }
     
     func crop(to: CGRect) -> UIImage? {
     /*
      var opaque = false
      if let cgImage = cgImage {
      switch cgImage.alphaInfo {
      case .noneSkipLast, .noneSkipFirst:
      opaque = true
      default:
      break
      }
      }
      */
     UIGraphicsBeginImageContextWithOptions(to.size, false, scale)
     draw(at: CGPoint(x: -to.origin.x, y: -to.origin.y))
     let result = UIGraphicsGetImageFromCurrentImageContext()
     UIGraphicsEndImageContext()
     return result
     }
     
     func rotate(center: CGPoint, angle: CGFloat) -> UIImage {
     let radian = angle * CGFloat.pi / 180
     UIGraphicsBeginImageContext(self.size)
     let context = UIGraphicsGetCurrentContext()!
     context.translateBy(x: center.x, y: center.y)
     context.scaleBy(x: 1.0, y: -1.0)
     
     context.rotate(by: radian)
     context.translateBy(x: -(center.x - self.size.width / 2),
     y: center.y - self.size.height / 2)
     context.draw(self.cgImage!, in: CGRect(x: -self.size.width / 2,
     y: -self.size.height / 2,
     width: self.size.width,
     height: self.size.height))
     
     let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()!
     UIGraphicsEndImageContext()
     return rotatedImage
     }
     */
    
    func fill(color: UIColor) -> UIImage {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let context = UIGraphicsGetCurrentContext()!
        
        context.setFillColor(color.cgColor)
        context.fill(rect)
        draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func clip(frame: CGRect, path: UIBezierPath, angle: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let center = frame.center
        let radian = angle * CGFloat.pi / 180
        if radian != 0.0 {
            path.apply(CGAffineTransform(translationX: center.x, y: center.y).inverted())
            path.apply(CGAffineTransform(rotationAngle: radian))
            path.apply(CGAffineTransform(translationX: center.x, y: center.y))
        }
        
        var context = UIGraphicsGetCurrentContext()!
        context.addPath(path.cgPath)
        context.clip(using: .winding)
        draw(in: CGRect(origin: .zero, size: size))
        let tmpImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        UIGraphicsBeginImageContextWithOptions(frame.size, false, scale)
        context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: frame.width / 2, y: frame.height / 2)
        context.scaleBy(x: 1.0, y: -1.0)
        if radian != 0.0 {
            context.rotate(by: radian)
        }
        context.translateBy(x: -(center.x - size.width / 2),
                            y: center.y - size.height / 2)
        let rect = CGRect(origin: CGPoint(x: -size.width / 2,
                                          y: -size.height / 2),
                          size: size)
        context.draw(tmpImage.cgImage!, in: rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    /*
     func clip(frame: CGRect, path: UIBezierPath, angle: CGFloat) -> UIImage {
     UIGraphicsBeginImageContextWithOptions(self.size, false, scale)
     
     if angle != 0.0 {
     let center = frame.center
     let radian = angle * CGFloat.pi / 180
     
     let context = UIGraphicsGetCurrentContext()!
     context.translateBy(x: center.x, y: center.y)
     context.scaleBy(x: 1.0, y: -1.0)
     
     context.rotate(by: radian)
     context.translateBy(x: -(center.x - self.size.width / 2),
     y: center.y - self.size.height / 2)
     context.draw(self.cgImage!, in: CGRect(x: -self.size.width / 2,
     y: -self.size.height / 2,
     width: self.size.width,
     height: self.size.height))
     
     let image = UIGraphicsGetImageFromCurrentImageContext()!
     UIGraphicsEndImageContext()
     
     return image.clip(frame: frame, path: path, angle: 0.0)
     } else {
     let context = UIGraphicsGetCurrentContext()!
     context.addPath(path.cgPath)
     context.clip(using: .winding)
     draw(in: CGRect(origin: .zero, size: size))
     
     let image = UIGraphicsGetImageFromCurrentImageContext()!
     UIGraphicsEndImageContext()
     
     let cropped = image.cgImage!.cropping(to: frame)!
     return UIImage(cgImage: cropped, scale: scale, orientation: imageOrientation)
     }
     }
     */
}
/*
 extension UIView {
 func getImage(size: CGSize) -> UIImage {
 UIGraphicsBeginImageContext(size)
 //UIGraphicsBeginImageContextWithOptions(size, false, 0.0)  // for Retina
 //        let context: CGContext = UIGraphicsGetCurrentContext()!
 //        self.layer.render(in: context)
 self.drawHierarchy(in: CGRect(origin: CGPoint(x:0, y:0), size: size),
 afterScreenUpdates: true)
 let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
 UIGraphicsEndImageContext()
 return image
 }
 }
 */
