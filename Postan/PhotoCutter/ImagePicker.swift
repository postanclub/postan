//
//  ImagePicker.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/10/01.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit
import PhotosUI
import Foundation

protocol ImagePickerDelegate: AnyObject {
    func imagePickerFinish(image: UIImage?, type: ImageType)
    func imagePickerFinish(image: UIImage?, type: ImageType, count: Int)
}

enum ImageType {
    case jpeg
    case png
}

class ImagePicker: NSObject,
                   UINavigationControllerDelegate,
                   UIImagePickerControllerDelegate,
                   PHPickerViewControllerDelegate {
    weak var delegate: ImagePickerDelegate!
    
    func present(controller: UIViewController, count: Int) {
        if #available(iOS 14.0, *) {
            var configuration = PHPickerConfiguration()
            configuration.filter = .images
            configuration.selectionLimit = count
            let picker = PHPickerViewController(configuration: configuration)
            picker.delegate = self
            controller.present(picker, animated: true)
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePickerController = UIImagePickerController()
                imagePickerController.sourceType = .photoLibrary
                imagePickerController.delegate = self
                controller.present(imagePickerController, animated: true, completion: nil)
            }
        }
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        delegate.imagePickerFinish(image: image, type: .jpeg)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        delegate.imagePickerFinish(image: nil, type: .jpeg)
    }
    
    @available(iOS 14, *)
    public func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true)
        
        var count = results.count
        if count == 0 {     // キャンセルの場合
            delegate.imagePickerFinish(image: nil, type: .jpeg)
            return
        }
        
        for result in results {
            let itemProvider = result.itemProvider
            let typeIdentifier = itemProvider.registeredTypeIdentifiers.first
            let imageType: ImageType
            switch typeIdentifier {
            case "public.png" :
                imageType = .png
            default : // "public.jpeg"
                imageType = .jpeg
            }
            if itemProvider.canLoadObject(ofClass: UIImage.self) {
                itemProvider.loadObject(ofClass: UIImage.self) { [weak self] image, error in
                    DispatchQueue.main.sync {
                        if let image = image as? UIImage {
                            if results.count == 1 {
                                self!.delegate.imagePickerFinish(image: image, type: imageType)
                            } else {
                                self!.delegate.imagePickerFinish(image: image, type: imageType, count: count)
                                count -= 1
                            }
                        }
                    }
                }
            }
        }
    }
}
