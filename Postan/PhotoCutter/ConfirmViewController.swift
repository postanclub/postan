//
//  ConfirmViewController.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/06/26.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

class ConfirmViewController: UIViewController {
    var image: UIImage!
    var message: String?
    var border: Bool = false
    var isOK: Bool = false
    weak var delegate: PanelControllerDelegate?
    
    @IBOutlet weak var confirmImage: UIImageView!
    @IBOutlet weak var messageView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if border {
            confirmImage.layer.borderWidth = 0.5
            confirmImage.layer.borderColor = UIColor.gray.cgColor
        }
        
        messageView.backgroundColor = .clear
        messageView.numberOfLines = 0
        if let image = image {
            confirmImage.image = image
        }
        if let message = message {
            messageView.text = message
        }
    }
    
    @IBAction func okButton(_ sender: Any) {
        isOK = true
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        isOK = false
        delegate?.panelControllerDidFinish(self)
    }
}
