//
//  CuttingView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/06/26.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

class CuttingView: UIView {
    private var cuttingTool: CuttingTool
    private var currentToolType: CuttingToolType
    private let originalSize: CGSize
    
    required init?(coder aDecoder: NSCoder) {
        cuttingTool = CuttingTool.create(type: .rect, size: CGSize(), originalSize: CGSize())
        currentToolType = .rect
        originalSize = CGSize()
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, originalSize: CGSize, type: CuttingToolType) {
        currentToolType = type
        self.originalSize = originalSize
        cuttingTool = CuttingTool.create(type: type,
                                         size: frame.size,
                                         originalSize: originalSize)
        super.init(frame: frame)
        self.backgroundColor = .clear
    }
    
    init(cuttingData: CuttingData, originalSize: CGSize) {
        let frame = CGRect(origin: CGPoint.zero, size: originalSize)
        cuttingTool = CuttingTool.create(cuttingData: cuttingData,
                                         size: originalSize,
                                         originalSize: originalSize)
        currentToolType = cuttingData.type
        self.originalSize = originalSize
        super.init(frame: frame)
        self.backgroundColor = .clear
    }
    
    override func draw(_ rect: CGRect) {
        cuttingTool.draw()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        cuttingTool.touchBegan(point: touch.location(in: self))
        setNeedsDisplay()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        cuttingTool.touchMoved(point: touch.location(in: self))
        setNeedsDisplay()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        cuttingTool.touchEnded(point: touch.location(in: self))
        setNeedsDisplay()
    }
    
    func getPathInfo() -> PathInfo? {
        if !cuttingTool.isReadyToMask {
            return nil
        }
        return cuttingTool.getPathInfo()
    }
    
    func getCuttingData() -> CuttingData {
        return cuttingTool.getCuttingData()
    }
    
    func changeFrame(_ rect: CGRect) {
        self.frame = rect
        cuttingTool.changeSize(frame.size)
        setNeedsDisplay()
    }
    
    func setToolType(_ type: CuttingToolType) {
        if (currentToolType != .freehand) && (type != .freehand) {
            cuttingTool.toolType = type
        } else {
            cuttingTool = CuttingTool.create(type: type,
                                             size: self.frame.size,
                                             originalSize: self.originalSize)
        }
        currentToolType = type
        setNeedsDisplay()
    }
}

