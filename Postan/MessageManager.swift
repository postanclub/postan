//
//  MessageManager.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2021/06/12.
//  Copyright © 2021 beginner. All rights reserved.
//

import Foundation

/**
 * ポップアップで使用するメッセージ一覧です
 *
 */
enum Message: Int, CaseIterable {
    case NoMessage
    case PrintError
    case PrintCancel
    case PrintSuccess
    case CopyPoster
    case DeletePoster
    case DuplicatePoster
    case DeleteFolder
    
    struct MessageData {
        var title: String
        var message: String
    }
    
    var info: MessageData {
        switch self {
        case .NoMessage:
            let messageData = MessageData(title: "", message:"")
            return messageData

        /**
         * プリントに関するメッセージ一覧です
         */
        case .PrintError:
            let messageData = MessageData(
                title: "印刷に失敗しました",
                message:"もう一度印刷してみてください。それでも印刷されない時はファイルアプリから印刷してください。")
            return messageData
        case .PrintCancel:
            let messageData = MessageData(
                title: "印刷を取り消しました",
                message:"")
            return messageData
        case .PrintSuccess:
            let messageData = MessageData(
                title: "印刷を実行しました",
                message:"")
            return messageData
            
        /**
         * ポスター管理に関するメッセージ一覧です
         */
        case .CopyPoster:
            let messageData = MessageData(
                title: "このポスターをコピーしますか？",
                message:"ファイル名は現在時刻となります。")
            return messageData
        case .DeletePoster:
            let messageData = MessageData(
                title: "このポスターを削除しますか？",
                message:"")
            return messageData
        case .DuplicatePoster:
            let messageData = MessageData(
                title: "フォルダ名が重複しています",
                message:"別のフォルダ名に変更してください。")
            return messageData
        case .DeleteFolder:
            let messageData = MessageData(
                title: "このフォルダを削除しますか？",
                message:"フォルダ内に保存されている全てのポスターが削除されます。ご注意ください。")
            return messageData
        }
    }
}
