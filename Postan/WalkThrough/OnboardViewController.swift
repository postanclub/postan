//
//  OnboardViewController.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2020/12/14.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit
import SwiftyOnboard
import SwiftGifOrigin

class OnboardViewController: UIViewController {
    
    private var walkThroughWindow: UIWindow!
    
    var swiftyOnboard: SwiftyOnboard!
    let bgColor: UIColor = #colorLiteral(red: 0.862745098, green: 0.5607843137, blue: 0.7725490196, alpha: 1)
    var gifImageView: [UIImageView] = []
    var gifFileCounts = 0
    var retentionPage = 0
    var stopAnimation = false
    
    // 自動回転 OFF で縦に固定（説明映像が見難いため）
    override var shouldAutorotate: Bool {
        UIDevice.current.setValue(1, forKey: "orientation")
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countBundleGifFiles()
        swiftyOnboard = SwiftyOnboard(frame: view.frame, style: .light)
        view.addSubview(swiftyOnboard)
        swiftyOnboard.dataSource = self
        swiftyOnboard.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        _ = self.initViewLayout
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private lazy var initViewLayout : Void = {
        swiftyOnboard.frame = self.view.frame
    }()
    
    private func countBundleGifFiles() {
        let fileManager = FileManager.default
        let bundleURL = Bundle.main.bundleURL
        let assetURL = bundleURL.appendingPathComponent("Gif.bundle")
        do {
            let contents = try fileManager.contentsOfDirectory(at: assetURL, includingPropertiesForKeys: [URLResourceKey.nameKey, URLResourceKey.isDirectoryKey], options: .skipsHiddenFiles)
            gifFileCounts = contents.count
        }
        catch let error as NSError {
            print(error)
        }
    }
    
    @objc func handleSkip() {
        let lastIndex = gifFileCounts - 1
        stopAnimation = true
        self.gifImageView[lastIndex].stopAnimating()
        swiftyOnboard?.goToPage(index: lastIndex, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.stopAnimation = false
            self.gifImageView[lastIndex].startAnimating()
            self.retentionPage = lastIndex
        }
    }
    
    @objc func handleContinue(sender: UIButton) {
        let index = sender.tag
        if index == (gifFileCounts - 1) {
            self.dismiss(animated: true, completion: nil)
        }
        swiftyOnboard?.goToPage(index: index + 1, animated: true)
    }
}

extension OnboardViewController: SwiftyOnboardDelegate, SwiftyOnboardDataSource {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        // Number of pages in the onboarding
        return gifFileCounts
    }
    
    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: SwiftyOnboard, atIndex index: Int) -> UIColor? {
        // Return the background color for the page at index
        return bgColor
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = SwiftyOnboardPage()
        
        // Set the image constraint
        self.view.addSubview(view)
        view.imageView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor, constant: 35).isActive = true
        view.imageView.heightAnchor.constraint(equalTo: self.view.layoutMarginsGuide.heightAnchor, multiplier: 0.9).isActive = true
        
        // Set the image on the page
        let bundleURL = Bundle.main.bundleURL
        let assetURL = bundleURL.appendingPathComponent("Gif.bundle")
        let file = UIImage.gif(url: assetURL.absoluteString.appending("movie\(index).gif"))
        view.imageView.image = file?.images?.first
        view.imageView.animationImages = file?.images
        view.imageView.animationDuration = file!.duration
        if index == 0 { view.imageView.startAnimating() }
        
        gifImageView.append(view.imageView)
        
        // Set the text in the page
        view.title.text = ""
        view.subTitle.text = ""
        
        // Return the page for the given index
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overlay = SwiftyOnboardOverlay()
        
        // Setup targets for the buttons on the overlay view
        overlay.skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        overlay.continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
        
        // Setup for the overlay buttons
        overlay.continueButton.setTitle("次へ", for: .normal)
        overlay.continueButton.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        overlay.skipButton.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        
        // Setup for the overlay page control
        overlay.pageControl.isUserInteractionEnabled = false
        
        // Return the overlay view
        return overlay
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let currentPage = round(position)
        overlay.pageControl.currentPage = Int(currentPage)
        overlay.continueButton.tag = Int(position)
        
        if (Int(position) != retentionPage && !stopAnimation) {
            gifImageView[Int(position)].stopAnimating()
            gifImageView[Int(position)].startAnimating()
            retentionPage = Int(position)
        }
        
        if currentPage == 0.0 || currentPage == 1.0 {
            overlay.continueButton.setTitle("次へ", for: .normal)
            overlay.skipButton.setTitle("Skip", for: .normal)
            overlay.skipButton.isHidden = false
        } else {
            overlay.continueButton.setTitle("閉じる", for: .normal)
            overlay.skipButton.isHidden = true
        }
        overlay.continueButton.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        overlay.skipButton.titleLabel?.font = UIFont.systemFont(ofSize: 18)
    }
}
