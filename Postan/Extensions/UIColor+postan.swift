//
//  UIColor+postan.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2021/07/15.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

extension UIColor {
    
    /**
     * ポスタン規定のピンク色を取得します。
     */
    class var postanPink: UIColor {
        return UIColor(red: 220/255.0, green: 143/255.0, blue: 197/255.0, alpha: 1.0)
    }
}
