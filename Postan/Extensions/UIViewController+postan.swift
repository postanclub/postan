//
//  UIViewController+postan.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2020/08/13.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// ローディングアニメーションを開始します。
    func startIndicator() {
        // ローディング画面の作成
        let loadingIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        loadingIndicator.center = self.view.center
        loadingIndicator.startAnimating()
        loadingIndicator.color = UIColor(
            red: 0.862,
            green: 0.560,
            blue: 197.0,
            alpha: 1.0
        ) // color (#DC8FC5)
        
        let grayOutView = UIView(frame: self.view.frame)
        grayOutView.backgroundColor = UIColor(
            red: 0.0,
            green: 0.0,
            blue: 0.0,
            alpha: 0.4
        )
        
        grayOutView.addSubview(loadingIndicator)
        self.view.addSubview(grayOutView)
    }
    
    /// ローディングアニメーションを停止します。
    func stopIndicator() {
        // ローディング画面が存在する場合は削除する
        if (self.view.getSubView(checkClass : UIActivityIndicatorView.self) != nil) {
            self.view.subviews.last?.removeFromSuperview()
        }
    }
    
}
