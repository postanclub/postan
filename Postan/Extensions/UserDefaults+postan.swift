//
//  UserDefaults+postan.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2021/06/19.
//  Copyright © 2021 beginner. All rights reserved.
//

import Foundation

extension UserDefaults
{
    struct Keys
    {
        /**
         * 端末情報に関する定義
         */
        static let postanUuid  = "postan_uuid"
        
        /**
         * ポスター管理に関する定義
         */
        static let enableFolder = "enable_folder"
        static let folderKey = "folder_key"
        
        /**
         * ポスター編集に関する定義
         */
        static let defaultPaperSize = "default_paper_size"
        static let touchOnItem = "touch_on_item"
        
    }
}
