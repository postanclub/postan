//
//  FileManager+postan.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2021/07/07.
//  Copyright © 2021 beginner. All rights reserved.
//

import Foundation

extension FileManager {
    func replaceWithCopyOfFile(at:URL, with:URL) throws -> Void {
        do {
            let url = try self.url(for: .itemReplacementDirectory, in: .userDomainMask, appropriateFor: with.deletingPathExtension(), create: true)
            try self.copyItem(at: with, to: url.appendingPathComponent(with.lastPathComponent))
            
            _ = try FileManager.default.replaceItemAt(at, withItemAt: url.appendingPathComponent(with.lastPathComponent))
            
            // removes whole temporary directory as a clean up
            try self.removeItem(at: url)
        }
    }
}
