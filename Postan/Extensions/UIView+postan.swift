//
//  UIView+postan.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2020/08/13.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

extension UIView {
    
    /// 指定クラスのオブジェクトを取得します。
    /// - Parameter checkClass: 取得対象クラス
    /// - Returns: 指定クラスのオブジェクト（存在しない場合は nil を返却）
    func getSubView(checkClass : AnyClass) -> AnyObject? {
        for subView in self.subviews {
            // その子のViewが引数のクラスだったらそのオブジェクトを返す
            if type(of: subView) == checkClass {
                return subView
            } else {
                // 違ったら下のViewを再起的にチェックし、見つかったらそのViewを返す
                if let view = subView.getSubView(checkClass : checkClass ) {
                    return view
                }
            }
        }
        return nil
    }
    
}
