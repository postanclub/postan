//
//  UIDevice+postan.swift
//  Postan
//
//  Created by Satoshi Ishizu on 2021/04/10.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

extension UIDevice {
    
    /**
     * デバイスコードを取得します。
     * モデルネームへの変換は enum 定義が必要であり、最新機器への追従が難しいためデバイスコード管理とします。
     */
    class var code: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let mirror = Mirror(reflecting: systemInfo.machine)
        let identifier = mirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToSimulator(identifier: String) -> String {
#if os(iOS)
            switch identifier {
            case "i386", "x86_64":  return "Simulator \(ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS")"
            default:                return identifier
            }
#endif
        }
        
        return mapToSimulator(identifier: identifier)
    }
    
    /**
     * デバイス UUID を取得します。
     * Bundle Identifier 単位で発行されるユニークな機器固有 ID です。
     */
    class var uuid: String {
        let uuid = UIDevice.current.identifierForVendor?.uuidString ?? ""
        return uuid
    }
    
    /**
     * 端末（iPad）に設定されたデバイス名称を取得します。
     * 設定App → 一般 → 名前（ここで設定した名称を表示）
     */
    class var devname: String {
        let name = UIDevice.current.name
        return name
    }
    
}
