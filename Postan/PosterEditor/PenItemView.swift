//
//  PenItemView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/09/22.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit
import PencilKit

struct PenItemData: Codable {
    let penDataId: UUID
    let penDataSize: UInt64
    let penRectSize: CGSize
}

class PenItemView: DrawItemView {
    override var isEditable: Bool { penData != nil }
    private var penDataId: UUID?
    private var penDataSize: UInt64!
    private var penRectSize: CGSize!
    private var penData: PKDrawing?
    private var isUpdated: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect) {
        super.init(frame: frame, type: .pen)
    }
    
    init(penItems: [PenItemView]) {
        super.init(frame: CGRect(), type: .pen)
        var drawing = penItems[0].transformPenData(rotation: true)
        for i in 1 ..< penItems.count {
            drawing.append(penItems[i].transformPenData(rotation: true))
        }
        rotateToZero()
        frame = drawing.bounds
        penData = zeroOrigin(drawing: drawing)
        isUpdated = true
        
        penRectSize = penData!.bounds.size
        image = penData!.image(from: penData!.bounds, scale: 2.0)
        setNeedsDisplay()
    }
    
    override init(data: DrawItemData, dataStore: CanvasDataStore) {
        if let penItemData = data.penItemData {
            penDataId = penItemData.penDataId
            penDataSize = penItemData.penDataSize
            penRectSize = penItemData.penRectSize
            penData = dataStore.loadPenData(id: penDataId!)
            super.init(data: data, dataStore: dataStore)
            if penData == nil {
                self.invalidData()
                return
            }
        } else {
            super.init(data: data, dataStore: dataStore)
            if data.drawing == nil {
                if image == nil {
                    self.invalidData()
                }
                return
            }
            deleteData()
            penData = zeroOrigin(drawing: data.drawing!)
            penRectSize = penData!.bounds.size
            penDataId = UUID()
            penDataSize = dataStore.savePenData(penData: penData!, id: penDataId!)
        }
        let drawing = transformPenData(rotation: false)
        image = drawing.image(from: drawing.bounds, scale: 2.0)
    }
    
    override func convAndSave(dataStore: CanvasDataStore) -> DrawItemData {
        if penData == nil {
            return super.convAndSave(dataStore: dataStore)
        }
        self.dataStore = dataStore
        
        rotateToZero()
        if isUpdated {
            if penDataId != nil {
                dataStore.deletePenData(id: penDataId!)
            }
            penDataId = UUID()
            isUpdated = false
        }
        penDataSize = dataStore.savePenData(penData: penData!, id: penDataId!)
        
        let penItemData = PenItemData(penDataId: penDataId!,
                                      penDataSize: penDataSize,
                                      penRectSize: penRectSize)
        
        let data = DrawItemData(type: self.type,
                                frame: self.frame,
                                angle: self.angle,
                                penItemData: penItemData)
        rotateToCurrent()
        return data
    }
    
    override func checkFileData(uuid: UUID) -> Bool {
        return uuid == penDataId
    }
    
    override func deleteData() {
        super.deleteData()
        penData = nil
        if penDataId != nil {
            self.dataStore!.deletePenData(id: penDataId!)
            penDataId = nil
            penDataSize = nil
            penRectSize = nil
        }
        isUpdated = false
    }
    
    override func copy(center: CGPoint? = nil) -> PenItemView {
        rotateToZero()
        let item = PenItemView(frame: frame)
        rotateToCurrent()
        if center != nil {
            item.center = center!
        }
        item.backgroundColor = self.backgroundColor
        item.image = self.image
        item.angle = self.angle
        item.rotateToCurrent()
        item.penRectSize = self.penRectSize
        item.penData = self.penData
        item.isUpdated = true
        return item
    }
    
    override func copyForUndo() -> PenItemView {
        let item: PenItemView = self.copy()
        item.penDataId = self.penDataId
        item.penDataSize = self.penDataSize
        item.isUpdated = false
        return item
    }
    
    func resumeDrawing() -> PKDrawing {
        image = nil
        let drawing = transformPenData(rotation: true)
        angle = 0.0
        rotateToZero()
        setNeedsDisplay()
        return drawing
    }
    
    func finishDrawing(drawing: PKDrawing) {
        penData = zeroOrigin(drawing: drawing)
        isUpdated = true
        
        penRectSize = penData!.bounds.size
        image = penData!.image(from: penData!.bounds, scale: 2.0)
        setNeedsDisplay()
    }
    
    private func transformPenData(rotation: Bool) -> PKDrawing {
        let x = center.x - penRectSize.width / 2
        let y = center.y - penRectSize.height / 2
        let scaleX = bounds.width / penRectSize.width
        let scaleY = bounds.height / penRectSize.height
        let trans0 = CGAffineTransform(translationX: x, y: y)
        let trans1 = CGAffineTransform(translationX: center.x, y: center.y).inverted()
        let trans2 = CGAffineTransform(scaleX: scaleX, y: scaleY)
        let trans3 = CGAffineTransform(rotationAngle:
                                        rotation ? angle * CGFloat.pi / 180.0 : 0)
        let trans4 = CGAffineTransform(translationX: center.x, y: center.y)
        var drawing = penData!.transformed(using: trans0)
        drawing = drawing.transformed(using: trans1.concatenating(trans2)
                                             .concatenating(trans3).concatenating(trans4))
        return drawing
    }
    
    private func zeroOrigin(drawing: PKDrawing) -> PKDrawing {
        let x = -drawing.bounds.minX
        let y = -drawing.bounds.minY
        let trans = CGAffineTransform(translationX: x, y: y)
        return drawing.transformed(using: trans)
    }
}
