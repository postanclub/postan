//
//  CanvasDataStore.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/08/23.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit
import PencilKit

fileprivate let itemsFileName: String = "items.json"
fileprivate let thumbnailFileName: String = "thumbnail.jpg"
fileprivate let photoFolderName: String = "photo_data"
fileprivate let pencilFolderName: String = "pencil_data"
fileprivate let illustFolderName: String = "illust_data"
fileprivate let textFolderName: String = "text_data"
fileprivate let tmpFolderName: String = "temp"
fileprivate let tmpPngFileName: String = "PNGイメージ.png"
fileprivate let photoJpegQuality: CGFloat = 0.74
fileprivate let printJpegQuality: CGFloat = 0.74
fileprivate let thumbnailJpegQuality: CGFloat = 0.70
fileprivate let dataFormatVersion: CGFloat = 3.0
fileprivate let thumbnailSize: CGFloat = 400

class CanvasProjectDataStore {
    private let folderUrl: URL
    private let printDataUrl: URL
    private let tmpFolderUrl = FileManager.default.temporaryDirectory
                                    .appendingPathComponent(tmpFolderName)

    init(folderUrl: URL, printDataUrl: URL) {
        self.folderUrl = folderUrl
        self.printDataUrl = printDataUrl
        
        // 旧バージョンの写真をイラストに変換するため
        let fileNames: [String]
        do {
            fileNames = try FileManager.default.contentsOfDirectory(atPath: folderUrl.path)
        } catch {
            print("Can't read directory (\(folderUrl.path))")
            return
        }
        
        let illustFolderUrl = folderUrl.appendingPathComponent(illustFolderName)
        for name in fileNames {
            let url = folderUrl.appendingPathComponent(name)
            if url.pathExtension == "png" {
                if !FileManager.default.fileExists(atPath: illustFolderUrl.path) {
                    do {
                        try FileManager.default.createDirectory(atPath: illustFolderUrl.path,
                                                                withIntermediateDirectories: true,
                                                                attributes: nil)
                    } catch {
                        print("Can't create directory (\(illustFolderUrl.path))")
                        return
                    }
                }
                let newUrl = illustFolderUrl.appendingPathComponent(name)
                do {
                    try FileManager.default.moveItem(at: url, to: newUrl)
                } catch {
                    print("Can't move file (\(url.path))")
                }
            }
        }
    }
    
    func deleteTmpFiles() {
        let fileNames: [String]
        do {
            fileNames = try FileManager.default.contentsOfDirectory(atPath: tmpFolderUrl.path)
        } catch {
            print("Can't read directory (\(tmpFolderUrl.path))")
            return
        }
        for name in fileNames {
            let url = tmpFolderUrl.appendingPathComponent(name)
            do {
                try FileManager.default.removeItem(at: url)
            } catch {
                print("Can't delete file (\(url.path))")
            }
        }
    }
    
    private func savePrintDataCommon(image: UIImage, url: URL) -> URL {
        let jpgImageData = image.jpegData(compressionQuality: printJpegQuality)
        do {
            try jpgImageData!.write(to: url, options: .atomic)
        } catch {
            fatalError("Can't write file (\(url.path))")
        }
        return url
    }
    
    func savePrintData(image: UIImage) -> URL {
        return savePrintDataCommon(image: image, url: printDataUrl)
    }
    
    func savePrintDataToTmp(image: UIImage) -> URL {
        let url = tmpFolderUrl.appendingPathComponent(printDataUrl.lastPathComponent)
        return savePrintDataCommon(image: image, url: url)
    }
    
    func savePngDataToTmp(image: UIImage) -> URL {
        let url = tmpFolderUrl.appendingPathComponent(tmpPngFileName)
        let pngImageData = image.pngData()
        do {
            try pngImageData!.write(to: url, options: .atomic)
        } catch {
            fatalError("Can't write file (\(url.path))")
        }
        return url
    }
    
    func saveThumbnail(image: UIImage, force: Bool) {
        let canvasSize = image.size
        let imageUrl = folderUrl.appendingPathComponent(thumbnailFileName)
        if !force && FileManager.default.fileExists(atPath: imageUrl.path) {
            return
        }
        
        let height: CGFloat
        let width: CGFloat
        
        if canvasSize.width < canvasSize.height {
            height = thumbnailSize
            width = canvasSize.width * (thumbnailSize / canvasSize.height)
        } else {
            width = thumbnailSize
            height = canvasSize.height * (thumbnailSize / canvasSize.width)
        }
        let resize = CGSize(width: width, height: height)
        if let thumbnailImage = image.resize(size: resize) {
            let jpgImageData = thumbnailImage.jpegData(compressionQuality: thumbnailJpegQuality)
            do {
                try jpgImageData!.write(to: imageUrl, options: .atomic)
            } catch {
                fatalError("Can't write file (\(imageUrl.path))")
            }
        }
    }
}

class CanvasDataStore {
    var dispatchGroup: DispatchGroup!
    private let folderUrl: URL
    
    struct CanvasData: Codable {
        var formatVersion: CGFloat? = dataFormatVersion
        var canvasSize: CGSize = CreateNewPosterController.oldCanvasSize
        var canvasDpi: CGFloat = CreateNewPosterController.oldCanvasDpi
        var canvasColor: Color?
        var canvasImageId: UUID?
        var canvasImageSize: UInt64?
        var drawItemDatas: [DrawItemData] = []
    }
    
    init(folderUrl: URL) {
        self.folderUrl = folderUrl
    }
    
    func cleanGarbage(drawItems: [DrawItemView]) {
        var photoItems: [DrawItemView] = []
        var pencilItems: [DrawItemView] = []
        var illustItems: [DrawItemView] = []
        var textItems: [DrawItemView] = []
        for item in drawItems {
            if item.type == .photo {
                photoItems.append(item)
            } else if item.type == .pen {
                pencilItems.append(item)
            } else if item.type == .illust {
                illustItems.append(item)
            } else if item.type == .text {
                textItems.append(item)
            }
        }
        deleteDataFiles(folderUrl: folderUrl.appendingPathComponent(photoFolderName), items: photoItems)
        deleteDataFiles(folderUrl: folderUrl.appendingPathComponent(pencilFolderName), items: pencilItems)
        deleteDataFiles(folderUrl: folderUrl.appendingPathComponent(illustFolderName), items: illustItems)
        deleteDataFiles(folderUrl: folderUrl.appendingPathComponent(textFolderName), items: textItems)
    }
    
    private func deleteDataFiles(folderUrl: URL, items: [DrawItemView]) {
        if FileManager.default.fileExists(atPath: folderUrl.path) {
            do {
                let fileUrls = try FileManager.default.contentsOfDirectory(at: folderUrl,
                                                                           includingPropertiesForKeys: nil)
                for fileUrl in fileUrls {
                    var found: Bool = false
                    if let uuid = UUID(uuidString: fileUrl.deletingPathExtension().lastPathComponent) {
                        for item in items {
                            if item.checkFileData(uuid: uuid) {
                                found = true
                                break
                            }
                        }
                    }
                    if !found {
                        do {
                            try FileManager.default.removeItem(at: fileUrl)
                        } catch {
                            print("Can't delete file (\(fileUrl.path))")
                        }
                    }
                }
            } catch {
                print ("Can't get directory information (\(folderUrl.path))")
            }
        }
    }
    
    func saveData(canvasView: CanvasView) {
        let url = folderUrl.appendingPathComponent(itemsFileName)
        var canvasData: CanvasData
        
        canvasData = CanvasData()
        canvasData.canvasSize = canvasView.canvasSize
        canvasData.canvasDpi = canvasView.canvasDpi
        canvasData.canvasColor = Color(uiColor: canvasView.backgroundColor!)
        if let canvasImage = canvasView.image {
            if canvasView.imageId == nil {
                let id = UUID()
                canvasView.imageId = id
                let imageUrl = folderUrl.appendingPathComponent(id.uuidString + ".jpg")
                let jpegImageData = canvasImage.jpegData(compressionQuality: photoJpegQuality)!
                do {
                    try jpegImageData.write(to: imageUrl, options: .atomic)
                } catch {
                    fatalError("Can't write file (\(imageUrl.path))")
                }
                do {
                    let attr = try FileManager.default.attributesOfItem(atPath: imageUrl.path)
                    canvasView.imageSize = attr[FileAttributeKey.size] as? UInt64
                } catch {
                    print("Can't read file attributes (\(imageUrl.path))")
                }
            }
            canvasData.canvasImageId = canvasView.imageId
            canvasData.canvasImageSize = canvasView.imageSize
        }
        
        for drawItem in canvasView.drawItems {
            canvasData.drawItemDatas.append(drawItem.convAndSave(dataStore: self))
        }
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let encoded = try! encoder.encode(canvasData)
        do {
            try encoded.write(to: url, options: .atomic)
        } catch {
            fatalError("Can't write file (\(url.path))")
        }
    }
    
    func loadData(canvasView: CanvasView) -> Bool {
        let url = folderUrl.appendingPathComponent(itemsFileName)
        var canvasData: CanvasData
        
        guard let string = try? String(contentsOfFile: url.path, encoding: .utf8) else {
            print("Can't read file (\(url.path))")
            return false
        }
        
        do {
            let tmp = try JSONDecoder().decode([DrawItemData].self, from: string.data(using: .utf8)!)
            canvasData = CanvasData()
            canvasData.drawItemDatas = tmp
        } catch {
            do {
                let tmp = try JSONDecoder().decode(CanvasData.self, from: string.data(using: .utf8)!)
                canvasData = tmp
            } catch {
                print("Can't decode JSON (\(url.path))")
                canvasView.canvasSize = CGSize.zero
                return false
            }
        }
        canvasView.canvasSize = canvasData.canvasSize
        canvasView.frame.size = canvasData.canvasSize
        canvasView.canvasDpi = canvasData.canvasDpi
        if let canvasColor = canvasData.canvasColor {
            canvasView.backgroundColor = canvasColor.uiColor
        }
        
        if let id = canvasData.canvasImageId {
            let imageUrl = folderUrl.appendingPathComponent(id.uuidString + ".jpg")
            if FileManager.default.fileExists(atPath: imageUrl.path) {
                if let data = try? Data(contentsOf: imageUrl) {
                    canvasView.image = UIImage(data: data)
                    canvasView.imageId = id
                    canvasView.imageSize = canvasData.canvasImageSize
                } else {
                    print("Can't read file (\(imageUrl.path))")
                }
            }
        }
        
        for itemData in canvasData.drawItemDatas {
            let item = DrawItemView.create(data: itemData, dataStore: self)
            canvasView.addItem(item, atLoading: true)
        }
        
        // 古いデータの場合、200dpiに変換
        let fixedCanvasDpi = CreateNewPosterController.fixedCanvasDpi
        if canvasView.canvasDpi != fixedCanvasDpi {
            let scale = fixedCanvasDpi / canvasView.canvasDpi
            canvasView.canvasSize = CGSize(width: canvasView.canvasSize.width * scale,
                                           height: canvasView.canvasSize.height * scale)
            canvasView.canvasDpi = fixedCanvasDpi
            
            for drawItem in canvasView.drawItems {
                drawItem.changeDpi(scale: scale)
            }
        }
        return true
    }
    
    func deleteCanvasImage(id: UUID) {
        let imageUrl = folderUrl.appendingPathComponent(id.uuidString + ".jpg")
        do {
            try FileManager.default.removeItem(at: imageUrl)
        } catch {
            print("Can't delete file (\(imageUrl.path))")
            return
        }
    }
    
    func saveImage(image: UIImage, id: UUID) -> UInt64 {
        let imageUrl = folderUrl.appendingPathComponent(id.uuidString + ".png")
        if !FileManager.default.fileExists(atPath: imageUrl.path) {
            let pngImageData = image.pngData()!
            do {
                try pngImageData.write(to: imageUrl, options: .atomic)
            } catch {
                fatalError("Can't write file (\(imageUrl.path))")
            }
        }
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: imageUrl.path)
            return attr[FileAttributeKey.size] as! UInt64
        } catch {
            print("Can't read file attributes (\(imageUrl.path))")
            return 0
        }
    }
    
    func loadImage(id: UUID) -> UIImage? {
        let imageUrl = folderUrl.appendingPathComponent(id.uuidString + ".png")
        if FileManager.default.fileExists(atPath: imageUrl.path) {
            guard let data = try? Data(contentsOf: imageUrl) else {
                print("Can't read file (\(imageUrl.path))")
                return nil
            }
            return UIImage(data: data)
            // 以下の方法だと元のサイズの全データを読み込まない
            //return UIImage(contentsOfFile: imageUrl.path)
        }
        return nil
    }
    
    func deleteImage(id: UUID) {
        let imageUrl = folderUrl.appendingPathComponent(id.uuidString + ".png")
        do {
            try FileManager.default.removeItem(at: imageUrl)
        } catch {
            print("Can't delete file (\(imageUrl.path))")
            return
        }
    }
    
    func saveIllust(image: UIImage, id: UUID) -> UInt64 {
        let illustFolderUrl = folderUrl.appendingPathComponent(illustFolderName)
        if !FileManager.default.fileExists(atPath: illustFolderUrl.path) {
            do {
                try FileManager.default.createDirectory(atPath: illustFolderUrl.path,
                                                        withIntermediateDirectories: true,
                                                        attributes: nil)
            } catch {
                fatalError("Can't create directory (\(illustFolderUrl.path))")
            }
        }
        
        let illustUrl = illustFolderUrl.appendingPathComponent(id.uuidString + ".png")
        if !FileManager.default.fileExists(atPath: illustUrl.path) {
            let pngImageData = image.pngData()!
            do {
                try pngImageData.write(to: illustUrl, options: .atomic)
            } catch {
                fatalError("Can't write file (\(illustUrl.path))")
            }
        }
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: illustUrl.path)
            return attr[FileAttributeKey.size] as! UInt64
        } catch {
            print("Can't read file attributes (\(illustUrl.path))")
            return 0
        }
    }
    
    func loadIllust(id: UUID) -> UIImage? {
        let illustFolderUrl = folderUrl.appendingPathComponent(illustFolderName)
        let illustUrl = illustFolderUrl.appendingPathComponent(id.uuidString + ".png")
        if FileManager.default.fileExists(atPath: illustUrl.path) {
            guard let data = try? Data(contentsOf: illustUrl) else {
                print("Can't read file (\(illustUrl.path))")
                return nil
            }
            return UIImage(data: data)
            // 以下の方法だと元のサイズの全データを読み込まない
            //return UIImage(contentsOfFile: illustUrl.path)
        }
        return nil
    }
    
    func deleteIllust(id: UUID) {
        let illustFolderUrl = folderUrl.appendingPathComponent(illustFolderName)
        let illustUrl = illustFolderUrl.appendingPathComponent(id.uuidString + ".png")
        do {
            try FileManager.default.removeItem(at: illustUrl)
        } catch {
            print("Can't delete file (\(illustUrl.path))")
            return
        }
    }
    
    func savePhoto(image: UIImage, id: UUID) -> UInt64 {
        let photoFolderUrl = folderUrl.appendingPathComponent(photoFolderName)
        if !FileManager.default.fileExists(atPath: photoFolderUrl.path) {
            do {
                try FileManager.default.createDirectory(atPath: photoFolderUrl.path,
                                                        withIntermediateDirectories: true,
                                                        attributes: nil)
            } catch {
                fatalError("Can't create directory (\(photoFolderUrl.path))")
            }
        }
        let photoUrl = photoFolderUrl.appendingPathComponent(id.uuidString + ".jpg")
        if !FileManager.default.fileExists(atPath: photoUrl.path) {
            let jpegImageData = image.jpegData(compressionQuality: photoJpegQuality)!
            do {
                try jpegImageData.write(to: photoUrl, options: .atomic)
            } catch {
                fatalError("Can't write file (\(photoUrl.path))")
            }
        }
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: photoUrl.path)
            return attr[FileAttributeKey.size] as! UInt64
        } catch {
            print("Can't read file attributes (\(photoUrl.path))")
            return 0
        }
    }
    
    func loadPhoto(id: UUID, scale: CGFloat) -> UIImage? {
        let photoFolderUrl = folderUrl.appendingPathComponent(photoFolderName)
        let photoUrl = photoFolderUrl.appendingPathComponent(id.uuidString + ".jpg")
        if FileManager.default.fileExists(atPath: photoUrl.path) {
            guard let data = try? Data(contentsOf: photoUrl) else {
                print("Can't read file (\(photoUrl.path))")
                return nil
            }
            return UIImage(data: data)
            // 必要な時にしか読まないし、UIImageが解放される場合もある
            //return UIImage(contentsOfFile: photoUrl.path)
        }
        return nil
    }
    
    func deletePhoto(id: UUID) {
        let photoFolderUrl = folderUrl.appendingPathComponent(photoFolderName)
        let photoUrl = photoFolderUrl.appendingPathComponent(id.uuidString + ".jpg")
        do {
            try FileManager.default.removeItem(at: photoUrl)
        } catch {
            print("Can't delete file (\(photoUrl.path))")
            return
        }
    }
    
    func savePenData(penData: PKDrawing, id: UUID) -> UInt64 {
        let pencilFolderUrl = folderUrl.appendingPathComponent(pencilFolderName)
        if !FileManager.default.fileExists(atPath: pencilFolderUrl.path) {
            do {
                try FileManager.default.createDirectory(atPath: pencilFolderUrl.path,
                                                        withIntermediateDirectories: true,
                                                        attributes: nil)
            } catch {
                fatalError("Can't create directory (\(pencilFolderUrl.path))")
            }
        }
        let pencilUrl = pencilFolderUrl.appendingPathComponent(id.uuidString + ".json")
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let encoded = try! encoder.encode(penData)
        if !FileManager.default.fileExists(atPath: pencilUrl.path) {
            do {
                try encoded.write(to: pencilUrl, options: .atomic)
            } catch {
                fatalError("Can't write file (\(pencilUrl.path))")
            }
        }
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: pencilUrl.path)
            return attr[FileAttributeKey.size] as! UInt64
        } catch {
            print("Can't read file attributes (\(pencilUrl.path))")
            return 0
        }
    }
    
    func loadPenData(id: UUID) -> PKDrawing? {
        let pencilFolderUrl = folderUrl.appendingPathComponent(pencilFolderName)
        let pencilUrl = pencilFolderUrl.appendingPathComponent(id.uuidString + ".json")
        if FileManager.default.fileExists(atPath: pencilUrl.path) {
            guard let string = try? String(contentsOfFile: pencilUrl.path, encoding: .utf8) else {
                print("Can't read file (\(pencilUrl.path))")
                return nil
            }
            
            do {
                let penData = try JSONDecoder().decode(PKDrawing.self, from: string.data(using: .utf8)!)
                return penData
            } catch {
                print("Can't decode JSON (\(pencilUrl.path))")
                return nil
            }
        }
        return nil
    }
    
    func deletePenData(id: UUID) {
        let pencilFolderUrl = folderUrl.appendingPathComponent(pencilFolderName)
        let pencilUrl = pencilFolderUrl.appendingPathComponent(id.uuidString + ".json")
        do {
            try FileManager.default.removeItem(at: pencilUrl)
        } catch {
            print("Can't delete file (\(pencilUrl.path))")
            return
        }
    }
    
    func saveTextData(textData: NSAttributedString?, id: UUID) -> UInt64 {
        let textFolderUrl = folderUrl.appendingPathComponent(textFolderName)
        if !FileManager.default.fileExists(atPath: textFolderUrl.path) {
            do {
                try FileManager.default.createDirectory(atPath: textFolderUrl.path,
                                                        withIntermediateDirectories: true,
                                                        attributes: nil)
            } catch {
                fatalError("Can't create directory (\(textFolderUrl.path))")
            }
        }
        let textUrl = textFolderUrl.appendingPathComponent(id.uuidString + ".rtf")
        
        if !FileManager.default.fileExists(atPath: textUrl.path) {
            if let text = textData {
                do {
                    let data = try text.data(from: .init(location: 0, length: text.length),
                                             documentAttributes: [.documentType: NSAttributedString.DocumentType.rtf])
                    try data.write(to: textUrl, options: .atomic)
                } catch {
                    fatalError("Can't write file (\(textUrl.path))")
                }
            } else {
                if !FileManager.default.createFile(atPath: textUrl.path,
                                                   contents: nil,
                                                   attributes: nil) {
                    fatalError("Can't create file (\(textUrl.path))")
                }
            }
        }
        
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: textUrl.path)
            return attr[FileAttributeKey.size] as! UInt64
        } catch {
            print("Can't read file attributes (\(textUrl.path))")
            return 0
        }
    }
    
    func loadTextData(id: UUID) -> NSAttributedString? {
        let textFolderUrl = folderUrl.appendingPathComponent(textFolderName)
        let textUrl = textFolderUrl.appendingPathComponent(id.uuidString + ".rtf")
        if FileManager.default.fileExists(atPath: textUrl.path) {
            do {
                let data = try NSAttributedString(url: textUrl,
                                                  options: [.documentType: NSAttributedString.DocumentType.rtf],
                                                  documentAttributes: nil)
                return data
            } catch {
                print("Can't read file (\(textUrl.path))")
                return nil
            }
        }
        return nil
    }
    
    func deleteTextData(id: UUID) {
        let textFolderUrl = folderUrl.appendingPathComponent(textFolderName)
        let textUrl = textFolderUrl.appendingPathComponent(id.uuidString + ".rtf")
        do {
            try FileManager.default.removeItem(at: textUrl)
        } catch {
            print("Can't delete file (\(textUrl.path))")
            return
        }
    }
}

struct Color: Codable {
    var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
    var uiColor: UIColor {
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    init(uiColor: UIColor) {
        uiColor.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
    }
}
