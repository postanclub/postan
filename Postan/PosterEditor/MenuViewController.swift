//
//  MenuViewController.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/07/11.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

enum MenuOperation {
    case noOperation
    case ruledLine
    case print
    case finish
    case fileExport
    case showWalkThrough
    case shareItemImage
    case sharePosterImage
}

enum BackgroundOperation {
    case noOperation
    case colorSelect
    case imageSelect
    case imageDelete
    case imageSet
}

class MenuViewController: UIViewController,
                          ColorPickerDelegate,
                          ImagePickerDelegate,
                          UIAdaptivePresentationControllerDelegate,
                          PanelControllerDelegate {
    var operation: MenuOperation = .noOperation
    var canvasView: CanvasView!
    weak var delegate: PanelControllerDelegate?
    private var bgColorPicker: ColorPicker!
    private var bgImagePicker: ImagePicker!
    private var originalBgColor: UIColor?
    @IBOutlet weak var paperSizeButtonView: UIButton!
    @IBOutlet weak var backgroundButtonView: UIButton!
    @IBOutlet weak var shareButtonView: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func paperSizeButton(_ sender: Any) {
        let storyboard: UIStoryboard = self.storyboard!
        let paperSizeController: PaperSizeViewController!
        paperSizeController
            = storyboard.instantiateViewController(withIdentifier: "PaperSizeViewController")
                as? PaperSizeViewController
        paperSizeController.delegate = self
        paperSizeController.modalPresentationStyle = .popover
        paperSizeController.preferredContentSize = CGSize(width: 140, height: 50)
        paperSizeController.popoverPresentationController?.sourceView = self.view
        paperSizeController.popoverPresentationController?.sourceRect = paperSizeButtonView.frame
        paperSizeController.popoverPresentationController?.permittedArrowDirections = .left
        paperSizeController.presentationController?.delegate = self
        paperSizeController.paperSize = canvasView.paperSize
        present(paperSizeController, animated: true, completion: nil)
    }
    
    @IBAction func changeBackgroundButton(_ sender: Any) {
        let storyboard: UIStoryboard = self.storyboard!
        let backgroundController: BackgroundViewController!
        backgroundController
            = storyboard.instantiateViewController(withIdentifier: "BackgroundViewController")
                as? BackgroundViewController
        backgroundController.delegate = self
        backgroundController.modalPresentationStyle = .popover
        backgroundController.preferredContentSize = CGSize(width: 170, height: 215)
        backgroundController.popoverPresentationController?.sourceView = self.view
        backgroundController.popoverPresentationController?.sourceRect = backgroundButtonView.frame
        backgroundController.popoverPresentationController?.permittedArrowDirections = .left
        backgroundController.presentationController?.delegate = self
        backgroundController.isDeleteBGImageButtonEnabled = canvasView.image != nil
        present(backgroundController, animated: true, completion: nil)
    }
    
    @IBAction func showWalkThroughButton(_ sender: Any) {
        operation = .showWalkThrough
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func ruledLineButton(_ sender: Any) {
        operation = .ruledLine
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func printButton(_ sender: Any) {
        operation = .print
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func shareButton(_ sender: Any) {
        let storyboard: UIStoryboard = self.storyboard!
        let shareController: ShareViewController!
        shareController
            = storyboard.instantiateViewController(withIdentifier: "ShareViewController")
                as? ShareViewController
        shareController.delegate = self
        shareController.modalPresentationStyle = .popover
        shareController.preferredContentSize = CGSize(width: 190, height: 110)
        shareController.popoverPresentationController?.sourceView = self.view
        shareController.popoverPresentationController?.sourceRect = shareButtonView.frame
        shareController.popoverPresentationController?.permittedArrowDirections = .left
        shareController.presentationController?.delegate = self
        shareController.canvasView = canvasView
        present(shareController, animated: true, completion: nil)
    }
    
    @IBAction func finishButton(_ sender: Any) {
        operation = .finish
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func fileExportButton(_ sender: Any) {
        operation = .fileExport
        delegate?.panelControllerDidFinish(self)
    }
    
    func panelControllerDidFinish(_ controller: UIViewController) {
        if let paperSizeController = controller as? PaperSizeViewController {
            let paperSize = paperSizeController.paperSize!
            paperSizeController.dismiss(animated: true, completion: {
                self.canvasView.changePaperSize(paperSize)
                self.delegate?.panelControllerDidFinish(self)
            })
        } else if let backgroundController = controller as? BackgroundViewController {
            let bgOperation = backgroundController.operation
            backgroundController.dismiss(animated: true, completion: {
                switch bgOperation {
                case .imageSet:
                    self.displayAlert("すべての描画アイテムが削除され背景として設定されます。\nよろしいですか？",
                                      cancel: true,
                                      cancelHandler: { (action) in
                                        self.delegate?.panelControllerDidFinish(self)
                                      },
                                      okHandler: { (action) in
                                        self.canvasView.setCanvasImage()
                                        self.delegate?.panelControllerDidFinish(self)
                                      })
                case .imageDelete:
                    self.canvasView.deleteCanvasImage()
                    self.delegate?.panelControllerDidFinish(self)
                case .imageSelect:
                    self.bgImagePicker = ImagePicker()
                    self.bgImagePicker.delegate = self
                    self.bgImagePicker.present(controller: self, count: 1)
                case .colorSelect:
                    self.bgColorPicker = ColorPicker(title: "背景色",
                                                     color: self.canvasView.backgroundColor!,
                                                     alpha: false,
                                                     view: self.view,
                                                     rect: self.backgroundButtonView.frame)
                    self.bgColorPicker.delegate = self
                    self.bgColorPicker.present(controller: self)
                default:
                    self.delegate?.panelControllerDidFinish(self)
                }
            })
        } else if let shareController = controller as? ShareViewController {
            operation = shareController.operation
            shareController.dismiss(animated: false, completion: {
                self.delegate?.panelControllerDidFinish(self)
            })
        }
    }
    
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        // モーダルが自動で消えたときの処理
        delegate?.panelControllerDidFinish(self)
    }
    
    func colorPickerDidSelectColor(_ uiColor: UIColor) {
        if originalBgColor == nil {
            originalBgColor = canvasView.backgroundColor
        }
        canvasView.backgroundColor = uiColor
        canvasView.setNeedsDisplay()
    }
    
    func colorPickerDidFinish() {
        if originalBgColor != nil {
            canvasView.changeCanvasColor(from: originalBgColor!,
                                         to: canvasView.backgroundColor!)
        }
        delegate?.panelControllerDidFinish(self)
    }
    
    func imagePickerFinish(image: UIImage?, type: ImageType) {
        if image != nil {
            canvasView.changeCanvasImage(image!)
        }
        bgImagePicker = nil
        delegate?.panelControllerDidFinish(self)
    }
    
    func imagePickerFinish(image: UIImage?, type: ImageType, count: Int) {
    }
}

class PaperSizeViewController: UIViewController {
    var paperSize: PaperSize!
    weak var delegate: PanelControllerDelegate?
    @IBOutlet weak var selectPaperSizeView: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectPaperSizeView.selectedSegmentIndex = paperSize.rawValue
        selectPaperSizeView.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)], for: .normal)
    }
    
    @IBAction func changedPaperSize(_ sender: Any) {
        paperSize = PaperSize(rawValue: selectPaperSizeView.selectedSegmentIndex)
        delegate?.panelControllerDidFinish(self)
    }
}

class ShareViewController: UIViewController {
    var canvasView: CanvasView!
    var operation: MenuOperation = .noOperation
    weak var delegate: PanelControllerDelegate?
    @IBOutlet weak var itemImageButtonView: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let item = canvasView.selectedItem {
            itemImageButtonView.isEnabled
                = item.type == .pen || item.type == .photo || item.type == .illust
        } else {
            itemImageButtonView.isEnabled = false
        }
    }
    
    @IBAction func shareItemImage(_ sender: Any) {
        operation = .shareItemImage
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func sharePosterImage(_ sender: Any) {
        operation = .sharePosterImage
        delegate?.panelControllerDidFinish(self)
    }
}

class BackgroundViewController: UIViewController {
    var isDeleteBGImageButtonEnabled: Bool = true
    var operation: BackgroundOperation = .noOperation
    weak var delegate: PanelControllerDelegate?
    @IBOutlet weak var deleteBGImageView: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !isDeleteBGImageButtonEnabled {
            deleteBGImageView.isEnabled = false
        }
    }
    
    @IBAction func changeBGColor(_ sender: Any) {
        operation = .colorSelect
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func changeBGImage(_ sender: Any) {
        operation = .imageSelect
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func deleteBGImage(_ sender: Any) {
        operation = .imageDelete
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func setBGImage(_ sender: Any) {
        operation = .imageSet
        delegate?.panelControllerDidFinish(self)
    }
}
