//
//  IllustItemView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2021/03/09.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

struct IllustItemData: Codable {
    let illustId: UUID
    let illustSize: UInt64
}

class IllustItemView: DrawItemView {
    override var isEditable: Bool { true }
    private var illustId: UUID?
    private var illustSize: UInt64!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, image: UIImage?) {
        super.init(frame: frame, type: .illust)
        self.image = image
    }
    
    override init(data: DrawItemData, dataStore: CanvasDataStore) {
        if let illustItemData = data.illustItemData {
            illustId = illustItemData.illustId
            illustSize = illustItemData.illustSize
            let illustImage = dataStore.loadIllust(id: illustId!)
            super.init(data: data, dataStore: dataStore)
            if illustImage == nil {
                self.invalidData()
                return
            }
            self.image = illustImage
        } else {
            super.init(data: data, dataStore: dataStore)
        }
    }
    
    override func convAndSave(dataStore: CanvasDataStore) -> DrawItemData {
        self.dataStore = dataStore
        
        rotateToZero()
        if image != nil {
            if illustId == nil {
                illustId = UUID()
            }
            illustSize = dataStore.saveIllust(image: image!, id: illustId!)
        }
        
        let illustItemData = IllustItemData(illustId: illustId!, illustSize: illustSize)
        
        let data = DrawItemData(type: self.type,
                                frame: self.frame,
                                angle: self.angle,
                                imageId: self.imageId,
                                imageSize: self.imageSize,
                                illustItemData: illustItemData)
        rotateToCurrent()
        return data
    }
    
    override func checkFileData(uuid: UUID) -> Bool {
        return uuid == illustId
    }
    
    override func deleteData() {
        super.deleteData()
        if illustId != nil {
            dataStore!.deleteIllust(id: illustId!)
            illustId = nil
            illustSize = nil
        }
    }
    
    override func getImage() -> UIImage? {
        return image
    }
    
    override func copy(center: CGPoint? = nil) -> IllustItemView {
        rotateToZero()
        let item = IllustItemView(frame: self.frame, image: self.image)
        rotateToCurrent()
        if center != nil {
            item.center = center!
        }
        item.backgroundColor = self.backgroundColor
        item.angle = self.angle
        item.rotateToCurrent()
        return item
    }
    
    override func copyForUndo() -> DrawItemView {
        let item: IllustItemView = self.copy()
        item.illustId = self.illustId
        item.illustSize = self.illustSize
        return item
    }
}
