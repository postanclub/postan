//
//  DrawItemView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/06/26.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit
import PencilKit

fileprivate let penInvalidColor = UIColor(red: 0.9, green: 0.7, blue: 0.6, alpha: 0.8)
fileprivate let photoInvalidColor = UIColor(red: 0.5, green: 0.7, blue: 0.9, alpha: 0.8)
fileprivate let illustInvalidColor = UIColor(red: 0.6, green: 0.9, blue: 0.7, alpha: 0.8)
fileprivate let drawItemMinSize: CGFloat = 30.0

enum ItemType: Int, Codable {
    case photo
    case illust
    case figure
    case pen
    case text
}

struct DrawItemData: Codable {
    let type: ItemType
    let frame: CGRect
    let angle: CGFloat
    let imageId: UUID?
    let imageSize: UInt64?          // for log data
    let controls: [CGPoint]?
    let originalFrame: CGRect?      // deprecated
    let drawing: PKDrawing?         // deprecated
    let figureItemData: FigureItemData?
    let photoItemData: PhotoItemData?
    let penItemData: PenItemData?
    let illustItemData: IllustItemData?
    let textItemData: TextItemData?
    
    init(type: ItemType, frame: CGRect, angle: CGFloat,
         imageId: UUID? = nil,
         imageSize: UInt64? = nil,
         controls: [CGPoint]? = nil,
         originalFrame: CGRect? = nil,
         drawing: PKDrawing? = nil,
         figureItemData: FigureItemData? = nil,
         photoItemData: PhotoItemData? = nil,
         penItemData: PenItemData? = nil,
         illustItemData: IllustItemData? = nil,
         textItemData: TextItemData? = nil) {
        self.type = type
        self.frame = frame
        self.angle = angle
        self.imageId = imageId
        self.imageSize = imageSize
        self.controls = controls
        self.originalFrame = originalFrame
        self.drawing = drawing
        self.figureItemData = figureItemData
        self.photoItemData = photoItemData
        self.penItemData = penItemData
        self.illustItemData = illustItemData
        self.textItemData = textItemData
    }
}

class DrawItemView: UIView {
    let type: ItemType
    var dataStore: CanvasDataStore?
    let isFixedAspectRatio: Bool
    var isEditable: Bool { false }
    var angle: CGFloat = 0.0
    var image: UIImage?
    var imageId: UUID?
    var imageSize: UInt64?          // for log data
    var controls: [CGPoint]?
    private var animeTimer: Timer?
    
    class func create(data: DrawItemData, dataStore: CanvasDataStore) -> DrawItemView {
        switch data.type {
        case .figure:
            return FigureItemView(data: data, dataStore: dataStore)
        case .photo:
            if data.imageId != nil {
                let illustData = IllustItemData(illustId: data.imageId!,
                                                illustSize: data.imageSize!)
                let newData = DrawItemData(type: .illust,
                                           frame: data.frame,
                                           angle: data.angle,
                                           illustItemData: illustData)
                return IllustItemView(data: newData, dataStore: dataStore)
            } else {
                return PhotoItemView(data: data, dataStore: dataStore)
            }
        case .pen:
            return PenItemView(data: data, dataStore: dataStore)
        case .illust:
            return IllustItemView(data: data, dataStore: dataStore)
        case .text:
            return TextItemView(data: data, dataStore: dataStore)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.type = .photo
        self.isFixedAspectRatio = true
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, type: ItemType) {
        self.type = type
        if (type == .photo) || (type == .illust) {
            self.isFixedAspectRatio = true
        } else {
            self.isFixedAspectRatio = false
        }
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
        self.contentMode = .scaleToFill
        self.backgroundColor = .clear
        self.setNeedsDisplay()
    }
    
    init(data: DrawItemData, dataStore: CanvasDataStore) {
        self.type = data.type
        self.dataStore = dataStore
        if (data.type == .photo) || (data.type == .illust) {
            self.isFixedAspectRatio = true
        } else {
            self.isFixedAspectRatio = false
        }
        self.angle = data.angle
        self.imageId = data.imageId
        self.imageSize = data.imageSize
        super.init(frame: data.frame)
        self.isUserInteractionEnabled = true
        self.contentMode = .scaleToFill
        self.backgroundColor = .clear
        self.setNeedsDisplay()
        
        if imageId != nil {
            self.image = dataStore.loadImage(id: imageId!)
        }
        self.transform = CGAffineTransform(rotationAngle: (self.angle * CGFloat.pi / 180.0))
    }
    
    override func draw(_ rect: CGRect) {
        if let image = image {
            image.draw(in: rect)
        }
    }
    
    func convAndSave(dataStore: CanvasDataStore) -> DrawItemData {
        self.dataStore = dataStore
        
        rotateToZero()
        if image != nil {
            if imageId == nil {
                imageId = UUID()
            }
            imageSize = dataStore.saveImage(image: image!, id: imageId!)
        }
        let data = DrawItemData(type: self.type,
                                frame: self.frame,
                                angle: self.angle,
                                imageId: imageId,
                                imageSize: imageSize)
        rotateToCurrent()
        return data
    }
    
    func invalidData() {
        if type == .pen {
            self.backgroundColor = penInvalidColor
        } else if type == .photo {
            self.backgroundColor = photoInvalidColor
        } else if type == .illust {
            self.backgroundColor = illustInvalidColor
        }
    }
    
    func checkFileData(uuid: UUID) -> Bool {
        return false
    }
    
    func deleteData() {
        image = nil
        if imageId != nil {
            dataStore!.deleteImage(id: imageId!)
            imageId = nil
            imageSize = nil
        }
    }
    
    func getImage() -> UIImage? {
        return image
    }
    
    func copy(center: CGPoint? = nil) -> DrawItemView {
        rotateToZero()
        let item = DrawItemView(frame: frame, type: self.type)
        rotateToCurrent()
        if center != nil {
            item.center = center!
        }
        item.backgroundColor = self.backgroundColor
        item.image = self.image
        item.angle = self.angle
        item.rotateToCurrent()
        return item
    }
    
    func copyForUndo() -> DrawItemView {
        return copy()
    }
    
    func move(deltaX: CGFloat, deltaY: CGFloat, bounds: CGSize? = nil) {
        var point = CGPoint()
        point.x = center.x + deltaX
        point.y = center.y + deltaY
        if let bounds = bounds {
            if point.x < 0 {
                point.x = 0
            } else if point.x > bounds.width {
                point.x = bounds.width
            }
            if point.y < 0 {
                point.y = 0
            } else if point.y > bounds.height {
                point.y = bounds.height
            }
        }
        center = point
    }
    
    func rotate(angle: CGFloat) {
        self.angle += angle
        self.transform = CGAffineTransform(rotationAngle: (self.angle * CGFloat.pi / 180.0))
    }
    
    func scale(x: CGFloat, y: CGFloat) {
        rotateToZero()
        let oldCenter = center
        frame = CGRect(origin: frame.origin, size: CGSize(width: frame.width * x,
                                                          height: frame.height * y))
        center = oldCenter
        if let controls = self.controls {
            var newcontrols: [CGPoint] = []
            for control in controls {
                let newX = (control.x - bounds.width / 2) * x + bounds.width * x / 2
                let newY = (control.y - bounds.height / 2) * y + bounds.height * y / 2
                newcontrols.append(CGPoint(x: newX, y: newY))
            }
            self.controls = newcontrols
        }
        rotateToCurrent()
        self.setNeedsDisplay()
    }
    
    func scale(direction: Direction, point: CGPoint) {
        func conv(base: CGPoint) -> Bool {
            dx = abs(base.x - new.x)
            dy = abs(base.y - new.y)
            if dx / frame.width > dy / frame.height {
                dy = (dx / frame.width) * frame.height
            } else {
                dx = (dy / frame.height) * frame.width
            }
            if min(dx, dy) > drawItemMinSize {
                sx = dx / bounds.width
                sy = dy / bounds.height
                return true
            } else {
                return false
            }
        }
        
        let new = point.rotate(-angle, center: center)
        let oldCenter = center
        rotateToZero()
        defer {
            rotateToCurrent()
        }
        var dx: CGFloat = 0.0
        var dy: CGFloat = 0.0
        var sx: CGFloat = 1.0
        var sy: CGFloat = 1.0
        switch  direction {
        case .topLeft:
            let base = CGPoint(x: frame.maxX, y: frame.maxY)
            guard base.x > new.x && base.y > new.y
                    && conv(base: base) else { return }
            frame = CGRect(origin: CGPoint(x: base.x - dx, y: base.y - dy),
                           size: CGSize(width: dx, height: dy))
        case .topRight:
            let base = CGPoint(x: frame.minX, y: frame.maxY)
            guard base.x < new.x && base.y > new.y
                    && conv(base: base) else { return }
            frame = CGRect(origin: CGPoint(x: frame.origin.x, y: base.y - dy),
                           size: CGSize(width: dx, height: dy))
        case .bottomLeft:
            let base = CGPoint(x: frame.maxX, y: frame.minY)
            guard base.x > new.x && base.y < new.y
                    && conv(base: base) else { return }
            frame = CGRect(origin: CGPoint(x: base.x - dx, y: frame.origin.y),
                           size: CGSize(width: dx, height: dy))
        case .bottomRight:
            let base = frame.origin
            guard base.x < new.x && base.y < new.y
                    && conv(base: base) else { return }
            frame = CGRect(origin: frame.origin,
                           size: CGSize(width: dx, height: dy))
        case .top:
            let base = CGPoint(x: frame.midX, y: frame.maxY)
            let dy = base.y - new.y
            guard dy > drawItemMinSize else { return }
            sy = dy / bounds.height
            frame = CGRect(origin: CGPoint(x: frame.origin.x, y: base.y - dy),
                           size: CGSize(width: frame.width, height: dy))
        case .left:
            let base = CGPoint(x: frame.maxX, y: frame.midY)
            let dx = base.x - new.x
            guard dx > drawItemMinSize else { return }
            sx = dx / bounds.width
            frame = CGRect(origin: CGPoint(x: base.x - dx, y: frame.origin.y),
                           size: CGSize(width: dx, height: frame.height))
        case .right:
            let base = CGPoint(x: frame.minX, y: frame.midY)
            let dx = new.x - base.x
            guard dx > drawItemMinSize else { return }
            sx = dx / bounds.width
            frame = CGRect(origin: frame.origin,
                           size: CGSize(width: dx, height: frame.height))
        default:    // .bottom
            let base = CGPoint(x: frame.midX, y: frame.minY)
            let dy = new.y - base.y
            guard dy > drawItemMinSize else { return }
            sy = dy / bounds.height
            frame = CGRect(origin: frame.origin,
                           size: CGSize(width: frame.width, height: dy))
        }
        if let controls = self.controls {
            var newcontrols: [CGPoint] = []
            for control in controls {
                newcontrols.append(CGPoint(x: control.x * sx, y: control.y * sy))
            }
            self.controls = newcontrols
        }
        
        center = center.rotate(angle, center: oldCenter)
        self.setNeedsDisplay()
    }
    
    func control(index: Int, point: CGPoint) {
    }
    
    func changeDpi(scale: CGFloat) {
        rotateToZero()
        frame = frame.scale(scale)
        rotateToCurrent()
    }
    
    func rotateToZero() {
        self.transform = CGAffineTransform(rotationAngle: 0.0)
    }
    
    func rotateToCurrent() {
        self.transform = CGAffineTransform(rotationAngle: (self.angle * CGFloat.pi / 180.0))
    }
    
    func startStandOut() {
        let duration: TimeInterval = 0.5
        let interval: TimeInterval = 0.15
        let delay: TimeInterval = 0.0
        
        if let animeTimer = animeTimer {
            animeTimer.invalidate()
        }
        UIView.animate(withDuration: interval, delay: delay,
                       options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
            self.alpha = 0.1
        }, completion: nil)
        
        animeTimer = Timer.scheduledTimer(withTimeInterval: duration, repeats: false) { _ in
            self.layer.removeAllAnimations()
            self.alpha = 1.0
            self.animeTimer = nil
        }
    }
    
    func stopStandOut() {
        if let animeTimer = animeTimer {
            animeTimer.invalidate()
            self.animeTimer = nil
        }
        self.layer.removeAllAnimations()
        self.alpha = 1.0
    }
}
