//
//  HandleView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/06/26.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

enum HandleOperationType {
    case noOperation
    case move
    case scale
    case scaleX
    case scaleY
    case rotate
    case control
}

enum Direction {
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
    case top
    case left
    case right
    case bottom
}

struct HandleOperation {
    var type: HandleOperationType
    var option: Any?
}

extension HandleOperation {
    init(type: HandleOperationType) {
        self.type = type
        option = nil
    }
}

struct HandleInfo {
    var rect: CGRect
    var touch: CGRect
    let operation: HandleOperationType
    let direction: Direction?
}

extension CGRect {
    var center: CGPoint {
        get { return CGPoint(x: midX, y: midY) }
        set { origin = CGPoint(x: newValue.x - width / 2, y: newValue.y - height / 2) }
    }
}

class HandleView: UIView {
    var isSimplified: Bool = false
    private var drawItem: DrawItemView!
    private var baseScale: CGFloat!
    private var touchSize: CGFloat {
        (UserDefaults.standard.bool(forKey: "touch_on_item") ? 30 : 20) / baseScale
    }
    private var rectSize: CGFloat {10 / baseScale}
    private var rectLineWidth: CGFloat {1.0 / baseScale}
    private var rotateOffset: CGFloat {40 / baseScale}
    private var handleInfos: [HandleInfo] = []
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, item: DrawItemView, scale: CGFloat) {
        drawItem = item
        baseScale = scale
        super.init(frame: frame)
        
        let rect = CGRect(origin: CGPoint.zero, size: CGSize(width: rectSize, height: rectSize))
        let touch = CGRect(origin: CGPoint.zero, size: CGSize(width: touchSize, height: touchSize))
        
        handleInfos.append(HandleInfo(rect: drawItem.frame,
                                      touch: drawItem.frame,
                                      operation: .move,
                                      direction: nil))
        handleInfos.append(HandleInfo(rect: rect, touch: touch, operation: .rotate, direction: nil))
        handleInfos.append(HandleInfo(rect: rect, touch: touch, operation: .scale, direction: .topLeft))
        handleInfos.append(HandleInfo(rect: rect, touch: touch, operation: .scale, direction: .topRight))
        handleInfos.append(HandleInfo(rect: rect, touch: touch, operation: .scale, direction: .bottomLeft))
        handleInfos.append(HandleInfo(rect: rect, touch: touch, operation: .scale, direction: .bottomRight))
        if !drawItem.isFixedAspectRatio {
            handleInfos.append(HandleInfo(rect: rect, touch: touch, operation: .scaleY, direction: .top))
            handleInfos.append(HandleInfo(rect: rect, touch: touch, operation: .scaleX, direction: .left))
            handleInfos.append(HandleInfo(rect: rect, touch: touch, operation: .scaleX, direction: .right))
            handleInfos.append(HandleInfo(rect: rect, touch: touch, operation: .scaleY, direction: .bottom))
        }
        
        rebind()
        self.isUserInteractionEnabled = false
        self.backgroundColor = .clear
        self.clipsToBounds = false
    }
    
    //　ハンドルを描画アイテムにバインドし直す（位置を移動）
    private func rebind() {
        drawItem.rotateToZero()    // 回転を一旦リセット
        
        handleInfos[0].rect = drawItem.frame
        handleInfos[1].rect.center = CGPoint(x: drawItem.frame.midX,                         // Rotate
                                             y: drawItem.frame.maxY + rotateOffset)
        handleInfos[2].rect.center = CGPoint(x: drawItem.frame.minX, y: drawItem.frame.minY) // Top-Left
        handleInfos[3].rect.center = CGPoint(x: drawItem.frame.maxX, y: drawItem.frame.minY) // Top-Right
        handleInfos[4].rect.center = CGPoint(x: drawItem.frame.minX, y: drawItem.frame.maxY) // Bottom-Left
        handleInfos[5].rect.center = CGPoint(x: drawItem.frame.maxX, y: drawItem.frame.maxY) // Bottom-Right
        if !drawItem.isFixedAspectRatio {
            handleInfos[6].rect.center = CGPoint(x: drawItem.frame.midX, y: drawItem.frame.minY) // Top
            handleInfos[7].rect.center = CGPoint(x: drawItem.frame.minX, y: drawItem.frame.midY) // Left
            handleInfos[8].rect.center = CGPoint(x: drawItem.frame.maxX, y: drawItem.frame.midY) // Right
            handleInfos[9].rect.center = CGPoint(x: drawItem.frame.midX, y: drawItem.frame.maxY) // Bottom
        }
        
        handleInfos[0].touch = drawItem.frame
        for i in 1 ..< handleInfos.count {
            handleInfos[i].touch.center = handleInfos[i].rect.center
        }
        
        drawItem.rotateToCurrent()     // 回転した状態に戻す
    }
    
    func operationType(point: CGPoint) -> HandleOperation {
        let point = point.rotate(-1 * drawItem.angle, center: drawItem.center)
        if let controls = drawItem.controls {
            drawItem.rotateToZero()
            let origin = drawItem.frame.origin
            drawItem.rotateToCurrent()
            for i in 0 ..< controls.count {
                let control = controls[i].translation(x: origin.x, y: origin.y)
                if control.distance(point) < touchSize / 2 {
                    return HandleOperation(type: .control, option: i)
                }
            }
        }
        
        for handleInfo in handleInfos.reversed() {
            // handleInfos[0]（アイテム全体）を最後に検出する
            let area = handleInfo.touch
            if (area.minX <= point.x) && (point.x <= area.maxX)
                && (area.minY <= point.y) && (point.y <= area.maxY) {
                return HandleOperation(type: handleInfo.operation, option: handleInfo.direction)
            }
        }
        return HandleOperation(type: .noOperation)
    }
    
    // キャンバスの大きさが変わった際にハンドルの大きさを変更
    func changeScale(scale: CGFloat) {
        drawItem.rotateToZero()    // 回転を一旦リセット
        
        baseScale = scale
        
        for i in 1 ..< handleInfos.count {
            let oldCenter = handleInfos[i].rect.center
            handleInfos[i].rect.size = CGSize(width: rectSize, height: rectSize)
            handleInfos[i].touch.size = CGSize(width: touchSize, height: touchSize)
            handleInfos[i].rect.center = oldCenter
            handleInfos[i].touch.center = oldCenter
        }
        let center = CGPoint(x: drawItem.frame.midX, y: drawItem.frame.maxY + rotateOffset)
        handleInfos[1].rect.center = center
        handleInfos[1].touch.center = center
        setNeedsDisplay()
        
        drawItem.rotateToCurrent()     // 回転した状態に戻す
    }
    
    func redraw() {
        rebind()
        setNeedsDisplay()
    }
    
    override func draw(_ unused: CGRect) {
        func rotatePath(_ path: UIBezierPath) {
            path.apply(CGAffineTransform(translationX: drawItem.center.x, y: drawItem.center.y).inverted())
            path.apply(CGAffineTransform(rotationAngle: (drawItem.angle * CGFloat.pi / 180.0)))
            path.apply(CGAffineTransform(translationX: drawItem.center.x, y: drawItem.center.y))
        }
        
        func stroke(rect: CGRect, lineWidth: CGFloat) {
            var path: UIBezierPath
            let rect2 = CGRect(x: rect.minX + rectLineWidth, y: rect.minY + rectLineWidth,
                               width: rect.width, height: rect.height)
            
            UIColor.black.setStroke()
            path = UIBezierPath(rect: rect)
            path.lineWidth = lineWidth
            rotatePath(path)
            path.stroke()
            
            UIColor.white.setStroke()
            path = UIBezierPath(rect: rect2)
            path.lineWidth = lineWidth
            rotatePath(path)
            path.stroke()
        }
        
        if isSimplified {
            stroke(rect: handleInfos[0].rect, lineWidth: rectLineWidth / 3)
        } else {
            for handleInfo in handleInfos {
                stroke(rect: handleInfo.rect, lineWidth: rectLineWidth)
            }
            let frame = handleInfos[0].rect
            let rect = CGRect(origin: CGPoint(x: frame.midX, y: frame.maxY),
                              size: CGSize(width: 0, height: rotateOffset))
            stroke(rect: rect, lineWidth: rectLineWidth)
            
            if let controls = drawItem.controls {
                drawItem.rotateToZero()
                let x = drawItem.frame.origin.x
                let y = drawItem.frame.origin.y
                drawItem.rotateToCurrent()
                for control in controls {
                    let rect = CGRect(x: control.x + x - rectSize / 2,
                                      y: control.y + y - rectSize / 2,
                                      width: rectSize, height: rectSize)
                    let path = UIBezierPath(ovalIn: rect)
                    UIColor.systemBlue.setFill()
                    rotatePath(path)
                    path.fill()
                    UIColor.black.setStroke()
                    path.lineWidth = rectLineWidth
                    path.stroke()
                }
            }
        }
    }
}

class BoundView: UIView {
    private var drawItem: DrawItemView!
    private var baseScale: CGFloat!
    private var rectLineWidth: CGFloat {0.8 / baseScale}
    private var rectMargin: CGFloat {2.2 / baseScale}
    var boundRect: CGRect!
    var timer: Timer!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, item: DrawItemView, scale: CGFloat) {
        drawItem = item
        baseScale = scale
        super.init(frame: frame)
        
        boundRect = drawItem.frame
        self.isUserInteractionEnabled = false
        self.backgroundColor = .clear
        self.clipsToBounds = false
    }
    // キャンバスの大きさが変わった際に境界線の幅を変更
    func changeScale(scale: CGFloat) {
        baseScale = scale
        setNeedsDisplay()
    }
    
    func redraw() {
        boundRect = drawItem.frame
        setNeedsDisplay()
    }
    
    override func draw(_ unused: CGRect) {
        var path: UIBezierPath
        let rect1 = CGRect(x: boundRect.minX - rectMargin,
                           y: boundRect.minY - rectMargin,
                           width: boundRect.width + rectMargin * 2,
                           height: boundRect.height + rectMargin * 2)
        let rect2 = CGRect(x: rect1.minX + rectLineWidth, y: rect1.minY + rectLineWidth,
                           width: rect1.width, height: rect1.height)
        
        UIColor(red: 0.148, green: 0.238, blue: 0.551, alpha: 1.00).setStroke()
        path = UIBezierPath(rect: rect1)
        path.lineWidth = rectLineWidth
        path.stroke()
        
        UIColor.white.setStroke()
        path = UIBezierPath(rect: rect2)
        path.lineWidth = rectLineWidth
        path.stroke()
    }
}
