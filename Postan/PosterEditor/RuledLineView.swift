//
//  RuledLineView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/08/01.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

enum RuledLineDisplayMode: Int {
    case off        = 0
    case alwaysOn   = 1
    case penModeOn  = 2
}

fileprivate let ruledLineSpaceMax: Float = 50.0
fileprivate let ruledLineSpaceMin: Float = 20.0
fileprivate let defaultRuledLineSpace: Float = 30.0
fileprivate let defaultDisplayMode: RuledLineDisplayMode = .penModeOn

class RuledLineSettingView: UIView {
    private var ruledLineView: RuledLineView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, ruledLineView: RuledLineView) {
        super.init(frame: frame)
        
        self.ruledLineView = ruledLineView
        self.backgroundColor = .white
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
        self.isHidden = true
        
        let label0 = UILabel(frame: CGRect(origin: CGPoint(x: 10, y: 3),
                                           size: CGSize(width: 40, height: 40)))
        label0.text = "罫線"
        label0.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(label0)
        
        let params = ["非表示", "常時表示", "ペン入力時"]
        let selector = UISegmentedControl(items: params)
        selector.frame = CGRect(origin: CGPoint(x: 60, y: 8),
                                size: CGSize(width: 250, height: 30))
        selector.selectedSegmentIndex = ruledLineView.displayMode.rawValue
        selector.addTarget(self, action: #selector(selectorChanged(_:)),
                                    for: .valueChanged)
        self.addSubview(selector)
        
        let label1 = UILabel(frame: CGRect(origin: CGPoint(x: 330, y: 3),
                                           size: CGSize(width: 20, height: 40)))
        label1.text = "幅"
        label1.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(label1)
        
        let slider = UISlider(frame: CGRect(origin: CGPoint(x: 360, y: 8),
                                            size: CGSize(width: 120, height: 30)))
        slider.tag = 0
        slider.minimumValue = ruledLineSpaceMin
        slider.maximumValue = ruledLineSpaceMax
        slider.value = Float(ruledLineView.lineSpace)
        slider.addTarget(self, action: #selector(sliderChanged(_:)),
                         for: .valueChanged)
        self.addSubview(slider)
        
        let button = UIButton(type: .system)
        button.frame = CGRect(origin: CGPoint(x: 490, y: 3),
                              size: CGSize(width: 40, height: 40))
        button.setTitle("OK", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        button.addTarget(self, action: #selector(buttonPushed(_:)), for: .touchUpInside)
        self.addSubview(button)
    }
    
    @objc func selectorChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case RuledLineDisplayMode.off.rawValue:         // 非表示
            ruledLineView.changeDisplayMode(mode: .off)
        case RuledLineDisplayMode.alwaysOn.rawValue:    // 常時表示
            ruledLineView.changeDisplayMode(mode: .alwaysOn)
        default:                                        // ペン入力時
            ruledLineView.changeDisplayMode(mode: .penModeOn)
        }
    }
    
    @objc func sliderChanged(_ sender: UISlider) {
        ruledLineView.changeLineSpace(space: CGFloat(sender.value))
    }
    
    @objc func buttonPushed(_ sender: UIButton) {
        self.isHidden = true
        self.superview!.isUserInteractionEnabled = false
    }
}

class RuledLineView: UIView {
    private var canvasDpi: CGFloat!
    private let lineWidth: CGFloat = 0.6
    private let dashPattern: [CGFloat] = [4, 4]
    var lineSpace: CGFloat!
    var displayMode: RuledLineDisplayMode!
    private var isPenMode: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, dpi: CGFloat) {
        canvasDpi = dpi
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.isUserInteractionEnabled = false
        let userDefaults = UserDefaults.standard
        userDefaults.register(defaults: ["RuledLineSpace": defaultRuledLineSpace,
                                         "RuledLineDisplayMode": defaultDisplayMode.rawValue])
        lineSpace = CGFloat(userDefaults.object(forKey: "RuledLineSpace") as! Float)
        displayMode = RuledLineDisplayMode(rawValue:
                                            userDefaults.object(forKey: "RuledLineDisplayMode") as! Int)!
        
        changeDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        let scale = canvasDpi / CreateNewPosterController.fixedCanvasDpi
        let path = UIBezierPath()
        path.lineWidth = lineWidth * scale
        let dash = [dashPattern[0] * scale, dashPattern[1] * scale]
        path.setLineDash(dash, count: dashPattern.count, phase: 1)
        UIColor.gray.setStroke()
        
        let space = lineSpace * scale
        var y: CGFloat = space
        while y < frame.height {
            let begin = CGPoint(x: 0, y: y)
            let end   = CGPoint(x: frame.width, y: y)
            path.move(to: begin)
            path.addLine(to: end)
            y += space
        }
        
        path.stroke()
    }
    
    private func changeDisplay() {
        switch displayMode {
        case .off:
            self.isHidden = true
        case .alwaysOn:
            self.isHidden = false
        default:        // case .penModeOn:
            if isPenMode {
                self.isHidden = false
            } else {
                self.isHidden = true
            }
        }
    }
    
    func changeDpi(_ dpi: CGFloat) {
        canvasDpi = dpi
    }
    
    func changeSize(_ size: CGSize) {
        self.frame.size = size
    }
    
    func changeLineSpace(space: CGFloat) {
        lineSpace = space
        setNeedsDisplay()
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(space, forKey: "RuledLineSpace")
    }
    
    func changeDisplayMode(mode: RuledLineDisplayMode) {
        displayMode = mode
        changeDisplay()
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(mode.rawValue, forKey: "RuledLineDisplayMode")
    }
    
    func penModeStart() {
        isPenMode = true
        changeDisplay()
    }
    
    func penModeEnd() {
        isPenMode = false
        changeDisplay()
    }
}
