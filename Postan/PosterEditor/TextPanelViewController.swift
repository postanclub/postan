//
//  TextPanelViewController.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2021/06/08.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

protocol SizePickerDelegate: AnyObject {
    func sizePickerSizeChanged(_ size: CGFloat)
}

func SizePicker(view: UIView, rect: CGRect) -> SizePickerViewController {
    let storyboard = UIStoryboard(name: "TextPanel", bundle: nil)
    let sizePickerVC: SizePickerViewController!
    sizePickerVC = storyboard.instantiateViewController(
        withIdentifier: "SizePickerViewController") as? SizePickerViewController
    sizePickerVC.modalPresentationStyle = .popover
    sizePickerVC.preferredContentSize = CGSize(width: 100, height: 300)
    sizePickerVC.popoverPresentationController?.sourceView = view
    sizePickerVC.popoverPresentationController?.sourceRect = rect
    sizePickerVC.popoverPresentationController?.permittedArrowDirections = .up
    return sizePickerVC
}

class SizePickerViewController: UIViewController {
    var value: Double!
    var max: Double!
    var min: Double!
    var buttonArray: [Double] = []
    weak var delegate: SizePickerDelegate!
    @IBOutlet weak var sizeStepper: UIStepper!
    @IBOutlet weak var sizeButton0: UIButton!
    @IBOutlet weak var sizeButton1: UIButton!
    @IBOutlet weak var sizeButton2: UIButton!
    @IBOutlet weak var sizeButton3: UIButton!
    @IBOutlet weak var sizeButton4: UIButton!
    @IBOutlet weak var sizeButton5: UIButton!
    @IBOutlet weak var sizeButton6: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sizeStepper.value = value
        sizeStepper.minimumValue = min
        sizeStepper.maximumValue = max
        sizeButton0.setTitle(String(Int(buttonArray[0])), for: .normal)
        sizeButton1.setTitle(String(Int(buttonArray[1])), for: .normal)
        sizeButton2.setTitle(String(Int(buttonArray[2])), for: .normal)
        sizeButton3.setTitle(String(Int(buttonArray[3])), for: .normal)
        sizeButton4.setTitle(String(Int(buttonArray[4])), for: .normal)
        sizeButton5.setTitle(String(Int(buttonArray[5])), for: .normal)
        sizeButton6.setTitle(String(Int(buttonArray[6])), for: .normal)
    }
    
    @IBAction func sizeChanged(_ sender: UIStepper) {
        delegate.sizePickerSizeChanged(CGFloat(sender.value))
    }
    
    private func sizeSelected(value: Double) {
        sizeStepper.value = value
        delegate.sizePickerSizeChanged(CGFloat(value))
    }
    
    @IBAction func size0(_ sender: Any) {
        sizeSelected(value: buttonArray[0])
    }
    
    @IBAction func size1(_ sender: Any) {
        sizeSelected(value: buttonArray[1])
    }
    
    @IBAction func size2(_ sender: Any) {
        sizeSelected(value: buttonArray[2])
    }
    
    @IBAction func size3(_ sender: Any) {
        sizeSelected(value: buttonArray[3])
    }
    
    @IBAction func size4(_ sender: Any) {
        sizeSelected(value: buttonArray[4])
    }
    
    @IBAction func size5(_ sender: Any) {
        sizeSelected(value: buttonArray[5])
    }
    
    @IBAction func size6(_ sender: Any) {
        sizeSelected(value: buttonArray[6])
    }
}

protocol ParagraphPickerDelegate: AnyObject {
    func paragraphPickerAlignChanged(_ aligment: NSTextAlignment)
    func paragraphPickerLineSpaceChanged(_ lineSpacing: CGFloat)
}

func ParagraphPicker(view: UIView, rect: CGRect) -> ParagraphPickerViewController {
    let storyboard = UIStoryboard(name: "TextPanel", bundle: nil)
    let textAlignVC: ParagraphPickerViewController!
    textAlignVC = storyboard.instantiateViewController(
        withIdentifier: "ParagraphPickerViewController") as? ParagraphPickerViewController
    textAlignVC.modalPresentationStyle = .popover
    textAlignVC.preferredContentSize = CGSize(width: 140, height: 400)
    textAlignVC.popoverPresentationController?.sourceView = view
    textAlignVC.popoverPresentationController?.sourceRect = rect
    textAlignVC.popoverPresentationController?.permittedArrowDirections = .up
    
    return textAlignVC
}

class SelectButton: UIButton {
    private let container: UIView
    private let orgImage: UIImage
    
    required init?(coder aDecoder: NSCoder) {
        self.container = UIView()
        self.orgImage = UIImage()
        super.init(coder: aDecoder)
    }
    
    init(container: UIView, image: UIImage, title: String) {
        self.container = container
        self.orgImage = image
        let rect = CGRect(origin: CGPoint(x: 6, y: 0), size: container.frame.size)
        super.init(frame: rect)
        self.setTitle(title, for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        self.contentHorizontalAlignment = .left
        container.layer.cornerRadius = 5
        container.addSubview(self)
    }
    
    func switchOn(_ on: Bool) {
        if on {
            self.setTitleColor(.white, for: .normal)
            let image = orgImage.withTintColor(.white, renderingMode: .alwaysOriginal)
            self.setImage(image, for: .normal)
            container.backgroundColor = .systemBlue
        } else {
            self.setTitleColor(.systemBlue, for: .normal)
            let image = orgImage.withTintColor(.systemBlue, renderingMode: .alwaysOriginal)
            self.setImage(image, for: .normal)
            container.backgroundColor = .clear
        }
    }
}

class ParagraphPickerViewController: UIViewController {
    var alignment: NSTextAlignment!
    var lineSpacing: Double!
    var minLineSpacing: Double!
    var maxLineSpacing: Double!
    weak var delegate: ParagraphPickerDelegate!
    @IBOutlet weak var alignLeftContainer: UIView!
    private        var alignLeftButton: SelectButton!
    @IBOutlet weak var alignCenterContainer: UIView!
    private        var alignCenterButton: SelectButton!
    @IBOutlet weak var alignRightContainer: UIView!
    private        var alignRightButton: SelectButton!
    @IBOutlet weak var lineSpaceLabel: UILabel!
    @IBOutlet weak var lineSpaceStepper: UIStepper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alignLeftButton = SelectButton(container: alignLeftContainer,
                                       image: UIImage(systemName: "text.alignleft")!,
                                       title: " 左揃え")
        alignLeftButton.addTarget(self, action: #selector(alignLeftPushed(_:)), for: .touchUpInside)
        alignCenterButton = SelectButton(container: alignCenterContainer,
                                         image: UIImage(systemName: "text.aligncenter")!,
                                         title: " 中央揃え")
        alignCenterButton.addTarget(self, action: #selector(alignCenterPushed(_:)), for: .touchUpInside)
        alignRightButton = SelectButton(container: alignRightContainer,
                                        image: UIImage(systemName: "text.alignright")!,
                                        title: " 右揃え")
        alignRightButton.addTarget(self, action: #selector(alignRightPushed(_:)), for: .touchUpInside)
        
        lineSpaceStepper.value = lineSpacing
        lineSpaceStepper.minimumValue = minLineSpacing
        lineSpaceStepper.maximumValue = maxLineSpacing
        
        changeButtonsState()
    }
    
    private func changeButtonsState() {
        alignLeftButton.switchOn(false)
        alignCenterButton.switchOn(false)
        alignRightButton.switchOn(false)
        switch alignment {
        case .left:
            alignLeftButton.switchOn(true)
        case .center:
            alignCenterButton.switchOn(true)
        default: // .right
            alignRightButton.switchOn(true)
        }
        lineSpaceLabel.text = "行間  \(Int(lineSpacing))"
    }
    
    @objc private func alignLeftPushed(_ sender: Any) {
        alignSelected(.left)
    }
    
    @objc private func alignCenterPushed(_ sender: Any) {
        alignSelected(.center)
    }
    
    @objc private func alignRightPushed(_ sender: Any) {
        alignSelected(.right)
    }
    
    private func alignSelected(_ alignment: NSTextAlignment) {
        self.alignment = alignment
        changeButtonsState()
        delegate.paragraphPickerAlignChanged(alignment)
    }
    
    private func lineSpaceSelected(value: Double) {
        lineSpaceStepper.value = value
        lineSpacing = value
        changeButtonsState()
        delegate.paragraphPickerLineSpaceChanged(CGFloat(lineSpacing))
    }
    
    @IBAction func lineSpaceChanged(_ sender: UIStepper) {
        lineSpaceSelected(value: sender.value)
    }
    
    @IBAction func lineSpace0(_ sender: Any) {
        lineSpaceSelected(value: 0)
    }
    
    @IBAction func lineSpace10(_ sender: Any) {
        lineSpaceSelected(value: 10)
    }
    
    @IBAction func lineSpace20(_ sender: Any) {
        lineSpaceSelected(value: 20)
    }
    
    @IBAction func lineSpace30(_ sender: Any) {
        lineSpaceSelected(value: 30)
    }
    
    @IBAction func lineSpace40(_ sender: Any) {
        lineSpaceSelected(value: 40)
    }
}
