//
//  ColorPicker.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/11/21.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

protocol ColorPickerDelegate: AnyObject {
    func colorPickerDidSelectColor(_ uiColor: UIColor)
    func colorPickerDidFinish()
}

class ColorPicker: NSObject,
                   ColorPickerDelegate,
                   UIAdaptivePresentationControllerDelegate,
                   UIColorPickerViewControllerDelegate {
    private let colorTitle: String
    private let startColor: UIColor
    private let supportsAlpha: Bool
    private let sourceView: UIView
    private let sourceRect: CGRect
    private let arrowDir: UIPopoverArrowDirection
    private var uiColorPickerVC: UIColorPickerViewController!
    weak var delegate: ColorPickerDelegate!
    
    init(title: String, color: UIColor, alpha: Bool, view: UIView, rect: CGRect,
         arrow: UIPopoverArrowDirection = .left) {
        colorTitle = title
        startColor = color
        supportsAlpha = alpha
        sourceView = view
        sourceRect = rect
        arrowDir = arrow
        super.init()
    }
    
    func present(controller: UIViewController) {
        uiColorPickerVC = UIColorPickerViewController()
        uiColorPickerVC.title = colorTitle
        uiColorPickerVC.selectedColor = startColor
        uiColorPickerVC.supportsAlpha = supportsAlpha
        
        uiColorPickerVC.delegate = self
        uiColorPickerVC.modalPresentationStyle = .popover
        uiColorPickerVC.popoverPresentationController?.sourceView = sourceView
        uiColorPickerVC.popoverPresentationController?.sourceRect = sourceRect
        uiColorPickerVC.popoverPresentationController?.permittedArrowDirections = arrowDir
        uiColorPickerVC.presentationController?.delegate = self
        
        controller.present(uiColorPickerVC, animated: true, completion: nil)
    }
    
    func dismiss(animated: Bool) {
        uiColorPickerVC.dismiss(animated: animated, completion: nil)
    }
    
    func colorPickerViewControllerDidSelectColor(_ uiColorPicker: UIColorPickerViewController) {
        delegate.colorPickerDidSelectColor(uiColorPicker.selectedColor)
    }
    
    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        // popover だと呼ばれない
    }
    
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        // popover の場合に必要
        delegate.colorPickerDidFinish()
    }
    
    func colorPickerDidSelectColor(_ uiColor: UIColor) {
        delegate.colorPickerDidSelectColor(uiColor)
    }
    
    func colorPickerDidFinish() {
    }
}

extension UIColor {
    func changeRed(_ value: CGFloat) -> UIColor {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return UIColor(red: value, green: green, blue: blue, alpha: alpha)
    }
    func changeGreen(_ value: CGFloat) -> UIColor {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return UIColor(red: red, green: value, blue: blue, alpha: alpha)
    }
    func changeBlue(_ value: CGFloat) -> UIColor {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return UIColor(red: red, green: green, blue: value, alpha: alpha)
    }
    func changeAlpha(_ value: CGFloat) -> UIColor {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return UIColor(red: red, green: green, blue: blue, alpha: value)
    }
}
