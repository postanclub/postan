//
//  FigureViewController.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/06/26.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

fileprivate var currentLineWidth: CGFloat = 3.0
fileprivate var currentLineColor: UIColor = .black
fileprivate var currentFillOn: Bool = false
fileprivate var currentFillColor: UIColor = UIColor(red: 0.5, green: 0.7, blue: 0.96, alpha: 1)

extension FigureViewController: ColorPickerDelegate {
    func colorPickerDidSelectColor(_ uiColor: UIColor) {
        if lineColorPicker != nil {
            currentLineColor = uiColor
            lineColorView.backgroundColor = uiColor
            figureItemView.lineColor = uiColor
        } else if fillColorPicker != nil {
            currentFillColor = uiColor
            fillColorView.backgroundColor = uiColor
            figureItemView.fillColor = uiColor
        }
        isModalInPresentation = true
        figureItemView.setNeedsDisplay()
    }
    
    func colorPickerDidFinish() {
        lineColorPicker = nil
        fillColorPicker = nil
    }
}

class FigureViewController: UIViewController {
    var figureItemView: FigureItemView!
    var targetRect: CGRect?
    var canvasView: CanvasView?
    var isOK: Bool = false
    weak var delegate: PanelControllerDelegate?
    
    @IBOutlet weak var lineWidthContainer: UIView!
    @IBOutlet weak var fillSwitchContainer: UIView!
    @IBOutlet weak var figureRectContainer: UIView!
    @IBOutlet weak var figureRoundedRectContainer: UIView!
    @IBOutlet weak var figureEllipseContainer: UIView!
    @IBOutlet weak var figureStarContainer: UIView!
    @IBOutlet weak var figureHeartContainer: UIView!
    @IBOutlet weak var figureArrowContainer: UIView!
    @IBOutlet weak var figureBalloon0Container: UIView!
    @IBOutlet weak var figureBalloon1Container: UIView!
    @IBOutlet weak var okButtonView: UIButton!
    @IBOutlet weak var lineColorView: UIButton!
    @IBOutlet weak var fillColorView: UIButton!
    
    private var backupItemView: FigureItemView?
    private var lineColorPicker: ColorPicker?
    private var fillColorPicker: ColorPicker?
    
    private func setupSlider(view: UIView, tag: Int, max: Float, value: Float) {
        let slider = UISlider(frame: view.frame)
        slider.tag = tag
        slider.minimumValue = 0.0
        slider.maximumValue = max
        slider.value = value
        slider.addTarget(self, action: #selector(changeSlider),
                         for: UIControl.Event.valueChanged)
        self.view.addSubview(slider)
        view.backgroundColor = .clear
    }
    
    private var sampleFigures: [SampleFigureView] = []
    
    private func setupFigure(view: UIView, figureType: FigureType) {
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = 1
        view.backgroundColor = .white
        
        let sampleFigureView = SampleFigureView(frame: view.frame, figureType: figureType)
        sampleFigures.append(sampleFigureView)
        self.view.addSubview(sampleFigureView)
        sampleFigureView.setNeedsDisplay()
    }
    
    private func setupColorButton(view: UIView, color: UIColor) {
        view.backgroundColor = color
        view.layer.cornerRadius = 6
        view.layer.borderColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
        view.layer.borderWidth = 0.6
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if figureItemView == nil {
            okButtonView.isEnabled = false
            figureItemView = FigureItemView(frame: targetRect!, figureType: .noFigure)
        } else {
            backupItemView = figureItemView.copy()
            currentLineWidth = figureItemView.lineWidth
            currentLineColor = figureItemView.lineColor
            currentFillOn = figureItemView.fillOn
            currentFillColor = figureItemView.fillColor
        }
        
        setupSlider(view: lineWidthContainer, tag: 0, max: 20.0, value: Float(currentLineWidth))
        
        setupColorButton(view: lineColorView, color: currentLineColor)
        setupColorButton(view: fillColorView, color: currentFillColor)
        
        let fillSwitch = UISwitch(frame: fillSwitchContainer.frame)
        fillSwitch.addTarget(self, action: #selector(changeSwitch),
                             for: UIControl.Event.valueChanged)
        fillSwitch.isOn = currentFillOn
        self.view.addSubview(fillSwitch)
        fillSwitchContainer.backgroundColor = .clear
        
        setupFigure(view: figureRectContainer, figureType: .rect)
        setupFigure(view: figureRoundedRectContainer, figureType: .roundedRect)
        setupFigure(view: figureEllipseContainer, figureType: .ellipse)
        setupFigure(view: figureStarContainer, figureType: .star)
        setupFigure(view: figureHeartContainer, figureType: .heart)
        setupFigure(view: figureArrowContainer, figureType: .arrow)
        setupFigure(view: figureBalloon0Container, figureType: .balloon0)
        setupFigure(view: figureBalloon1Container, figureType: .balloon1)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let view = touch.view
        for figure in sampleFigures {
            if view == figure as UIView {
                if figureItemView.figureType == .noFigure {
                    okButtonView.isEnabled = true
                    canvasView!.addSubview(figureItemView)
                }
                figureItemView.initFigureType(figure.figureType)
                canvasView!.changeHandle(item: figureItemView)
                isModalInPresentation = true
                figureItemView.setNeedsDisplay()
            }
        }
    }
    
    @IBAction func lineColorButton(_ sender: UIButton) {
        lineColorPicker = ColorPicker(title: "線の色",
                                      color: currentLineColor,
                                      alpha: true,
                                      view: self.view,
                                      rect: lineColorView.frame)
        lineColorPicker!.delegate = self
        lineColorPicker!.present(controller: self)
    }
    
    @IBAction func fillColorButton(_ sender: Any) {
        fillColorPicker = ColorPicker(title: "塗り潰しの色",
                                      color: currentFillColor,
                                      alpha: true,
                                      view: self.view,
                                      rect: fillColorView.frame)
        fillColorPicker!.delegate = self
        fillColorPicker!.present(controller: self)
    }
    
    @IBAction func okButton(_ sender: Any) {
        isOK = true
        delegate?.panelControllerDidFinish(self)
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        cancel()
        delegate?.panelControllerDidFinish(self)
    }
    
    private func cancel() {
        if let backupItemView = backupItemView {
            // 図形編集の場合
            figureItemView.lineWidth = backupItemView.lineWidth
            figureItemView.lineColor = backupItemView.lineColor
            figureItemView.fillOn = backupItemView.fillOn
            figureItemView.fillColor = backupItemView.fillColor
            figureItemView.figureType = backupItemView.figureType
            figureItemView.controls = backupItemView.controls
            canvasView!.changeHandle(item: figureItemView)
            figureItemView.setNeedsDisplay()
            self.backupItemView = nil
        } else {
            // 新規追加の場合
            figureItemView.removeFromSuperview()
            if figureItemView.figureType != .noFigure {
                // 図形を指定した場合
                canvasView!.deleteHandle()
            }
            figureItemView = nil
        }
    }
    
    @objc private func changeSlider(_ sender: UISlider) {
        figureItemView.lineWidth = CGFloat(sender.value)
        currentLineWidth = CGFloat(sender.value)
        isModalInPresentation = true
        figureItemView.setNeedsDisplay()
    }
    
    @objc func changeSwitch(_ sender: UISwitch) {
        figureItemView.fillOn = sender.isOn
        currentFillOn = sender.isOn
        isModalInPresentation = true
        figureItemView.setNeedsDisplay()
    }
}

enum FigureType: Int, Codable {
    case noFigure
    case rect
    case roundedRect
    case ellipse
    case star
    case heart
    case arrow
    case balloon0
    case balloon1
}

struct FigureItemData: Codable {
    let figureType: FigureType
    let lineWidth: CGFloat
    let lineColor: Color?
    let fillOn: Bool
    let fillColor: Color?
    let lineRed: CGFloat?       // deprecated
    let lineGreen: CGFloat?     // deprecated
    let lineBlue: CGFloat?      // deprecated
    let fillRed: CGFloat?       // deprecated
    let fillGreen: CGFloat?     // deprecated
    let fillBlue: CGFloat?      // deprecated
}

// DrawItemViewをUIImageViewにした場合に必要
class FigureDrawView: UIView {
    private weak var itemView: FigureItemView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, itemView: FigureItemView) {
        self.itemView = itemView
        super.init(frame: frame)
        self.isUserInteractionEnabled = false
        self.contentMode = .scaleToFill
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.backgroundColor = .clear
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        itemView.draw(rect)
    }
}

class FigureItemView: DrawItemView {
    var lineWidth: CGFloat = currentLineWidth
    var lineColor: UIColor = currentLineColor
    var fillOn: Bool = currentFillOn
    var fillColor: UIColor = currentFillColor
    var figureType: FigureType
    override var isEditable: Bool { true }
    private let cornerRadius: CGFloat = 30
    private let rectMargin: CGFloat = 13
    private var drawView: FigureDrawView?
    
    static var canvasDpi: CGFloat = CreateNewPosterController.fixedCanvasDpi
    
    required init?(coder aDecoder: NSCoder) {
        self.figureType = .noFigure
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, figureType: FigureType) {
        self.figureType = figureType
        super.init(frame: frame, type: .figure)
        /* DrawItemViewをUIImageViewにした場合に必要
         drawView = FigureDrawView(frame: CGRect(origin: .zero, size: frame.size),
         itemView: self)
         self.addSubview(drawView!)
         */
    }
    
    override init(data: DrawItemData, dataStore: CanvasDataStore) {
        let figureItemData = data.figureItemData!
        self.lineWidth = figureItemData.lineWidth
        if let color = figureItemData.lineColor {
            lineColor = color.uiColor
        } else {
            lineColor = UIColor(red: figureItemData.lineRed!,
                                green: figureItemData.lineGreen!,
                                blue: figureItemData.lineBlue!,
                                alpha: 1.0)
        }
        self.fillOn = figureItemData.fillOn
        if let color = figureItemData.fillColor {
            fillColor = color.uiColor
        } else {
            fillColor = UIColor(red: figureItemData.fillRed!,
                                green: figureItemData.fillGreen!,
                                blue: figureItemData.fillBlue!,
                                alpha: 1.0)
        }
        self.figureType = figureItemData.figureType
        super.init(data: data, dataStore: dataStore)
        self.controls = data.controls
        /* DrawItemViewをUIImageViewにした場合に必要
         drawView = FigureDrawView(frame: CGRect(origin: .zero, size: data.frame.size),
         itemView: self)
         self.addSubview(drawView!)
         */
    }
    
    func initFigureType(_ type: FigureType) {
        let dpiScale = FigureItemView.canvasDpi / CreateNewPosterController.fixedCanvasDpi
        let margin = rectMargin * dpiScale
        figureType = type
        controls = nil
        if type == .balloon0 || type == .balloon1 {
            controls = []
            controls!.append(CGPoint(x: bounds.width / 2,
                                     y: bounds.height * 0.75))
            controls!.append(CGPoint(x: bounds.width / 3,
                                     y: bounds.height - margin))
        } else if type == .arrow {
            controls = []
            controls!.append(CGPoint(x: bounds.width * 0.55,
                                     y: bounds.height * 0.25))
        }
    }
    
    override func convAndSave(dataStore: CanvasDataStore) -> DrawItemData {
        rotateToZero()
        let lineColor = Color(uiColor: self.lineColor)
        let fillColor = Color(uiColor: self.fillColor)
        let figureData = FigureItemData(figureType: figureType,
                                        lineWidth: lineWidth,
                                        lineColor: lineColor,
                                        fillOn: fillOn,
                                        fillColor: fillColor,
                                        lineRed: nil,
                                        lineGreen: nil,
                                        lineBlue: nil,
                                        fillRed: nil,
                                        fillGreen: nil,
                                        fillBlue: nil)
        
        let data = DrawItemData(type: self.type,
                                frame: self.frame,
                                angle: self.angle,
                                controls: controls,
                                figureItemData: figureData)
        rotateToCurrent()
        return data
    }
    
    override func control(index: Int, point: CGPoint) {
        let dpiScale = FigureItemView.canvasDpi / CreateNewPosterController.fixedCanvasDpi
        let margin = rectMargin * dpiScale
        rotateToZero()
        var newPoint = point.rotate(-1 * angle, center: center)
            .translation(x: -frame.origin.x, y: -frame.origin.y)
        if figureType == .balloon0 || figureType == .balloon1 {
            if index == 0 {
                let minX = 0.35 * bounds.width
                let maxX = 0.65 * bounds.width
                if newPoint.x < minX {
                    newPoint.x = minX
                } else if newPoint.x > maxX {
                    newPoint.x = maxX
                }
                let minY = 0.2 * bounds.height
                let maxY = bounds.height - margin
                if newPoint.y < minY {
                    newPoint.y = minY
                } else if newPoint.y > maxY {
                    newPoint.y = maxY
                }
            } else {
                let minX = margin
                let maxX = bounds.width - margin
                if newPoint.x < minX {
                    newPoint.x = minX
                } else if newPoint.x > maxX {
                    newPoint.x = maxX
                }
                let minY = margin
                let maxY = bounds.height - margin
                if newPoint.y < minY {
                    newPoint.y = minY
                } else if newPoint.y > maxY {
                    newPoint.y = maxY
                }
            }
        }
        if figureType == .arrow {
            let minX = margin
            let maxX = bounds.width - margin
            if newPoint.x < minX {
                newPoint.x = minX
            } else if newPoint.x > maxX {
                newPoint.x = maxX
            }
            let minY = margin
            let maxY = bounds.height - margin
            if newPoint.y < minY {
                newPoint.y = minY
            } else if newPoint.y > maxY {
                newPoint.y = maxY
            }
        }
        controls!.remove(at: index)
        controls!.insert(newPoint, at: index)
        rotateToCurrent()
        setNeedsDisplay()
    }
    
    override func copy(center: CGPoint? = nil) -> FigureItemView {
        rotateToZero()
        let item = FigureItemView(frame: frame, figureType: self.figureType)
        rotateToCurrent()
        if center != nil {
            item.center = center!
        }
        item.angle = self.angle
        item.controls = self.controls
        item.rotateToCurrent()
        item.lineWidth = self.lineWidth
        item.lineColor = self.lineColor
        item.fillOn = self.fillOn
        item.fillColor = self.fillColor
        return item
    }
    
    override func draw(_ rect: CGRect) {
        let margin, corner, width: CGFloat
        let dpiScale = FigureItemView.canvasDpi / CreateNewPosterController.fixedCanvasDpi
        margin = rectMargin * dpiScale
        corner = cornerRadius * dpiScale
        width = lineWidth * dpiScale
        
        let figureRect = CGRect(origin: CGPoint(x: rect.minX + margin, y: rect.minY + margin),
                                size: CGSize(width: rect.size.width - margin * 2,
                                             height: rect.size.height - margin * 2))
        let path: UIBezierPath
        switch figureType {
        case .noFigure:
            return
        case .rect:
            path = UIBezierPath(rect: figureRect)
        case .roundedRect:
            path = UIBezierPath(roundedRect: figureRect, cornerRadius: corner)
        case .ellipse:
            path = UIBezierPath(ovalIn: figureRect)
        case .star:
            path = UIBezierPath(starIn: figureRect)
        case .heart:
            path = UIBezierPath(heartIn: figureRect)
        case .arrow:
            path = UIBezierPath(arrow: figureRect, control: controls![0])
        case .balloon0:
            path = UIBezierPath(balloon0In: figureRect,
                                base: controls![0],
                                whisker: controls![1])
        case .balloon1:
            path = UIBezierPath(balloon1In: figureRect,
                                base: controls![0],
                                whisker: controls![1])
        }
        if fillOn {
            fillColor.setFill()
            path.fill()
        }
        lineColor.setStroke()
        path.lineWidth = width
        path.stroke()
    }
    /* DrawItemViewをUIImageViewにした場合に必要
     override func setNeedsDisplay() {
     if let drawView = drawView {
     drawView.setNeedsDisplay()
     }
     }
     */
}

class SampleFigureView: UIView {
    var figureType: FigureType
    private let lineWidth: CGFloat = 2.0
    private let cornerRadius: CGFloat = 10
    private let rectMargin: CGFloat = 5.0
    
    required init?(coder aDecoder: NSCoder) {
        self.figureType = .noFigure
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, figureType: FigureType) {
        self.figureType = figureType
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
        self.backgroundColor = .clear
    }
    
    override func draw(_ rect: CGRect) {
        let figureRect = CGRect(origin: CGPoint(x: rect.minX + rectMargin,
                                                y: rect.minY + rectMargin),
                                size: CGSize(width: rect.size.width - rectMargin * 2,
                                             height: rect.size.height - rectMargin * 2))
        let path: UIBezierPath
        switch figureType {
        case .noFigure:
            return
        case .rect:
            path = UIBezierPath(rect: figureRect)
        case .roundedRect:
            path = UIBezierPath(roundedRect: figureRect, cornerRadius: cornerRadius)
        case .ellipse:
            path = UIBezierPath(ovalIn: figureRect)
        case .star:
            path = UIBezierPath(starIn: figureRect)
        case .heart:
            path = UIBezierPath(heartIn: figureRect)
        case .arrow:
            path = UIBezierPath(arrow: figureRect,
                                control: CGPoint(x: figureRect.width * 0.7,
                                                 y: figureRect.height * 0.32))
        case .balloon0:
            path = UIBezierPath(balloon0In: figureRect,
                                base: CGPoint(x: figureRect.midX,
                                              y: figureRect.minY + figureRect.height * 0.7),
                                whisker: CGPoint(x: figureRect.minX + figureRect.width / 3,
                                                 y: figureRect.maxY - rectMargin))
        case .balloon1:
            path = UIBezierPath(balloon1In: figureRect,
                                base: CGPoint(x: figureRect.midX,
                                              y: figureRect.minY + figureRect.height * 0.7),
                                whisker: CGPoint(x: figureRect.minX + figureRect.width / 3,
                                                 y: figureRect.maxY - rectMargin))
        }
        UIColor.black.setStroke()
        path.lineWidth = lineWidth
        path.stroke()
    }
}

