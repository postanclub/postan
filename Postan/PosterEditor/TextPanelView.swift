//
//  TextPanelView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2021/06/07.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

fileprivate let minTextSize: Double = 10.0
fileprivate let maxTextSize: Double = 70.0
fileprivate let minKerning: Double = -20.0
fileprivate let maxKerning: Double = 40.0
fileprivate let minLineSpacing: Double = 0.0
fileprivate let maxLineSpacing: Double = 40.0

protocol TextPanelDelegate: AnyObject {
    func textEditingFinish(item: TextItemView)
}

struct TypingAttributes {
    var size: CGFloat
    var color: UIColor
    var bold: Bool
    var underLine: Bool
    var kerning: CGFloat
    var alignment: NSTextAlignment
    var lineSpacing: CGFloat
}

class TextPanelView: UIView,
                     UITextViewDelegate,
                     ColorPickerDelegate,
                     SizePickerDelegate,
                     ParagraphPickerDelegate {
    var isActive = false
    private var isUndoEnabled = false
    private var typingAttr = TypingAttributes(size: 30,
                                              color: .black,
                                              bold: false,
                                              underLine: false,
                                              kerning: 0,
                                              alignment: .left,
                                              lineSpacing: 0)
    private var textItemView: TextItemView!
    private var textView: UITextView!
    private var backgroundView: UIView!
    private let sizeButton = UIButton(type: .system)
    private let kernButton = UIButton(type: .system)
    private let colorButton = UIButton(type: .custom)
    private let paragraphButton = UIButton(type: .system)
    private let boldButton = ToggleButton()
    private let ulineButton = ToggleButton()
    private var colorPicker: ColorPicker?
    private var anyPicker: UIViewController?
    private var debugLabel: UILabel?
    weak var delegate: TextPanelDelegate!
    weak var parentViewController: UIViewController?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, parent: UIViewController) {
        super.init(frame: frame)
        
        parentViewController = parent
        self.backgroundColor = .white
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
        self.isHidden = true
        
        let sizeLabel = UILabel(frame: CGRect(origin: CGPoint(x: 10, y: 3),
                                              size: CGSize(width: 80, height: 40)))
        sizeLabel.text = "サイズ"
        sizeLabel.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(sizeLabel)
        
        sizeButton.frame = CGRect(origin: CGPoint(x: 70, y: 3),
                                  size: CGSize(width: 40, height: 40))
        sizeButton.setLabel(String(Int(typingAttr.size)))
        sizeButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        sizeButton.addTarget(self, action: #selector(sizePushed(_:)),
                             for: .touchUpInside)
        self.addSubview(sizeButton)
        
        let kernLabel = UILabel(frame: CGRect(origin: CGPoint(x: 130, y: 3),
                                              size: CGSize(width: 80, height: 40)))
        kernLabel.text = "文字間"
        kernLabel.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(kernLabel)
        
        kernButton.frame = CGRect(origin: CGPoint(x: 190, y: 3),
                                  size: CGSize(width: 40, height: 40))
        kernButton.setLabel(String(Int(typingAttr.kerning)))
        kernButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        kernButton.addTarget(self, action: #selector(kernPushed(_:)),
                             for: .touchUpInside)
        self.addSubview(kernButton)
        
        let colorLabel = UILabel(frame: CGRect(origin: CGPoint(x: 247, y: 3),
                                               size: CGSize(width: 90, height: 40)))
        colorLabel.text = "色"
        colorLabel.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(colorLabel)
        
        colorButton.frame = CGRect(origin: CGPoint(x: 280, y: 9),
                                   size: CGSize(width: 40, height: 28))
        colorButton.backgroundColor = typingAttr.color
        colorButton.layer.cornerRadius = 6
        colorButton.layer.borderColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
        colorButton.layer.borderWidth = 0.6
        colorButton.addTarget(self, action: #selector(colorPushed(_:)), for: .touchUpInside)
        self.addSubview(colorButton)
        
        boldButton.frame = CGRect(origin: CGPoint(x: 355, y: 6),
                                  size: CGSize(width: 34, height: 34))
        boldButton.setTitle("B", for: .normal)
        boldButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
        boldButton.isOn = typingAttr.bold
        boldButton.addTarget(self, action: #selector(boldPushed(_:)), for: .touchUpInside)
        self.addSubview(boldButton)
        
        ulineButton.frame = CGRect(origin: CGPoint(x: 410, y: 6),
                                   size: CGSize(width: 34, height: 34))
        let attributedString = NSAttributedString(string: "U",
                                                  attributes: [.underlineStyle:
                                                                NSUnderlineStyle.single.rawValue])
        ulineButton.setAttributedTitle(attributedString, for: .normal)
        ulineButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        ulineButton.isOn = typingAttr.underLine
        ulineButton.addTarget(self, action: #selector(ulinePushed(_:)), for: .touchUpInside)
        self.addSubview(ulineButton)
        
        paragraphButton.frame = CGRect(origin: CGPoint(x: 470, y: 3),
                                       size: CGSize(width: 100, height: 40))
        paragraphButton.setTitle("段落書式", for: .normal)
        paragraphButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        paragraphButton.addTarget(self, action: #selector(paragraphPushed(_:)),
                                  for: .touchUpInside)
        self.addSubview(paragraphButton)
        
        let finishButton = UIButton(type: .system)
        finishButton.frame = CGRect(origin: CGPoint(x: 590, y: 3),
                                    size: CGSize(width: 40, height: 40))
        finishButton.setTitle("完了", for: .normal)
        finishButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        finishButton.addTarget(self, action: #selector(finishPushed(_:)), for: .touchUpInside)
        self.addSubview(finishButton)
        
        debugLabel = UILabel(frame: CGRect(origin: CGPoint(x: 630, y: 24),
                                           size: CGSize(width: 10, height: 30)))
        debugLabel!.text = "0"
        debugLabel!.textColor = .red
        debugLabel!.font = UIFont.systemFont(ofSize: 10)
        debugLabel!.isHidden = true
        self.addSubview(debugLabel!)
    }
    
    func startEditing(item: TextItemView, canvasView: CanvasView) {
        isActive = true
        self.isHidden = false
        self.superview!.isUserInteractionEnabled = true
        
        textItemView = item
        
        backgroundView = TouchIgnoreView(frame: CGRect(origin: .zero, size: canvasView.bounds.size))
        backgroundView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.4)
        backgroundView.isOpaque = false
        canvasView.addSubview(backgroundView)
        backgroundView.isUserInteractionEnabled = true
        
        let newTextItemView = textItemView.startEditing()
        textView = newTextItemView.textView
        
        let undoLevel: Int = UserDefaults.standard.integer(forKey: "undo_redo_level")
        if undoLevel > 0 {
            textView.undoManager?.levelsOfUndo = undoLevel
            isUndoEnabled = true
        } else {
            isUndoEnabled = false
        }
        
        // addSubviewより前でないと反映されない？
        if !isUndoEnabled {
            textView.inputAssistantItem.leadingBarButtonGroups = []
        }
        textView.inputAssistantItem.trailingBarButtonGroups = [setupButtonGroup()]
        
        textView.delegate = self
        backgroundView.addSubview(newTextItemView)
        
        if let attr = textItemView.typingAttr {
            typingAttr = attr
        }
        
        textView.selectedRange = NSMakeRange(0, 0)
        textViewDidChangeSelection(textView)
        
        changeButtonState()
    }
    
    func finishEditing() {
        guard isActive else { return }
        isActive = false
        self.isHidden = true
        self.superview!.isUserInteractionEnabled = false
        
        textView.delegate = nil
        textView.undoManager?.removeAllActions()
        textView.removeFromSuperview()
        textItemView.typingAttr = typingAttr
        textItemView.finishEditing()
        
        for view in backgroundView.subviews {
            view.removeFromSuperview()
        }
        backgroundView.removeFromSuperview()
        textView = nil
        backgroundView = nil
        self.delegate.textEditingFinish(item: textItemView)
        textItemView = nil
    }
    
    private func setAttributedText(_ text: NSAttributedString, range: NSRange) {
        registerUndoRedo(newAttributedText: text)
        textView.isScrollEnabled = false
        textView.attributedText = text
        textView.isScrollEnabled = true
        textView.selectedRange = range
    }
    
    private func setupButtonGroup() -> UIBarButtonItemGroup {
        let leftImage = UIImage(systemName: "arrowtriangle.left.fill")!
            .withTintColor(.systemBlue, renderingMode: .alwaysOriginal)
        let left = UIBarButtonItem(image: leftImage,
                                   style: .plain, target: self,
                                   action: #selector(leftPushed(_:)))
        let rightImage = UIImage(systemName: "arrowtriangle.right.fill")!
            .withTintColor(.systemBlue, renderingMode: .alwaysOriginal)
        let right = UIBarButtonItem(image: rightImage,
                                    style: .plain, target: self,
                                    action: #selector(rightPushed(_:)))
        let space = UIBarButtonItem(barButtonSystemItem: .fixedSpace,
                                    target: nil, action: nil)
        space.width = 14
        let finish = UIBarButtonItem(barButtonSystemItem: .done,
                                     target: self,
                                     action: #selector(finishPushed(_:)))
        let font = UIFont.boldSystemFont(ofSize: 20)
        finish.setTitleTextAttributes([.font : font,
                                       .foregroundColor: UIColor.systemBlue],
                                      for: .normal)
        
        return UIBarButtonItemGroup(barButtonItems: [left, right, space, finish],
                                    representativeItem: nil)
    }
    
    private func changeButtonState() {
        sizeButton.setLabel(String(Int(typingAttr.size)))
        kernButton.setLabel(String(Int(typingAttr.kerning)))
        colorButton.backgroundColor = typingAttr.color
        boldButton.isOn = typingAttr.bold
        ulineButton.isOn = typingAttr.underLine
    }
    
    private func setTypingAttributes() {
        var font: UIFont
        
        if typingAttr.bold {
            font = UIFont.boldSystemFont(ofSize: typingAttr.size)
        } else {
            font = UIFont.systemFont(ofSize: typingAttr.size)
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = typingAttr.lineSpacing
        paragraphStyle.alignment = typingAttr.alignment
        
        textView.typingAttributes = [
            .font: font,
            .kern: typingAttr.kerning,
            .foregroundColor: typingAttr.color,
            .paragraphStyle: paragraphStyle,
        ]
        if typingAttr.underLine {
            textView.typingAttributes.updateValue(NSUnderlineStyle.single.rawValue,
                                                  forKey: .underlineStyle)
        }
    }
    
    private func addAttrToSelectedParagraph(type: ParagraphType, value: Any) {
        let text = textView.attributedText.string
        guard text.count > 0 else {
            return
        }
        let paraRange = (text as NSString).paragraphRange(for: textView.selectedRange)
        print(paraRange)
        let mutable = NSMutableAttributedString(attributedString: textView.attributedText)
        
        textView.attributedText.enumerateAttribute(.paragraphStyle,
                                                   in: paraRange,
                                                   options: []) { (attr, part, _) in
            let alignment: NSTextAlignment
            let lineSpacing: CGFloat
            if let attr = attr as? NSMutableParagraphStyle {
                alignment = attr.alignment
                lineSpacing = attr.lineSpacing
            } else {
                // 段落書式が無い場合：念のため
                alignment = .left
                lineSpacing = 0
                debugLabel!.isHidden = false
                debugLabel!.text = String(part.location)
            }
            let newStyle = NSMutableParagraphStyle()
            if type == .alignment {
                newStyle.alignment = value as! NSTextAlignment
                newStyle.lineSpacing = lineSpacing
            } else {
                newStyle.alignment = alignment
                newStyle.lineSpacing = value as! CGFloat
            }
            mutable.addAttribute(.paragraphStyle, value: newStyle, range: part)
        }
        setAttributedText(mutable, range: textView.selectedRange)
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        let range = textView.selectedRange
        var fontPoint = range.location
        var paraPoint = range.location
        let count = textView.attributedText.string.count
        if range.length == 0 {
            // 選択してない場合
            if fontPoint > 0 {
                // カーソルが先頭ではない場合
                fontPoint -= 1
            } else {
                // カーソルが先頭の場合
                if count == 0 {
                    // テキストが空の場合
                    setTypingAttributes()
                    return
                }
            }
            if paraPoint >= count {
                paraPoint = count - 1
            }
        }
        var attributes = textView.attributedText.attributes(at: fontPoint,
                                                            effectiveRange: nil)
        let font = attributes[.font] as! UIFont
        let color: UIColor
        if let value = attributes[.foregroundColor] {
            color = value as! UIColor
        } else {
            color = .black
        }
        let uline = attributes[.underlineStyle] == nil ? false : true
        typingAttr.size = font.pointSize
        typingAttr.color = color
        typingAttr.bold = font.fontDescriptor.symbolicTraits.contains(.traitBold)
        typingAttr.underLine = uline
        
        if fontPoint != paraPoint {
            attributes = textView.attributedText.attributes(at: paraPoint,
                                                            effectiveRange: nil)
        }
        if let kern = attributes[.kern] {
            typingAttr.kerning = kern as! CGFloat
        } else {
            typingAttr.kerning = 0
        }
        if let attribute = attributes[.paragraphStyle] {
            let paragraphStyle = attribute as! NSParagraphStyle
            typingAttr.alignment = paragraphStyle.alignment
            typingAttr.lineSpacing = paragraphStyle.lineSpacing
        } else {
            typingAttr.alignment = .left
            typingAttr.lineSpacing = 0
            debugLabel!.isHidden = false
            debugLabel!.text = String(paraPoint)
        }
        
        //print("length:\(textView.selectedRange.length) " + font.fontName + ":\(Int(typingAttr.size))")
        changeButtonState()
        setTypingAttributes()
    }
    
    //------------------------------------------------------------------------------------
    struct TextUndoData {
        let attributedText: NSAttributedString
        let selectedRange: NSRange
    }
    
    private func registerUndoRedo(newAttributedText: NSAttributedString) {
        guard isUndoEnabled else { return }
        let undoData = TextUndoData(attributedText: textView.attributedText,
                                    selectedRange: textView.selectedRange)
        let redoData = TextUndoData(attributedText: newAttributedText,
                                    selectedRange: textView.selectedRange)
        textView.undoManager?.registerUndo(withTarget: self) {
            $0.undoTextView(undoData: undoData, redoData: redoData)
        }
    }
    
    private func undoTextView(undoData: TextUndoData, redoData: TextUndoData) {
        textView.attributedText = undoData.attributedText
        textView.selectedRange = undoData.selectedRange
        textViewDidChangeSelection(textView)
        
        textView.undoManager?.registerUndo(withTarget: self) {
            $0.undoTextView(undoData: redoData, redoData: undoData)
        }
    }
    
    //------------------------------------------------------------------------------------
    enum PickerType {
        case textSize
        case kerning
    }
    private var sizePickerType: PickerType = .textSize
    
    func sizePickerSizeChanged(_ size: CGFloat) {
        switch sizePickerType {
        case .textSize:
            textSizeChanged(size)
        case .kerning:
            kerningChanged(size)
        }
    }
    
    //------------------------------------------
    @objc func sizePushed(_ sender: UIButton) {
        let buttonArray: [Double] = [10, 20, 30, 40, 50, 60, 70]
        let sizePicker = SizePicker(view: self, rect: sizeButton.frame)
        sizePicker.value = Double(typingAttr.size)
        sizePicker.max = maxTextSize
        sizePicker.min = minTextSize
        sizePicker.buttonArray = buttonArray
        sizePicker.delegate = self
        anyPicker = sizePicker
        sizePickerType = .textSize
        parentViewController!.present(sizePicker, animated: true, completion: nil)
    }
    
    private func textSizeChanged(_ size: CGFloat) {
        typingAttr.size = size
        changeButtonState()
        setTypingAttributes()
        
        let range = textView.selectedRange
        if range.length == 0 {
            return
        }
        let mutable = NSMutableAttributedString(attributedString: textView.attributedText)
        textView.attributedText.enumerateAttribute(.font,
                                                   in: range,
                                                   options: []) { (attr, part, _) in
            var font = attr as! UIFont
            font = font.withSize(typingAttr.size)
            mutable.addAttribute(.font, value: font, range: part)
        }
        setAttributedText(mutable, range: range)
        textView.refresh()
    }
    
    //------------------------------------------
    @objc func kernPushed(_ sender: UIButton) {
        let buttonArray: [Double] = [-20, -10, 0, 10, 20, 30, 40]
        let sizePicker = SizePicker(view: self, rect: kernButton.frame)
        sizePicker.value = Double(typingAttr.kerning)
        sizePicker.max = maxKerning
        sizePicker.min = minKerning
        sizePicker.buttonArray = buttonArray
        sizePicker.delegate = self
        anyPicker = sizePicker
        sizePickerType = .kerning
        parentViewController!.present(sizePicker, animated: true, completion: nil)
    }
    
    private func kerningChanged(_ size: CGFloat) {
        typingAttr.kerning = size
        changeButtonState()
        setTypingAttributes()
        
        let range = textView.selectedRange
        if range.length > 0 {
            let mutable = NSMutableAttributedString(attributedString: textView.attributedText)
            mutable.addAttribute(.kern, value: typingAttr.kerning, range: range)
            setAttributedText(mutable, range: range)
        }
    }
    
    //------------------------------------------
    @objc func colorPushed(_ sender: UIButton) {
        colorPicker = ColorPicker(title: "文字の色",
                                  color: typingAttr.color,
                                  alpha: true,
                                  view: self,
                                  rect: colorButton.frame,
                                  arrow: .up)
        colorPicker!.delegate = self
        colorPicker!.present(controller: parentViewController!)
    }
    
    func colorPickerDidSelectColor(_ uiColor: UIColor) {
        typingAttr.color = uiColor
        changeButtonState()
        setTypingAttributes()
        
        let range = textView.selectedRange
        if range.length > 0 {
            let mutable = NSMutableAttributedString(attributedString: textView.attributedText)
            mutable.addAttribute(.foregroundColor, value: typingAttr.color, range: range)
            mutable.removeAttribute(.underlineColor, range: range)
            setAttributedText(mutable, range: range)
        }
    }
    
    func colorPickerDidFinish() {
        colorPicker = nil
    }
    
    //------------------------------------------
    @objc func boldPushed(_ sender: ToggleButton) {
        typingAttr.bold = sender.isOn
        changeButtonState()
        setTypingAttributes()
        
        let range = textView.selectedRange
        if range.length == 0 {
            return
        }
        let mutable = NSMutableAttributedString(attributedString: textView.attributedText)
        textView.attributedText.enumerateAttribute(.font,
                                                   in: range,
                                                   options: []) { (attr, part, _) in
            var font = attr as! UIFont
            if typingAttr.bold {
                font = UIFont.boldSystemFont(ofSize: font.pointSize)
            } else {
                font = UIFont.systemFont(ofSize: font.pointSize)
            }
            mutable.addAttribute(.font, value: font, range: part)
        }
        setAttributedText(mutable, range: range)
    }
    
    //------------------------------------------
    @objc func ulinePushed(_ sender: ToggleButton) {
        typingAttr.underLine = sender.isOn
        changeButtonState()
        setTypingAttributes()
        
        let range = textView.selectedRange
        if range.length == 0 {
            return
        }
        let mutable = NSMutableAttributedString(attributedString: textView.attributedText)
        mutable.removeAttribute(.underlineColor, range: range)
        if typingAttr.underLine {
            mutable.addAttribute(.underlineStyle,
                                 value: NSUnderlineStyle.single.rawValue,
                                 range: range)
        } else {
            mutable.removeAttribute(.underlineStyle, range: range)
        }
        setAttributedText(mutable, range: range)
    }
    
    //------------------------------------------
    @objc func leftPushed(_ sender: UIButton) {
        let range = textView.selectedRange
        if range.length == 0 {
            var location: Int = 0
            if range.location > 0 {
                location = range.location - 1
            }
            textView.selectedRange = NSMakeRange(location, 0)
        }
    }
    
    @objc func rightPushed(_ sender: UIButton) {
        let range = textView.selectedRange
        if range.length == 0 {
            var location: Int = textView.attributedText.string.count
            if range.location < location {
                location = range.location + 1
            }
            textView.selectedRange = NSMakeRange(location, 0)
        }
    }
    
    @objc func finishPushed(_ sender: UIButton) {
        if let anyPicker = anyPicker {
            anyPicker.dismiss(animated: true)
        }
        anyPicker = nil
        if let colorPicker = colorPicker {
            colorPicker.dismiss(animated: true)
        }
        colorPicker = nil
        finishEditing()
    }
    
    //------------------------------------------------------------------------------------
    enum ParagraphType {
        case alignment
        case lineSpacing
        case kerning
    }
    
    @objc func paragraphPushed(_ sender: UIButton) {
        let paragraphPicker = ParagraphPicker(view: self, rect: paragraphButton.frame)
        paragraphPicker.delegate = self
        paragraphPicker.alignment = typingAttr.alignment
        paragraphPicker.lineSpacing = Double(typingAttr.lineSpacing)
        paragraphPicker.maxLineSpacing = maxLineSpacing
        paragraphPicker.minLineSpacing = minLineSpacing
        anyPicker = paragraphPicker
        parentViewController?.present(paragraphPicker, animated: true, completion: nil)
    }
    
    func paragraphPickerAlignChanged(_ alignment: NSTextAlignment) {
        typingAttr.alignment = alignment
        setTypingAttributes()
        addAttrToSelectedParagraph(type: .alignment, value: alignment)
    }
    
    func paragraphPickerLineSpaceChanged(_ lineSpacing: CGFloat) {
        typingAttr.lineSpacing = lineSpacing
        setTypingAttributes()
        addAttrToSelectedParagraph(type: .lineSpacing, value: lineSpacing)
    }
}

//--------------------------------------------------------------------------------
extension UIFont {
    func withTraits(_ traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
        // create a new font descriptor with the given traits
        guard let fd = fontDescriptor.withSymbolicTraits(traits) else {
            // the given traits couldn't be applied, return self
            return self
        }
        // return a new font with the created font descriptor
        return UIFont(descriptor: fd, size: pointSize)
    }
    
    func normal() -> UIFont {
        return UIFont.systemFont(ofSize: pointSize)
    }
    
    func italics() -> UIFont {
        return withTraits(.traitItalic)
    }
    
    func bold() -> UIFont {
        return withTraits(.traitBold)
    }
    
    func boldItalics() -> UIFont {
        return withTraits([ .traitBold, .traitItalic ])
    }
}

class ToggleButton: UIButton {
    var isOn: Bool = false {
        didSet {
            if isOn {
                self.setTitleColor(.white, for: .normal)
                self.backgroundColor = .systemGreen
            } else {
                self.setTitleColor(UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1), for: .normal)
                self.backgroundColor = UIColor(red: 0.94, green: 0.94, blue: 0.94, alpha: 1)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setup() {
        self.layer.cornerRadius = 10
        self.layer.borderColor = UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1).cgColor
        self.addTarget(self, action: #selector(togglePushed(_:)), for: .touchUpInside)
    }
    
    init() {
        super.init(frame: CGRect())
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @objc func togglePushed(_ sender: ToggleButton) {
        isOn = !isOn
    }
}

extension UIButton {
    func setLabel(_ label: String) {
        UIView.setAnimationsEnabled(false)
        setTitle(label, for: .normal)
        layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
}

class TouchIgnoreView: UIView {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
