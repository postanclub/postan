//
//  CanvasViewController.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/06/26.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit
import UniformTypeIdentifiers

let editButtonTitle = "編集"
let joinButtonTitle = "結合"

extension UIViewController {
    func displayAlert(_ message: String,
                      cancel: Bool = false,
                      cancelHandler: ((UIAlertAction) -> Void)? = nil,
                      okHandler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        if cancel {
            alert.addAction(UIAlertAction(title: "いいえ", style: .cancel, handler: cancelHandler))
            alert.addAction(UIAlertAction(title: "はい", style: .default, handler: okHandler))
        } else {
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: okHandler))
        }
        present(alert, animated: true)
    }
}

extension CanvasViewController: CanvasViewDelegate {
    func statusDidChange() {
        changeButtonsState()
    }
    func itemDidDoubleTap(item: DrawItemView) {
        editButton()
    }
}

class CanvasViewController: UIViewController,
                            ImagePickerDelegate,
                            TextPanelDelegate,
                            UIDocumentPickerDelegate,
                            PanelControllerDelegate {
    @IBOutlet weak var clipWindow: UIView!
    var isNewCanvas: Bool!                      // In
    var folderUrl: URL!                         // In
    var printDataUrl: URL!                      // In
    var canvasSize = CGSize.zero                // In
    var parentViewControllerInfo: UpdateInfo!   // In
    
    private var projectDataStore: CanvasProjectDataStore!
    private var canvasView: CanvasView!
    private let copyOffset: CGFloat = 50
    private var menuButtonView: UIButton!
    private var ruledLineSettingView: RuledLineSettingView!
    private var textPanelView: TextPanelView!
    private var clipWidth: CGFloat?     // 回転の検知用
    @IBOutlet weak var menuContainer: UIView!
    @IBOutlet weak var ruledLineContainer: UIView!
    @IBOutlet weak var textPanelContainer: UIView!
    private        var figureButtonView: UIButton!
    @IBOutlet weak var editButtonView: UIButton!
    @IBOutlet weak var aboveButtonView: UIButton!
    @IBOutlet weak var belowButtonView: UIButton!
    @IBOutlet weak var deleteButtonView: UIButton!
    @IBOutlet weak var copyButtonView: UIButton!
    @IBOutlet weak var undoButtonView: UIButton!
    @IBOutlet weak var redoButtonView: UIButton!
    private        var penSwitchView: UISwitch!
    @IBOutlet weak var buttonsContainer: UIView!
    
    private func canvasChangeDpi(_ dpi: CGFloat) {
        canvasView.changeDpi(dpi)
        FigureItemView.canvasDpi = dpi
    }
    
    private func canvasFinish() {
        projectDataStore.deleteTmpFiles()
        ruledLineSettingView = nil
        textPanelView = nil
        self.dismiss(animated: true, completion: {
            self.canvasView.removeFromSuperview()
            self.canvasView = nil
            self.projectDataStore = nil
            self.parentViewControllerInfo.returnedFromOtherViewController(self.folderUrl as Any?)
        })
        //self.dismiss(animated: true, completion: nil)
    }
    
    private func setupButtons(isTextEnable: Bool) {
        var margin, offset: CGFloat
        if isTextEnable {
            margin = 18
            offset = 3
        } else {
            margin = 26
            offset = 2
        }
        func addItemButton(offset: CGFloat, label: String, action: Selector) -> UIButton {
            let x: CGFloat = margin + (margin + 41) * offset
            let button = UIButton(type: .system)
            button.frame = CGRect(origin: CGPoint(x: x, y: 5),
                                  size: CGSize(width: 41, height: 36))
            button.setTitle(label, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 20)
            button.addTarget(self, action: action, for: .touchUpInside)
            buttonsContainer.addSubview(button)
            return button
        }
        
        buttonsContainer.backgroundColor = .clear
        _ = addItemButton(offset: 0, label: "写真", action: #selector(photoButton(_:)))
        figureButtonView = addItemButton(offset: 1, label: "図形", action: #selector(figureButton(_:)))
        if isTextEnable {
            _ = addItemButton(offset: 2, label: "文字", action: #selector(textButton(_:)))
        }
        
        let penRect = CGRect(origin: CGPoint(x: margin + (margin + 41) * offset, y: 5),
                             size: CGSize(width: 41, height: 36))
        let pen = UILabel(frame: penRect)
        pen.text = "ペン"
        pen.font = UIFont.systemFont(ofSize: 20)
        buttonsContainer.addSubview(pen)
        
        let swichRect = CGRect(origin: CGPoint(x: (margin + 41) * (offset + 1), y: 8),
                               size: CGSize(width: 49, height: 31))
        penSwitchView = UISwitch(frame: swichRect)
        penSwitchView.addTarget(self, action: #selector(penSwitch(_:)), for: .valueChanged)
        /*
         let penRect = CGRect(origin: CGPoint(x: margin + (margin + 41) * offset + 5, y: 6),
         size: CGSize(width: 50, height: 34))
         penSwitchView = ToggleButton(frame: penRect)
         penSwitchView.setTitle("ペン", for: .normal)
         penSwitchView.titleLabel?.font = UIFont.systemFont(ofSize: 20)
         penSwitchView.isOn = false
         penSwitchView.addTarget(self, action: #selector(penSwitch(_:)), for: .touchUpInside)
         */
        buttonsContainer.addSubview(penSwitchView)
    }
    
    private func changeButtonsState() {
        let count = canvasView.selectedItemCount
        UIView.performWithoutAnimation {
            editButtonView.setTitle(editButtonTitle, for: .normal)
            editButtonView.layoutIfNeeded()
        }
        if count == 0 {
            aboveButtonView.isEnabled = false
            belowButtonView.isEnabled = false
            deleteButtonView.isEnabled = false
            copyButtonView.isEnabled = false
            editButtonView.isEnabled = false
        } else {
            aboveButtonView.isEnabled = true
            belowButtonView.isEnabled = true
            deleteButtonView.isEnabled = true
            if count == 1 {
                copyButtonView.isEnabled = true
                editButtonView.isEnabled = canvasView.selectedItem!.isEditable
            } else {
                copyButtonView.isEnabled = false
                if canvasView.selectedPenItemCount > 1 {
                    editButtonView.isEnabled = true
                    UIView.performWithoutAnimation {
                        editButtonView.setTitle(joinButtonTitle, for: .normal)
                        editButtonView.layoutIfNeeded()
                    }
                } else {
                    editButtonView.isEnabled = false
                }
            }
        }
        
        if canvasView.isUndoEnabled {
            if canvasView.inPencilMode || textPanelView.isActive {
                undoButtonView.isEnabled = false
                redoButtonView.isEnabled = false
            } else {
                undoButtonView.isEnabled = canvasView.undoSystem.canUndo
                redoButtonView.isEnabled = canvasView.undoSystem.canRedo
            }
        } else {
            undoButtonView.isHidden = true
            redoButtonView.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clipWindow.clipsToBounds = true
        clipWindow.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.00)
        clipWindow.layer.borderWidth = 1
        clipWindow.layer.borderColor = UIColor.gray.cgColor
        
        ruledLineContainer.backgroundColor = .clear
        textPanelContainer.backgroundColor = .clear
        menuButtonView = UIButton(frame: CGRect(origin: CGPoint.zero, size: menuContainer.frame.size))
        menuButtonView.setImage(UIImage(named: "menu"), for: .normal)
        menuButtonView.addTarget(self, action: #selector(menuButton), for: .touchUpInside)
        menuContainer.backgroundColor = .white
        menuContainer.addSubview(menuButtonView)
        
        setupButtons(isTextEnable: true)
        
        projectDataStore = CanvasProjectDataStore(folderUrl: folderUrl,
                                                  printDataUrl: printDataUrl)
        let dataStore = CanvasDataStore(folderUrl: folderUrl)
        canvasView = CanvasView(canvasSize: canvasSize,
                                canvasColor: CreateNewPosterController.defaultCanvasColor,
                                baseView: clipWindow,
                                dataStore: dataStore)
        canvasView.delegate = self
        if isNewCanvas {
            if let image = canvasView.createImage() {
                projectDataStore.saveThumbnail(image: image, force: false)
            }
            canvasView.saveData()
        } else {
            dataStore.dispatchGroup = DispatchGroup()
            canvasView.isHidden = true
            startIndicator()
            let result = canvasView.loadData()
            dataStore.dispatchGroup.notify(queue: DispatchQueue.main) { [weak self] in
                self!.canvasView.isHidden = false
                self!.canvasView.loadDataDidFinish()
                self!.stopIndicator()
            }
            if result {
                FigureItemView.canvasDpi = canvasView.canvasDpi
            } else {
                if canvasView.canvasSize == CGSize.zero {
                    // items.jsonが壊れている場合
                } else {
                    // items.jsonが存在しない場合
                    canvasView.canvasSize = CGSize.zero
                }
                return
            }
        }
        canvasSize = canvasView.canvasSize
        clipWindow.addSubview(canvasView)
        let ruledLineView = RuledLineView(frame: CGRect(origin: .zero, size: canvasView.canvasSize),
                                          dpi: canvasView.canvasDpi)
        canvasView.addRuledLine(ruledLineView)
        
        let rect = CGRect(origin: .zero, size: ruledLineContainer.frame.size)
        ruledLineSettingView = RuledLineSettingView(frame: rect,
                                                    ruledLineView: ruledLineView)
        ruledLineContainer.backgroundColor = .clear
        ruledLineContainer.isUserInteractionEnabled = false
        ruledLineContainer.addSubview(ruledLineSettingView)
        
        let textRect = CGRect(origin: .zero, size: textPanelContainer.frame.size)
        textPanelView = TextPanelView(frame: textRect, parent: self)
        textPanelView.delegate = self
        textPanelContainer.backgroundColor = .clear
        textPanelContainer.isUserInteractionEnabled = false
        textPanelContainer.addSubview(textPanelView)
        
        let undoImage = UIImage(systemName: "arrow.uturn.left.circle")?
            .withTintColor(.systemGray4, renderingMode: .alwaysOriginal)
        let redoImage = UIImage(systemName: "arrow.uturn.right.circle")?
            .withTintColor(.systemGray4, renderingMode: .alwaysOriginal)
        undoButtonView.setImage(undoImage, for: .disabled)
        redoButtonView.setImage(redoImage, for: .disabled)
        
        changeButtonsState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if canvasView.canvasSize == CGSize.zero {
            // items.jsonが壊れている場合
            displayAlert("データが壊れています\n削除してください",
                         okHandler: { (action) in self.canvasFinish() })
        }
    }
    
    // 最初の一回と回転時の処理
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if clipWidth != clipWindow.bounds.width {
            clipWidth = clipWindow.bounds.width
            canvasView.resetFrame()
        }
    }
    
    private func alertPhotoLimit() {
        displayAlert("写真（JPEG画像）は\(canvasView.photoItemLimiter)個まで\nしか取り込めません")
    }
    
    //-------------------------------------------------------------------------------------
    // Undo/Redo
    //-------------------------------------------------------------------------------------
    @IBAction func undoButton(_ sender: Any) {
        canvasView.undoSystem.undo()
        changeButtonsState()
    }
    
    @IBAction func redoButton(_ sender: Any) {
        canvasView.undoSystem.redo()
        changeButtonsState()
    }
    
    //-------------------------------------------------------------------------------------
    // 写真ボタン
    //-------------------------------------------------------------------------------------
    private var offsetPosition: Int?
    private var imagePicker: ImagePicker!
    
    @objc private func photoButton(_ sender: UIButton) {
        if offsetPosition != nil {  // 複数枚貼り付けが終わっていない
            return
        }
        penModeEnd()
        textEditingEnd()
        
        let photoMax = 16
        var count = canvasView.photoRemainCount > photoMax ?
                        photoMax : canvasView.photoRemainCount
        if count == 0 { count = 1 }
        imagePicker = ImagePicker()
        imagePicker.delegate = self
        imagePicker.present(controller: self, count: count)
    }
    
    func imagePickerFinish(image: UIImage?, type: ImageType, count: Int) { //　複数枚貼り付け
        if offsetPosition == nil {
            offsetPosition = count / 2
        } else {
            offsetPosition = offsetPosition! - 1
        }
        if let image = image {
            canvasView.homePosition()
            if type == .jpeg {
                if let photoImage = image.adjust() {
                    attachPhoto(image: photoImage, offset: offsetPosition!)
                }
            } else if type == .png {
                attachIllust(image: image, offset: offsetPosition!)
            }
        }
        if count == 1 {     // 最後の一枚の場合
            offsetPosition = nil
        }
    }
    
    func imagePickerFinish(image: UIImage?, type: ImageType) {          // 一枚貼り付け
        guard let image = image else { return }                         // キャンセルの場合
        
        if type == .jpeg {
            if !canvasView.checkPhotoLimit() {
                alertPhotoLimit()
                return
            }
            if let photoImage = image.adjust() {
                attachPhoto(image: photoImage, offset: 0)
            }
        } else if type == .png {
            attachIllust(image: image, offset: 0)
        }
    }
    
    private func attachText() {
        let rect = setupNewFrame(aspect: 1.0, diagonal: 0)
        let item = TextItemView(frame: rect, empty: true)
        canvasView.addItem(item)
    }
    
    private func attachIllust(image: UIImage, offset: Int) {
        let size = image.size
        let rect = setupNewFrame(aspect: size.height / size.width, diagonal: offset)
        let item = IllustItemView(frame: rect, image: image)
        canvasView.addItem(item)
    }
    
    private func attachPhoto(image: UIImage, offset: Int) {
        let size = image.size
        let rect = setupNewFrame(aspect: size.height / size.width, diagonal: offset)
        let item = createPhoto(frame: rect, image: image)
        canvasView.addItem(item)
    }
    
    private func createPhoto(frame: CGRect, image: UIImage) -> PhotoItemView {
        let cuttingTool = CuttingTool.create(type: .rect,
                                             size: image.size,
                                             originalSize: image.size)
        let item = PhotoItemView(frame: frame,
                                 photoImage: image,
                                 cuttingData: cuttingTool.getCuttingData(),
                                 image: image)
        return item
    }
    
    private func setupNewFrame(aspect: CGFloat, diagonal: Int = 0, vertical: Int = 0) -> CGRect {
        let offsetScale: CGFloat = 60
        let canvasSize: CGSize = canvasView.canvasSize
        let shortSide = canvasSize.width < canvasSize.height ? canvasSize.width : canvasSize.height
        let width, height: CGFloat
        if aspect < 1.0 {
            width = shortSide / 4.5
            height = width * aspect
        } else {
            height = shortSide / 4.5
            width = height / aspect
        }
        var rect = CGRect(origin: CGPoint(x: canvasSize.width / 2 - width / 2,
                                          y: canvasSize.height / 2 - height / 2),
                          size: CGSize(width: width, height: height))
        
        rect.center = clipWindow.convert(clipWindow.center, to: canvasView)
        rect.center.x += offsetScale * CGFloat(diagonal)
        rect.center.y += offsetScale * CGFloat(diagonal)
        rect.center.y += offsetScale * CGFloat(vertical)
        return rect
    }
    
    private func photoEdit(item: PhotoItemView) {
        cutterControllerPresent(photoItem: item)
    }
    
    private var editingIllustItem: IllustItemView?
    
    private func illustEdit(item: IllustItemView) {
        let photoItem = createPhoto(frame: item.frame, image: item.image!.fill(color: .white))
        editingIllustItem = item
        cutterControllerPresent(photoItem: photoItem)
    }
    
    private func cutterControllerPresent(photoItem: PhotoItemView?) {
        let storyboard: UIStoryboard = UIStoryboard(name: "PhotoCutter", bundle: nil)
        let cutterController: CutterViewController!
        cutterController = storyboard.instantiateViewController(withIdentifier: "CutterViewController") as? CutterViewController
        cutterController.delegate = self
        cutterController.modalPresentationStyle = .fullScreen
        cutterController.photoItemView = photoItem
        present(cutterController, animated: true, completion: nil)
    }
    /*
     override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
     panelControllerDidFinish() に移動
     }
     */
    //-------------------------------------------------------------------------------------
    // 手書き結合ボタン
    //-------------------------------------------------------------------------------------
    private var joinedPenItems: JoinedPenItems!
    
    private func joinItemsButton() {
        joinedPenItems = canvasView.joinPenItems()
        if let joinedPenItems = joinedPenItems {
            let storyboard = UIStoryboard(name: "PhotoCutter", bundle: nil)
            let confirmController: ConfirmViewController!
            confirmController = storyboard.instantiateViewController(withIdentifier: "ConfirmViewController")
                                as? ConfirmViewController
            confirmController.delegate = self
            confirmController.modalPresentationStyle = .formSheet
            //      confirmController.isModalInPresentation = true
            //      confirmController.presentationController?.delegate = self
            confirmController.image = joinedPenItems.image
            confirmController.message = "結合してもよろしいですか？"
            confirmController.border = true
            present(confirmController, animated: true, completion: nil)
        } else {
            displayAlert("複数の手書きアイテムを\n指定してください")
        }
    }
    //-------------------------------------------------------------------------------------
    // テキストボタン
    //-------------------------------------------------------------------------------------
    @objc private func textButton(_ sender: UIButton) {
        penModeEnd()
        attachText()
    }
    
    private func textEdit(item: TextItemView) {
        canvasView.simplifyHandle()
        textPanelView.startEditing(item: item, canvasView: canvasView)
        changeButtonsState()
    }
    
    private func textEditingEnd() {
        textPanelView.finishEditing()
        canvasView.completeHandle()
        changeButtonsState()
    }
    
    func textEditingFinish(item: TextItemView) {
        canvasView.modifyItem(item)
    }
    
    //-------------------------------------------------------------------------------------
    // menuボタン、図形ボタン、罫線ボタン、プリントボタン、戻るボタン、外部出力ボタン、共有ボタン
    //-------------------------------------------------------------------------------------
    @objc func menuButton(_ sender: UIButton) {
        if offsetPosition != nil {  // 複数枚貼り付けが終わっていない
            return
        }
        penModeEnd()
        textEditingEnd()
        
        let storyboard: UIStoryboard = self.storyboard!
        let menuController: MenuViewController!
        menuController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        menuController.delegate = self
        menuController.modalPresentationStyle = .popover // .pageSheet, .fullScreen, .formSheet, .popover
        //        menuController.isModalInPresentation = true
        
        menuController.preferredContentSize = CGSize(width: 160, height: 360)
        menuController.popoverPresentationController?.sourceView = menuButtonView
        menuController.popoverPresentationController?.sourceRect = menuButtonView.frame
        menuController.popoverPresentationController?.permittedArrowDirections = .down
        menuController.canvasView = canvasView  // 背景色変更のため
        present(menuController, animated: true, completion: nil)
    }
    
    @objc private func figureButton(_ sender: UIButton) {
        penModeEnd()
        textEditingEnd()
        figureControllerPresent(item: nil)
    }
    
    private func figureEdit(item: FigureItemView) {
        figureControllerPresent(item: item)
    }
    
    private func figureControllerPresent(item: FigureItemView?) {
        let storyboard: UIStoryboard = self.storyboard!
        let figureController: FigureViewController!
        figureController = storyboard.instantiateViewController(withIdentifier: "FigureViewController")
            as? FigureViewController
        figureController.delegate = self
        figureController.modalPresentationStyle = .popover // .pageSheet, .fullScreen, .formSheet, .popover
        figureController.isModalInPresentation = true
        
        figureController.preferredContentSize = CGSize(width: 540, height: 210)
        figureController.popoverPresentationController?.sourceView = figureButtonView
        figureController.popoverPresentationController?.sourceRect = figureButtonView.bounds
        figureController.popoverPresentationController?.permittedArrowDirections = .down
        
        if item != nil {
            figureController.figureItemView = item
        } else {
            figureController.targetRect = setupNewFrame(aspect: 1.0, vertical: -4)
        }
        figureController.canvasView = canvasView
        present(figureController, animated: true, completion: nil)
    }
    
    func panelControllerDidFinish(_ controller: UIViewController) {
        if let cutterController = controller as? CutterViewController {
            let photoItem = cutterController.photoItemView
            cutterController.dismiss(animated: true, completion: {
                if self.editingIllustItem == nil {
                    if let photoItem = photoItem {
                        self.canvasView.modifyItem(photoItem)
                    }
                } else {
                    let illustItem = self.editingIllustItem!
                    self.editingIllustItem = nil
                    if let photoItem = photoItem {
                        self.canvasView.exchangeItem(illustItem, for: photoItem)
                    }
                }
            })
        } else if let figureController = controller as? FigureViewController {
            let isOK = figureController.isOK
            let isNew = figureController.targetRect != nil
            let figureItemView = figureController.figureItemView
            figureController.dismiss(animated: true, completion: {
                if isOK {
                    if isNew {
                        self.canvasView.addItem(figureItemView!)
                    } else {
                        self.canvasView.modifyItem(figureItemView!)
                    }
                }
            })
        } else if let menuController = controller as? MenuViewController {
            let operation = menuController.operation
            menuController.dismiss(animated: true, completion: {
                switch operation {
                case .ruledLine:
                    self.ruledLineButton()
                case .print:
                    self.printButton()
                case .finish:
                    self.finishButton()
                case .fileExport:
                    self.fileExportButton()
                case .showWalkThrough:
                    self.showWalkThroughButton()
                case .shareItemImage:
                    self.shareButton(operation: .shareItemImage)
                case .sharePosterImage:
                    self.shareButton(operation: .sharePosterImage)
                default:
                    break
                }
            })
        } else if let confirmController = controller as? ConfirmViewController {
            let isOK = confirmController.isOK
            confirmController.dismiss(animated: true, completion: {
                if isOK {
                    self.canvasView.attachJoinedPenItems(self.joinedPenItems)
                }
                self.joinedPenItems = nil
            })
        }
    }
    
    private func ruledLineButton() {
        ruledLineSettingView.isHidden = false
        ruledLineSettingView.superview!.isUserInteractionEnabled = true
    }
    
    private func dpiSelector(dpi: CGFloat) {
        canvasChangeDpi(dpi)
    }
    
    private func createImage() -> UIImage? {
        canvasView.hideHandle()
        canvasView.hideMultiHandle()
        let ruledLineState = canvasView.hideRuledLine()
        let image = canvasView.createImage()
        canvasView.showHandle()
        canvasView.showMultiHandle()
        canvasView.restoreRuledLine(state: ruledLineState)
        return image
    }
    
    private func printButton() {
        if let image = createImage() {
            let url = projectDataStore.savePrintData(image: image)
            _ = PrintManager.printFunc(url: url, self)
        }
    }
    
    private func finishButton() {
        if let image = createImage() {
            _ = projectDataStore.savePrintData(image: image)
            projectDataStore.saveThumbnail(image: image, force: true)
        }
        
        canvasView.deleteHandle()
        canvasView.deleteMultiHandle()
        canvasView.deleteRuledLine()
        canvasView.saveData()
        canvasFinish()
    }
    
    private func fileExportButton() {
        if let image = createImage() {
            let url = projectDataStore.savePrintData(image: image)
            
            // iPadOS 14.0 で deprecated となった。 iPadOS 13.x 対応を行う場合は下記コードを利用しバージョン分岐の実装を行うこと
            // let documentPicker = UIDocumentPickerViewController(url: url, in: .exportToService)
            let documentPicker = UIDocumentPickerViewController(forExporting: [url], asCopy: true)
            documentPicker.delegate = self
            present(documentPicker, animated: true, completion: nil)
        }
    }
    
    private func showWalkThroughButton() {
        let walkThroughViewController = OnboardViewController()
        self.present(walkThroughViewController, animated: true, completion: nil)
    }
    
    private func shareButton(operation: MenuOperation) {
        func presentActivityView(_ datas: [Any]) {
            let rect = CGRect(x: 0,
                              y: clipWindow.bounds.height,
                              width: 0,
                              height: 0)
            let margins = UIEdgeInsets(top: 46, left: 14, bottom: 46, right: 14)
            let shareController = UIActivityViewController(activityItems: datas,
                                                           applicationActivities: nil)
            shareController.popoverPresentationController?.sourceView = clipWindow
            shareController.popoverPresentationController?.sourceRect = rect
            shareController.popoverPresentationController?.popoverLayoutMargins = margins
            shareController.popoverPresentationController?.permittedArrowDirections = []
            shareController.excludedActivityTypes = [
                .addToReadingList,
//              .airDrop,
                .assignToContact,
//              .copyToPasteboard,
//              .mail,
                .markupAsPDF,
//              .message,
                .openInIBooks,
                .postToFacebook,
                .postToFlickr,
                .postToTencentWeibo,
                .postToTwitter,
                .postToVimeo,
                .postToWeibo,
//              .print,
//              .saveToCameraRoll,
            ]
            self.present(shareController, animated: true, completion: nil)
        }
        
        switch operation {
        case .shareItemImage:
            if let image = canvasView.getItemImage() {
                let url = projectDataStore.savePngDataToTmp(image: image)
                let data = ShareActivityItemSource(url: url, text: "イラスト画像（PNG）")
                presentActivityView([data])
            }
        default: //.sharePosterImage:
            if let image = createImage() {
                let url = projectDataStore.savePrintData(image: image)
                let data = ShareActivityItemSource(url: url, text: "ポスター画像（JPEG）")
                presentActivityView([data])
                // [data, ２枚目はimageを直接指定する]
            }
        }
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        guard let presentationController = presentationController else {
            return
        }
        presentationController.delegate?.presentationControllerDidDismiss?(presentationController)
    }
    
    //-------------------------------------------------------------------------------------
    // 描画アイテムの操作ボタン
    //-------------------------------------------------------------------------------------
    @IBAction func editButton(_ sender: UIButton) {
        editButton()
    }
    
    private func editButton() {
        penModeEnd()
        textEditingEnd()
        let count = canvasView.selectedItemCount
        if count > 1 {
            if editButtonView.title(for: .normal) == joinButtonTitle {
                joinItemsButton()
            }
            return
        } else if count == 0 {
            return
        }
        
        let item = canvasView.selectedItem!
        if item.type == .photo {
            let photoItem = item as! PhotoItemView
            if photoItem.isEditable {
                photoEdit(item: photoItem)
            }
        } else if item.type == .illust {
            let illustItem = item as! IllustItemView
            if !canvasView.checkPhotoLimit() {
                displayAlert("PNG画像を編集するとJPEG画像に変わります。JPEG画像は\(canvasView.photoItemLimiter)個までしか取り込めません")
            } else {
                displayAlert("PNG画像を編集すると透明部分が白色に変わります。\n編集を続けますか？",
                             cancel: true, okHandler: { (action) in
                                self.illustEdit(item: illustItem)
                             })
            }
        } else if item.type == .pen {
            let penItem = item as! PenItemView
            if penItem.isEditable {
                penModeEdit(item: penItem)
            }
        } else if item.type == .figure {
            let figureItem = item as! FigureItemView
            figureEdit(item: figureItem)
        } else if item.type == .text {
            let textItem = item as! TextItemView
            textEdit(item: textItem)
        }
    }
    
    @IBAction func aboveButton(_ sender: Any) {
        penModeEnd()
        textEditingEnd()
        canvasView.bringItemsToFront()
    }
    
    @IBAction func belowButton(_ sender: Any) {
        penModeEnd()
        textEditingEnd()
        canvasView.sendItemsToBack()
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        penModeEnd()
        textEditingEnd()
        if canvasView.isUndoEnabled {
            canvasView.deleteItems()
            changeButtonsState()
        } else {
            displayAlert("削除してもよろしいですか？",
                         cancel: true, okHandler: { (action) in
                            self.canvasView.deleteItems()
                            self.changeButtonsState()
                         })
        }
    }
    
    @IBAction func copyButton(_ sender: Any) {
        penModeEnd()
        textEditingEnd()
        let count = canvasView.selectedItemCount
        if count > 1 {
            displayAlert("複数の描画アイテムを指定して\nコピーはできません")
            return
        } else if count == 0 {
            return
        }
        
        let item = canvasView.selectedItem!
        if item.type == .photo {
            if !canvasView.checkPhotoLimit() {
                alertPhotoLimit()
                return
            }
        }
        
        let x = item.center.x < copyOffset ? 0 : item.center.x - copyOffset
        let y = item.center.y < copyOffset ? 0 : item.center.y - copyOffset
        let newItem = item.copy(center: CGPoint(x: x, y: y))
        canvasView.addItem(newItem)
    }
    
    //-------------------------------------------------------------------------------------
    // ペン入力モードの処理
    //-------------------------------------------------------------------------------------
    @objc private func penSwitch(_ sender: UISwitch) {
        if sender.isOn {
            textEditingEnd()
            penModeStart()
        } else {
            penModeEnd()
        }
    }
    
    private func penModeStart() {
        canvasView.penModeStart()
    }
    
    private func penModeEnd() {
        penSwitchView.isOn = false
        canvasView.penModeEnd()
    }
    
    private func penModeEdit(item: PenItemView) {
        penSwitchView.isOn = true
        canvasView.penModeEdit(item: item)
    }
}

import LinkPresentation

class ShareActivityItemSource: NSObject, UIActivityItemSource {
    private let url: URL
    private let image: UIImage?
    private let text: String
    
    init(url: URL, text: String) {
        self.url = url
        self.image = nil
        self.text = text
        super.init()
    }
    
    init(url: URL, image: UIImage, text: String) {
        self.url = url
        self.image = image
        self.text = text
        super.init()
    }
    
    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        let linkMetadata = LPLinkMetadata()
        linkMetadata.title = text
        if image != nil {
            linkMetadata.iconProvider = NSItemProvider(object: image!)
        } else {
            linkMetadata.iconProvider = NSItemProvider(contentsOf: url)
        }
        return linkMetadata
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return url
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController,
                                itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return url
    }
}

protocol PanelControllerDelegate: AnyObject {
    func panelControllerDidFinish(_ controller: UIViewController)
}
