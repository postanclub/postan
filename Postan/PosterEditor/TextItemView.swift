//
//  TextItemView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2021/06/05.
//  Copyright © 2021 beginner. All rights reserved.
//

import UIKit

struct TypingData: Codable {
    let size: Int
    let color: Color
    let bold: Bool
    let underLine: Bool
    let kerning: Int
    let alignment: Int
    let lineSpacing: Int
}

struct TextItemData: Codable {
    let textDataId: UUID
    let textDataSize: UInt64
    let typingData: TypingData?
}

class TextItemView: DrawItemView, UIGestureRecognizerDelegate {
    override var isEditable: Bool { true }
    var textView: UITextView!
    var typingAttr: TypingAttributes?
    private var isEmpty = false
    private var isUpdated: Bool = false
    private var textDataId: UUID?
    private var textDataSize: UInt64!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, empty: Bool, withTextView: Bool = true) {
        super.init(frame: frame, type: .text)
        if withTextView {
            textView = createTextView(size: frame.size)
            isEmpty = empty
            if isEmpty {
                emptyText()
            }
            self.addSubview(textView)
        }
        isUpdated = true
    }
    
    override init(data: DrawItemData, dataStore: CanvasDataStore) {
        let textItemData = data.textItemData!
        textDataId = textItemData.textDataId
        textDataSize = textItemData.textDataSize
        if textItemData.typingData != nil {
            let align = NSTextAlignment(rawValue: textItemData.typingData!.alignment)!
            typingAttr = TypingAttributes(size: CGFloat(textItemData.typingData!.size),
                                          color: textItemData.typingData!.color.uiColor,
                                          bold: textItemData.typingData!.bold,
                                          underLine: textItemData.typingData!.underLine,
                                          kerning: CGFloat(textItemData.typingData!.kerning),
                                          alignment: align,
                                          lineSpacing: CGFloat(textItemData.typingData!.lineSpacing))
        }
        super.init(data: data, dataStore: dataStore)
        
        textView = createTextView(size: data.frame.size)
        if textDataSize > 0 {
            textView.attributedText = dataStore.loadTextData(id: textDataId!)
            textView.selectedRange = NSMakeRange(0, 0)
            textView.refresh()
            isEmpty = false
        } else {
            emptyText()
        }
        self.addSubview(textView)
    }
    
    private func createTextView(size: CGSize) -> UITextView {
        let textView = UITextView(frame: CGRect(origin: .zero, size: size))
        textView.contentMode = .scaleToFill
//        textView.deleteMargin()
        textView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        textView.backgroundColor = .clear
        textView.autocapitalizationType = .none
        textView.autocorrectionType = .no
        textView.isUserInteractionEnabled = false
        
        // UITextView上での２本指カーソル移動を無効化
        let panGesture = UIPanGestureRecognizer(target: self,
                                                action: #selector(panAction))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 2
        panGesture.maximumNumberOfTouches = 2
        textView.addGestureRecognizer(panGesture)
        
        return textView
    }
    
    @objc private func panAction(gesture: UIPanGestureRecognizer) {
    }
    
    private func emptyText() {
        isEmpty = true
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 0
        paragraphStyle.alignment = .left
        textView.typingAttributes = [
            .font: UIFont.systemFont(ofSize: 24),
            .kern: 0,
            .foregroundColor: UIColor.gray,
            .paragraphStyle: paragraphStyle,
        ]
        textView.text = "文字入力"
    }
    
    override func convAndSave(dataStore: CanvasDataStore) -> DrawItemData {
        self.dataStore = dataStore
        
        rotateToZero()
        if isUpdated {
            if textDataId != nil {
                dataStore.deleteTextData(id: textDataId!)
            }
            textDataId = UUID()
            isUpdated = false
        }
        let textData = isEmpty ? nil : textView.attributedText
        textDataSize = dataStore.saveTextData(textData: textData,
                                              id: textDataId!)
        
        var typingData: TypingData? = nil
        if typingAttr != nil && isEmpty {
            typingData = TypingData(
                size: Int(typingAttr!.size),
                color: Color(uiColor: typingAttr!.color),
                bold: typingAttr!.bold,
                underLine: typingAttr!.underLine,
                kerning: Int(typingAttr!.kerning),
                alignment: typingAttr!.alignment.rawValue,
                lineSpacing: Int(typingAttr!.lineSpacing))
        }
        let textItemData = TextItemData(textDataId: textDataId!,
                                        textDataSize: textDataSize,
                                        typingData: typingData)
        
        let data = DrawItemData(type: self.type,
                                frame: self.frame,
                                angle: self.angle,
                                textItemData: textItemData)
        rotateToCurrent()
        return data
    }
    
    override func checkFileData(uuid: UUID) -> Bool {
        return uuid == textDataId
    }
    
    override func deleteData() {
        super.deleteData()
        if textDataId != nil {
            self.dataStore!.deleteTextData(id: textDataId!)
            textDataId = nil
            textDataSize = nil
        }
        isUpdated = false
    }
    
    override func copy(center: CGPoint? = nil) -> TextItemView {
        let item = copy(withTextView: true)
        if center != nil {
            item.center = center!
        }
        return item
    }
    
    private func copy(withTextView: Bool) -> TextItemView {
        rotateToZero()
        let item = TextItemView(frame: frame, empty: isEmpty, withTextView: withTextView)
        rotateToCurrent()
        
        item.angle = self.angle
        item.rotateToCurrent()
        
        if withTextView {
            item.textView.attributedText = textView.attributedText
        }
        item.typingAttr = typingAttr
        item.isUpdated = true
        return item
    }
    
    override func copyForUndo() -> TextItemView {
        let item: TextItemView = self.copy()
        item.textDataId = self.textDataId
        item.textDataSize = self.textDataSize
        item.isUpdated = false
        return item
    }
    
    // 編集開始
    // textViewを新しいtextItemViewのsubViewに付け替える
    // textViewを自分と新しいtextItemView両方から参照して戻り値に設定
    func startEditing() -> TextItemView {
        let newTextItemView: TextItemView = copy(withTextView: false)
        textView.removeFromSuperview()
        newTextItemView.textView = textView
        newTextItemView.addSubview(textView)
        if isEmpty {
            textView.text = nil
            textView.attributedText = nil
        }
        
        textView.selectedRange = NSRange(location: 0, length: 0)
        textView.scrollRangeToVisible(textView.selectedRange)
        textView.isUserInteractionEnabled = true
        
        textView.becomeFirstResponder()
        return newTextItemView
    }
    
    // 編集終了
    // textViewを自分のsubViewに付けなおす
    func finishEditing() {
        textView.resignFirstResponder()
        textView.setContentOffset(.zero, animated: true)
        textView.isUserInteractionEnabled = false
        
        self.addSubview(textView)
        isUpdated = true
        if textView.text.count > 0 {
            isEmpty = false
        } else {
            emptyText()
        }
    }
}

extension UITextView {
    func deleteMargin() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
        layoutManager.usesFontLeading = false
    }
    
    // 以下に対応するために必要：もっと良い方法があると思われるが発見できていない
    // ・rtfファイルにセーブ・ロードすると行間がずれる
    // ・文字列を選択してサイズを変えると行間が自動で変わらない
    func refresh() {
        func refresh1() ->NSMutableAttributedString {
            var range = NSMakeRange(0, attributedText.length)
            var part = NSRange()
            let mutable = NSMutableAttributedString(attributedString: attributedText)
            while range.length > 0 {
                let attributes = attributedText.attributes(at: range.location,
                                                           longestEffectiveRange: &part,
                                                           in: range)
                if part.length == 0 {
                    continue
                }
                var font = attributes[.font] as! UIFont
                if font.fontDescriptor.symbolicTraits.contains(.traitBold) {
                    font = UIFont.systemFont(ofSize: font.pointSize)
                } else {
                    font = UIFont.boldSystemFont(ofSize: font.pointSize)
                }
                mutable.addAttribute(.font, value: font, range: part)
                range.length -= part.length
                range.location += part.length
            }
            return mutable
        }
        let range = selectedRange
        isScrollEnabled = false
        attributedText = refresh1()
        attributedText = refresh1()
        isScrollEnabled = true
        selectedRange = range
    }
}
