//
//  MultiHandleView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/10/11.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit

class MultiHandleView: UIView {
    var selectedItems: [DrawItemView] = []
    private let beginPoint: CGPoint
    private var endPoint: CGPoint?
    private var selectionRect: CGRect?
    private var selectedRect: CGRect?
    private let allItems: [DrawItemView]
    private var selectedItem: DrawItemView?
    private var isSelected: Bool = false
    private var isMoved: Bool = false
    private var rectLineWidth: CGFloat {1.0 / baseScale}
    private var rectMargin: CGFloat {3.0 / baseScale}
    private var baseScale: CGFloat!
    
    required init?(coder aDecoder: NSCoder) {
        beginPoint = CGPoint()
        allItems = []
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, scale: CGFloat,
         allItems: [DrawItemView], selectedItems: [DrawItemView]) {
        beginPoint = CGPoint()
        baseScale = scale
        self.allItems = allItems
        isSelected = true
        self.selectedItems = selectedItems
        super.init(frame: frame)
        selectedRect = createSelectedRect()
        self.isUserInteractionEnabled = false
        self.backgroundColor = .clear
    }
    
    init(frame: CGRect, begin: CGPoint, scale: CGFloat, allItems: [DrawItemView]) {
        beginPoint = begin
        baseScale = scale
        self.allItems = allItems
        super.init(frame: frame)
        self.isUserInteractionEnabled = false
        self.backgroundColor = .clear
    }
    
    func checkModified() -> Bool {
        return isMoved
    }
    
    func touchesBegan(item: DrawItemView) {
        if isSelected {
            selectedItem = item
            isMoved = false
        }
    }
    
    private func checkIntersects(rect: CGRect, item: DrawItemView) -> Bool {
        func isCollisionSide(_ r1: CGPoint, _ r2: CGPoint, _ p1: CGPoint, _ p2: CGPoint) -> Bool {
            let t1 = (r1.x - r2.x) * (p1.y - r1.y) + (r1.y - r2.y) * (r1.x - p1.x)
            let t2 = (r1.x - r2.x) * (p2.y - r1.y) + (r1.y - r2.y) * (r1.x - p2.x)
            if (t1 * t2 < 0) || (t1 == 0) || (t2 == 0) {
                return true
            } else {
                return false
            }
        }
        
        item.rotateToZero()
        let itemPoints = item.frame.points(angle: item.angle, center: item.center)
        item.rotateToCurrent()
        
        for point in itemPoints {
            if rect.contains(point) {
                return true
            }
        }
        
        let rectPoints = rect.points()
        for i in 0 ..< 4 {
            let toi = i < 3 ? i + 1 : 0
            for j in 0 ..< 4 {
                let toj = j < 3 ? j + 1 : 0
                let t1 = isCollisionSide(rectPoints[i], rectPoints[toi],
                                         itemPoints[j], itemPoints[toj])
                let t2 = isCollisionSide(itemPoints[j], itemPoints[toj],
                                         rectPoints[i], rectPoints[toi])
                if t1 && t2 {
                    return true
                }
            }
        }
        return false
    }
    
    func touchesMoved(new: CGPoint, old: CGPoint) {
        if !isSelected {
            endPoint = new
            let size = CGSize(width: new.x - beginPoint.x,
                              height: new.y - beginPoint.y)
            selectionRect = CGRect(origin: beginPoint, size: size)
            
            if allItems.count > 0 {
                selectedItems = []
                for item in allItems {
                    if checkIntersects(rect: selectionRect!, item: item) {
                        item.startStandOut()
                        selectedItems.append(item)
                    } else {
                        item.stopStandOut()
                    }
                }
            }
            setNeedsDisplay()
        } else {
            if selectedItems.firstIndex(of: selectedItem!) != nil {
                var deltaX = new.x - old.x
                var deltaY = new.y - old.y
                if selectedRect!.minX + deltaX < 0 {
                    deltaX = -selectedRect!.minX
                } else if selectedRect!.maxX + deltaX > bounds.width {
                    deltaX = bounds.width - selectedRect!.maxX
                }
                if selectedRect!.minY + deltaY < 0 {
                    deltaY = -selectedRect!.minY
                } else if selectedRect!.maxY + deltaY > bounds.height {
                    deltaY = bounds.height - selectedRect!.maxY
                }
                selectedRect!.center.x += deltaX
                selectedRect!.center.y += deltaY
                for item in selectedItems {
                    item.stopStandOut()
                    item.move(deltaX: deltaX, deltaY: deltaY)
                }
                isMoved = true
                setNeedsDisplay()
            }
        }
    }
    
    func touchesEnded() -> Int {
        if !isSelected {
            isSelected = true
        } else {
            if let selectedItem = selectedItem {
                self.selectedItem = nil
                if !isMoved {
                    let index = selectedItems.firstIndex(of: selectedItem)
                    if let index = index {
                        selectedItems.remove(at: index)
                    } else {
                        selectedItem.startStandOut()
                        selectedItems.append(selectedItem)
                        sortSelectedItems()
                    }
                }
            }
        }
        setNeedsDisplay()
        if selectedItems.count > 0 {
            selectedRect = createSelectedRect()
        }
        return selectedItems.count
    }
    
    private func createSelectedRect() -> CGRect {
        var minX = selectedItems[0].frame.center.x
        var maxX = minX
        var minY = selectedItems[0].frame.center.y
        var maxY = minY
        for item in selectedItems {
            minX = min(minX, item.frame.center.x)
            maxX = max(maxX, item.frame.center.x)
            minY = min(minY, item.frame.center.y)
            maxY = max(maxY, item.frame.center.y)
        }
        return CGRect(origin: CGPoint(x: minX, y: minY),
                      size: CGSize(width: maxX - minX,
                                   height: maxY - minY))
    }
    
    func changeScale(scale: CGFloat) {
        baseScale = scale
        setNeedsDisplay()
    }
    
    override func draw(_ unused: CGRect) {
        func rotatePath(item: DrawItemView, path: UIBezierPath) {
            path.apply(CGAffineTransform(translationX: item.center.x, y: item.center.y).inverted())
            path.apply(CGAffineTransform(rotationAngle: (item.angle * CGFloat.pi / 180.0)))
            path.apply(CGAffineTransform(translationX: item.center.x, y: item.center.y))
        }
        
        if !isSelected {
            if let rect = selectionRect {
                let path = UIBezierPath(rect: rect)
                UIColor.black.setStroke()
                UIColor.gray.withAlphaComponent(0.3).setFill()
                path.lineWidth = rectLineWidth
                path.stroke()
                path.fill()
            }
        }
        
        for item in selectedItems {
            var path: UIBezierPath
            item.rotateToZero()
            let rect1 = CGRect(x: item.frame.minX - rectMargin,
                               y: item.frame.minY - rectMargin,
                               width: item.frame.width + rectMargin * 2,
                               height: item.frame.height + rectMargin * 2)
            item.rotateToCurrent()
            let rect2 = CGRect(x: rect1.minX + rectLineWidth, y: rect1.minY + rectLineWidth,
                               width: rect1.width, height: rect1.height)
            
            UIColor.black.setStroke()
            path = UIBezierPath(rect: rect1)
            path.lineWidth = rectLineWidth
            rotatePath(item: item, path: path)
            path.stroke()
            
            UIColor.white.setStroke()
            path = UIBezierPath(rect: rect2)
            path.lineWidth = rectLineWidth
            rotatePath(item: item, path: path)
            path.stroke()
        }
    }
    
    private func sortSelectedItems() {
        var sortedItems: [DrawItemView] = []
        for item in allItems {
            for sItem in selectedItems {
                if item == sItem {
                    sortedItems.append(item)
                }
            }
        }
        selectedItems = sortedItems
    }
}
