//
//  CanvasView.swift
//  Postan
//
//  Created by Shigeo Suzuki on 2020/08/22.
//  Copyright © 2020 beginner. All rights reserved.
//

import UIKit
import PencilKit

var singleDirectionHandle: Bool = false

struct JoinedPenItems {
    var image: UIImage? { penItem.image }
    var penItems: [PenItemView]
    var penItem: PenItemView
    
    init(penItems: [PenItemView]) {
        self.penItems = penItems
        self.penItem = PenItemView(penItems: penItems)
    }
}

protocol CanvasViewDelegate: AnyObject {
    func statusDidChange()
    func itemDidDoubleTap(item: DrawItemView)
}

class CanvasView: UIImageView, UIGestureRecognizerDelegate,
                  PKCanvasViewDelegate, PKToolPickerObserver {
    var canvasSize: CGSize!
    var canvasDpi: CGFloat!
    var imageId: UUID?
    var imageSize: UInt64!
    var drawItems: [DrawItemView] = []
    let photoItemLimiter: Int = 30
    var photoRemainCount: Int { photoItemLimiter - photoItemCount }
    var undoSystem = UndoManager()
    var inPencilMode: Bool { pencilView != nil }
    var isUndoEnabled: Bool = false
    var paperSize: PaperSize
    weak var delegate: CanvasViewDelegate?
    
    private var maximumScale: CGFloat
    private var currentItem: DrawItemView?
    private var isTouchDisabled: Bool
    private let multiSelection: Bool = true
    private var photoItemCount: Int = 0
    private var dataStore: CanvasDataStore!
    private var currentScale: CGFloat!
    private var ruledLineView: RuledLineView!
    private var baseScale: CGFloat!
    private weak var baseView: UIView!
    private var handleView: HandleView?
    private var multiHandleView: MultiHandleView?
    
    required init?(coder aDecoder: NSCoder) {
        self.paperSize = PaperSize(rawValue: UserDefaults.standard.integer(forKey: "default_paper_size"))!
        self.maximumScale = 1.0
        self.isTouchDisabled = true
        super.init(coder: aDecoder)!
    }
    
    init(canvasSize: CGSize,
         canvasColor: UIColor,
         baseView: UIView,
         dataStore: CanvasDataStore) {
        self.canvasSize = canvasSize
        self.canvasDpi = CreateNewPosterController.fixedCanvasDpi
        self.baseView = baseView
        self.dataStore = dataStore
        self.isTouchDisabled = !UserDefaults.standard.bool(forKey: "touch_on_item")
        self.paperSize = PaperSize(rawValue: UserDefaults.standard.integer(forKey: "default_paper_size"))!
        self.maximumScale = CreateNewPosterController.maximumScale(paperSize: paperSize)
        /*
         UserDefaults.standard.register(defaults: ["multi_selection" : true])    // Root.plistのDefault値もYesに設定
         self.multiSelection = UserDefaults.standard.bool(forKey: "multi_selection")
         */
        super.init(frame: CGRect(origin: .zero, size: canvasSize))
        self.isUserInteractionEnabled = true
        self.autoresizesSubviews = true
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        self.backgroundColor = canvasColor
        
        singleDirectionHandle = UserDefaults.standard.bool(forKey: "single_direction_handle")
        
        let panGesture = UIPanGestureRecognizer(target: self,
                                                action: #selector(panAction))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 2
        panGesture.maximumNumberOfTouches = 2
        self.addGestureRecognizer(panGesture)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self,
                                                    action: #selector(pinchAction))
        pinchGesture.delegate = self
        self.addGestureRecognizer(pinchGesture)
        // Root.plistのDefault値も20に設定
        UserDefaults.standard.register(defaults: ["undo_redo_level" : 20])
        let undoLevel: Int = UserDefaults.standard.integer(forKey: "undo_redo_level")
        if undoLevel > 0 {
            undoSystem.levelsOfUndo = undoLevel
            isUndoEnabled = true
        } else {
            isUndoEnabled = false
        }
        undoSystem.groupsByEvent = false
        resetBaseUndoData()
    }
    
    private func canvasStatusChanged() {
        delegate?.statusDidChange()
    }
    
    func changePaperSize(_ paperSize: PaperSize) {
        self.paperSize = paperSize
        maximumScale = CreateNewPosterController.maximumScale(paperSize: paperSize)
        if currentScale > maximumScale {
            self.transform = CGAffineTransform(scaleX: maximumScale, y: maximumScale)
            changeScale(maximumScale)
        }
    }
    
    func changeScale(_ scale: CGFloat) {
        currentScale = scale
        if let handleView = handleView {
            handleView.changeScale(scale: currentScale)
        }
        if let boundView = boundView {
            boundView.changeScale(scale: currentScale)
        }
        if let multiHandleView = multiHandleView {
            multiHandleView.changeScale(scale: currentScale)
        }
        if let divideButtonView = divideButtonView {
            divideButtonView.frame.size = divideButtonSize
            placeDividButton()
        }
    }
    
    func changeDpi(_ dpi: CGFloat) {
        /*
         if canvasDpi == dpi {
         return
         }
         
         let scale = dpi / canvasDpi
         for drawItem in drawItems {
         drawItem.changeDpi(scale: scale)
         drawItem.setNeedsDisplay()
         }
         
         canvasDpi = dpi
         canvasSize = CGSize(width: canvasSize.width * scale,
         height: canvasSize.height * scale)
         resetFrame()
         
         redrawHandle()
         
         ruledLineView.changeDpi(canvasDpi)
         ruledLineView.changeSize(canvasSize)
         ruledLineView.setNeedsDisplay()
         */
    }
    
    func resetFrame() {
        self.transform = .identity
        self.frame.size = canvasSize
        let wScale = baseView.frame.size.width / canvasSize.width
        let hScale = baseView.frame.size.height / canvasSize.height
        baseScale = min(wScale, hScale)
        changeScale(baseScale)
        self.center = CGPoint(x: baseView.frame.size.width / 2,
                              y: baseView.frame.size.height / 2)
        self.transform = CGAffineTransform(scaleX: baseScale, y: baseScale)
    }
    
    func checkPhotoLimit() -> Bool {
        if photoItemCount < photoItemLimiter {
            return true
        } else {
            return false
        }
    }
    
    func setCanvasImage() {
        deleteHandle()
        deleteMultiHandle()
        let ruledLineState = hideRuledLine()
        beginUndoGrouping()
        if let image = createImage() {
            changeCanvasImage(image)
        }
        deleteAllItems()
        endUndoGrouping()
        restoreRuledLine(state: ruledLineState)
        canvasStatusChanged()
    }
    
    func changeCanvasImage(_ canvasImage: UIImage) {
        setupBaseUndoData()
        if imageId != nil {
            dataStore.deleteCanvasImage(id: imageId!)
        }
        image = canvasImage
        imageId = nil
        autoSave()
        registerUndoRedo(type: .canvas)
    }
    
    func deleteCanvasImage() {
        if imageId == nil {
            return
        }
        setupBaseUndoData()
        dataStore.deleteCanvasImage(id: imageId!)
        image = nil
        imageId = nil
        autoSave()
        registerUndoRedo(type: .canvas)
    }
    
    func changeCanvasColor(from: UIColor, to: UIColor) {
        backgroundColor = from
        setupBaseUndoData()
        backgroundColor = to
        autoSave()
        registerUndoRedo(type: .canvas)
    }
    
    func homePosition() {
        transform = CGAffineTransform(scaleX: baseScale,
                                      y: baseScale)
        changeScale(baseScale)
        center = CGPoint(x: baseView.bounds.size.width / 2,
                         y: baseView.bounds.size.height / 2)
    }
    
    func getItemImage() -> UIImage? {
        if let currentItem = currentItem {
            if currentItem.type == .pen || currentItem.type == .photo
                || currentItem.type == .illust {
                return currentItem.getImage()
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    //-------------------------------------------------------------------------------------
    // Undo/Redo
    //-------------------------------------------------------------------------------------
    enum UndoType {
        case add
        case delete
        case modify
        case toFront
        case toBack
        case canvas
    }
    
    struct UndoData {
        var drawItems: [DrawItemView]
        var selectedItems: [DrawItemView]
        var canvasColor: UIColor?
        var canvasImage: UIImage?
        var photoItemCount: Int
    }
    private var baseUndoData = UndoData(drawItems: [],
                                        selectedItems: [],
                                        canvasColor: nil,
                                        photoItemCount: 0)
    
    func resetUndoSystem() {
        undoSystem.removeAllActions()
    }
    
    private func resetBaseUndoData() {
        if !isUndoEnabled {
            return
        }
        baseUndoData.drawItems = []
        baseUndoData.selectedItems = []
        for item in drawItems {
            baseUndoData.drawItems.append(item.copyForUndo())
        }
        baseUndoData.canvasColor = backgroundColor
        baseUndoData.canvasImage = image
        baseUndoData.photoItemCount = 0
    }
    
    private func setupBaseUndoData() {
        if !isUndoEnabled {
            return
        }
        let selectedItems: [DrawItemView]
        if let currentItem = currentItem {
            selectedItems = [currentItem]
        } else {
            if let multiHandleView = multiHandleView {
                selectedItems = multiHandleView.selectedItems
            } else {
                selectedItems = []
            }
        }
        
        baseUndoData.selectedItems = []
        for item in selectedItems {
            let index = drawItems.firstIndex(of: item)!
            baseUndoData.selectedItems.append(baseUndoData.drawItems[index])
        }
        baseUndoData.canvasColor = backgroundColor
        baseUndoData.canvasImage = image
        baseUndoData.photoItemCount = photoItemCount
    }
    
    private func undoCanvasView(undoData: UndoData, redoData: UndoData) {
        for item in drawItems {
            item.removeFromSuperview()
        }
        drawItems = []
        dataStore.cleanGarbage(drawItems: undoData.drawItems)
        
        backgroundColor = undoData.canvasColor
        if image != undoData.canvasImage {
            if imageId != nil {
                dataStore.deleteCanvasImage(id: imageId!)
                imageId = nil
            }
            image = undoData.canvasImage
        }
        photoItemCount = undoData.photoItemCount
        
        var selectedItems: [DrawItemView] = []
        var index: Int = 0
        for item in undoData.drawItems {
            let copy: DrawItemView = item.copyForUndo()
            drawItems.append(copy)
            addSubview(copy)
            copy.setNeedsDisplay()
            
            if index < undoData.selectedItems.count
                && item == undoData.selectedItems[index] {
                selectedItems.append(copy)
                index += 1
            }
        }
        
        self.bringSubviewToFront(ruledLineView)
        
        if selectedItems.count == 1 {
            currentItem = selectedItems[0]
            changeHandle(item: currentItem!)
        } else if selectedItems.count > 1 {
            changeMultiHandle(items: selectedItems)
        } else {
            deleteHandle()
            deleteMultiHandle()
        }
        
        baseUndoData = undoData
        
        undoSystem.registerUndo(withTarget: self) {
            $0.undoCanvasView(undoData: redoData, redoData: undoData)
        }
        
        canvasStatusChanged()
        autoSave()
    }
    
    private func registerUndoRedo(type: UndoType, newItem: DrawItemView? = nil) {
        if !isUndoEnabled {
            return
        }
        if type == .modify {
            // .modifyだけアイテム配列の並びが変わらないので、呼び出し側でsetupBaseUndoData()
            // を呼ばずに済むようにここで呼び出す
            setupBaseUndoData()
        }
        let undoData = baseUndoData
        var redoData = baseUndoData
        
        switch type {
        case .add:
            let copy: DrawItemView = newItem!.copyForUndo()
            redoData.drawItems.append(copy)
            redoData.selectedItems = [copy]
        case .delete:
            for item in redoData.selectedItems {
                let index = redoData.drawItems.firstIndex(of: item)!
                redoData.drawItems.remove(at: index)
            }
            redoData.selectedItems = []
        case .modify:
            var selectedItems: [DrawItemView] = []
            for item in redoData.selectedItems {
                let index = redoData.drawItems.firstIndex(of: item)!
                redoData.drawItems.remove(at: index)
                let copy: DrawItemView = drawItems[index].copyForUndo()
                redoData.drawItems.insert(copy, at: index)
                selectedItems.append(copy)
            }
            redoData.selectedItems = selectedItems
        case .toFront:
            for item in redoData.selectedItems {
                let index = redoData.drawItems.firstIndex(of: item)!
                redoData.drawItems.remove(at: index)
                redoData.drawItems.append(item)
            }
        case .toBack:
            for item in redoData.selectedItems.reversed() {
                let index = redoData.drawItems.firstIndex(of: item)!
                redoData.drawItems.remove(at: index)
                redoData.drawItems.insert(item, at: 0)
            }
        case .canvas:
            redoData.canvasColor = backgroundColor
            redoData.canvasImage = image
        }
        redoData.photoItemCount = photoItemCount
        baseUndoData = redoData
        
        if !isBusyUndoGrouping {
            undoSystem.beginUndoGrouping()
        }
        undoSystem.registerUndo(withTarget: self) {
            $0.undoCanvasView(undoData: undoData, redoData: redoData)
        }
        if !isBusyUndoGrouping {
            undoSystem.endUndoGrouping()
        }
        canvasStatusChanged()
    }
    
    private var isBusyUndoGrouping: Bool = false
    
    private func beginUndoGrouping() {
        if isUndoEnabled {
            isBusyUndoGrouping = true
            undoSystem.beginUndoGrouping()
        }
    }
    
    private func endUndoGrouping() {
        if isUndoEnabled {
            isBusyUndoGrouping = false
            undoSystem.endUndoGrouping()
        }
    }
    
    //-------------------------------------------------------------------------------------
    // セーブ、ロード
    //-------------------------------------------------------------------------------------
    func autoSave() {
        saveData()
    }
    
    func saveData() {
        dataStore.saveData(canvasView: self)
    }
    
    func loadData() -> Bool {
        return dataStore.loadData(canvasView: self)
    }
    
    func loadDataDidFinish() {
        resetBaseUndoData()
    }
    
    func createImage() -> UIImage? {
        var image: UIImage? = nil
        
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, 0.0)
        if let context = UIGraphicsGetCurrentContext() {
            self.layer.render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        
        return image
    }
    
    //-------------------------------------------------------------------------------------
    // 罫線の操作
    //-------------------------------------------------------------------------------------
    func addRuledLine(_ ruledLine: RuledLineView) {
        ruledLineView = ruledLine
        self.addSubview(ruledLine)
    }
    
    func deleteRuledLine() {
        ruledLineView.removeFromSuperview()
        ruledLineView = nil
    }
    
    func hideRuledLine() -> Bool {
        let isHidden = ruledLineView.isHidden
        ruledLineView.isHidden = true
        return isHidden
    }
    
    func restoreRuledLine(state: Bool) {
        ruledLineView.isHidden = state
    }
    
    //-------------------------------------------------------------------------------------
    // 描画アイテムの操作
    //-------------------------------------------------------------------------------------
    func addItem(_ item: DrawItemView, atLoading: Bool = false) {
        if item.type == .photo {
            photoItemCount += 1
        }
        self.addSubview(item)
        drawItems.append(item)
        if !atLoading {
            self.bringSubviewToFront(ruledLineView)
            autoSave()
            registerUndoRedo(type: .add, newItem: item)
            
            currentItem = item
            changeHandle(item: currentItem!)
            canvasStatusChanged()
        }
    }
    
    func modifyItem(_ item: DrawItemView) {
        autoSave()
        registerUndoRedo(type: .modify)
        changeHandle(item: item)
    }
    
    var selectedItem: DrawItemView? {
        var item: DrawItemView?
        if currentItem != nil {
            item = currentItem
        } else {
            if let multiHandleView = multiHandleView {
                if multiHandleView.selectedItems.count == 1 {
                    item = multiHandleView.selectedItems.first
                }
            }
        }
        return item
    }
    
    var selectedItemCount: Int {
        var count: Int = 0
        if let multiHandleView = multiHandleView {
            count = multiHandleView.selectedItems.count
        } else if currentItem != nil {
            count = 1
        }
        return count
    }
    
    var selectedPenItemCount: Int {
        var count: Int = 0
        if let multiHandleView = multiHandleView {
            for item in multiHandleView.selectedItems {
                if item.type == .pen {
                    count += 1
                }
            }
        } else if currentItem != nil {
            if currentItem!.type == .pen {
                count = 1
            }
        }
        return count
    }
    
    func exchangeItem(_ item: DrawItemView, for newItem: DrawItemView) {
        setupBaseUndoData()
        beginUndoGrouping()
        deleteItem(item)
        registerUndoRedo(type: .delete)
        addItem(newItem)
        endUndoGrouping()
    }
    
    func deleteAllItems() {
        setupBaseUndoData()
        for item in drawItems {
            deleteItem(item)
        }
        deleteMultiHandle()
        deleteHandle()
        autoSave()
        registerUndoRedo(type: .delete)
    }
    
    func deleteItems() {
        setupBaseUndoData()
        if let multiHandleView = multiHandleView {
            for item in multiHandleView.selectedItems {
                deleteItem(item)
            }
            deleteMultiHandle()
        } else if let currentItem = currentItem {
            deleteItem(currentItem)
            deleteHandle()
        }
        autoSave()
        registerUndoRedo(type: .delete)
    }
    
    private func deleteItem(_ item: DrawItemView) {
        if item.type == .photo {
            photoItemCount -= 1
        }
        item.removeFromSuperview()
        item.deleteData()
        drawItems.remove(at: drawItems.firstIndex(of: item)!)
    }
    
    func bringItemsToFront() {
        setupBaseUndoData()
        if let multiHandleView = multiHandleView {
            for item in multiHandleView.selectedItems {
                self.bringSubviewToFront(item)
                drawItems.remove(at: drawItems.firstIndex(of: item)!)
                drawItems.append(item)
            }
            self.bringSubviewToFront(multiHandleView)
        } else if let item = currentItem {
            self.bringSubviewToFront(item)
            self.bringSubviewToFront(handleView!)
            drawItems.remove(at: drawItems.firstIndex(of: item)!)
            drawItems.append(item)
        } else {
            return
        }
        self.bringSubviewToFront(ruledLineView)
        autoSave()
        registerUndoRedo(type: .toFront)
    }
    
    func sendItemsToBack() {
        setupBaseUndoData()
        if let multiHandleView = multiHandleView {
            for item in multiHandleView.selectedItems.reversed() {
                self.sendSubviewToBack(item)
                drawItems.remove(at: drawItems.firstIndex(of: item)!)
                drawItems.insert(item, at: 0)
            }
        } else if let item = currentItem {
            self.sendSubviewToBack(item)
            drawItems.remove(at: drawItems.firstIndex(of: item)!)
            drawItems.insert(item, at: 0)
        }
        autoSave()
        registerUndoRedo(type: .toBack)
    }
    
    //-------------------------------------------------------------------------------------
    // ハンドルの操作
    //-------------------------------------------------------------------------------------
    func changeHandle(item: DrawItemView) {
        if let handleView = handleView {
            handleView.removeFromSuperview()
        }
        deleteMultiHandle()
        handleView = HandleView(frame: CGRect(origin: .zero, size: canvasSize),
                                item: item, scale: currentScale)
        self.addSubview(handleView!)
    }
    
    func changeMultiHandle(items: [DrawItemView]) {
        if let multiHandleView = multiHandleView {
            multiHandleView.removeFromSuperview()
        }
        deleteHandle()
        multiHandleView = MultiHandleView(frame: bounds,
                                          scale: currentScale,
                                          allItems: drawItems,
                                          selectedItems: items)
        self.addSubview(multiHandleView!)
    }
    
    func deleteHandle() {
        if let handleView = handleView {
            handleView.removeFromSuperview()
            self.handleView = nil
        }
        currentItem = nil
    }
    
    func redrawHandle() {
        if let handleView = handleView {
            handleView.redraw()
        }
    }
    
    func simplifyHandle() {
        if let handleView = handleView {
            handleView.isSimplified = true
            handleView.setNeedsDisplay()
        }
    }
    
    func completeHandle() {
        if let handleView = handleView {
            handleView.isSimplified = false
            handleView.setNeedsDisplay()
        }
    }
    
    func showHandle() {
        if let handleView = handleView {
            handleView.isHidden = false
            handleView.setNeedsDisplay()
        }
    }
    
    func hideHandle() {
        if let handleView = handleView {
            handleView.isHidden = true
        }
    }
    
    func getHandleOperation(point: CGPoint) -> HandleOperation {
        if let handleView = handleView {
            return handleView.operationType(point: point)
        } else {
            return HandleOperation(type: .noOperation)
        }
    }
    
    func deleteMultiHandle() {
        if let multiHandleView = multiHandleView {
            multiHandleView.removeFromSuperview()
            self.multiHandleView = nil
        }
    }
    
    func showMultiHandle() {
        if let multiHandleView = multiHandleView {
            multiHandleView.isHidden = false
        }
    }
    
    func hideMultiHandle() {
        if let multiHandleView = multiHandleView {
            multiHandleView.isHidden = true
        }
    }
    
    //-------------------------------------------------------------------------------------
    // ２本指タッチ操作
    //-------------------------------------------------------------------------------------
    private var pinchStartImageCenter: CGPoint!
    private var pinchCenter: CGPoint!
    
    @objc func panAction(gesture: UIPanGestureRecognizer) {
        if isRulerActive {
            return
        }
        if gesture.state == .began || gesture.state == .changed {
            canvasPan(delta: gesture.translation(in: baseView))
            gesture.setTranslation(CGPoint.zero, in: baseView)
        } else if gesture.state == .ended {
            singleTouchFinish()     // touchedEnded() が呼ばれない場合に必要
        }
    }
    
    private func canvasPan(delta: CGPoint) {
        let point: CGPoint = CGPoint(x: self.center.x + delta.x,
                                     y: self.center.y + delta.y)
        let x, y: CGFloat
        let baseWidth = baseView.frame.size.width
        let baseHeight = baseView.frame.size.height
        let halfWidth = (canvasSize.width / 2) * currentScale
        let halfHeight = (canvasSize.height / 2) * currentScale
        let marginX = baseWidth / 2
        let marginY = baseHeight / 2
        
        if point.x + halfWidth < marginX {
            x = -1 * (halfWidth - marginX)
        } else if halfWidth - (point.x - baseWidth) < marginX {
            x = baseWidth + (halfWidth - marginX)
        } else {
            x = point.x
        }
        if point.y + halfHeight < marginY {
            y = -1 * (halfHeight - marginY)
        } else if halfHeight - (point.y - baseHeight) < marginY {
            y = baseHeight + (halfHeight - marginY)
        } else {
            y = point.y
        }
        let deltaX = x - self.center.x
        let deltaY = y - self.center.y
        self.center.x = x
        self.center.y = y
        if pinchCenter != nil {
            pinchCenter.x += deltaX
            pinchCenter.y += deltaY
            pinchStartImageCenter.x += deltaX
            pinchStartImageCenter.y += deltaY
        }
    }
    
    @objc func pinchAction(gesture: UIPinchGestureRecognizer) {
        if isRulerActive {
            return
        }
        if gesture.state == .began {
            // ピンチを開始したときの画像の中心点を保存しておく
            pinchStartImageCenter = self.center
            
            // 指の中間点を保存しておく
            // UIGestureRecognizerState.Changedで毎回中心点とすると、
            // ピンチ状態で片方の指だけ動かしたときに中心点がずれておかしな位置でズームされるため
            pinchCenter = gesture.location(in: baseView)
            /*
             ２本指目の情報が無い場合があるらしいので以下のコードは上記に置き換えた
             let touchPoint1 = gesture.location(ofTouch: 0, in: baseView)
             let touchPoint2 = gesture.location(ofTouch: 1, in: baseView)
             pinchCenter = CGPoint(x: (touchPoint1.x + touchPoint2.x) / 2,
             y: (touchPoint1.y + touchPoint2.y) / 2)
             */
            
        } else if gesture.state == .changed {
            // 定規がピンチインで消えるため、.beganの処理が実行されてないケースに対応
            guard pinchStartImageCenter != nil else { return }
            
            let scale = gesture.scale // ピンチを開始してからの拡大率。差分ではない
            
            if scale * currentScale < maximumScale {
                // ピンチした位置を中心としてズーム（イン/アウト）するように、画像の中心位置をずらす
                let x = pinchStartImageCenter.x
                    - ((pinchCenter.x - pinchStartImageCenter.x) * scale - (pinchCenter.x - pinchStartImageCenter.x))
                let y = pinchStartImageCenter.y
                    - ((pinchCenter.y - pinchStartImageCenter.y) * scale - (pinchCenter.y - pinchStartImageCenter.y))
                
                self.center = CGPoint(x: x, y: y)
                self.transform = CGAffineTransform(scaleX: scale * currentScale, y: scale * currentScale)
            }
        } else if gesture.state == .ended {
            // 定規がピンチインで消えるため、.beganの処理が実行されてないケースに対応
            guard pinchStartImageCenter != nil else { return }
            
            var scale = sqrt(abs(self.transform.a * self.transform.d
                - self.transform.b * self.transform.c))
            
            var center: CGPoint! = nil
            if scale < baseScale {
                scale = baseScale
                center = CGPoint(x: self.baseView.frame.size.width / 2,
                                 y: self.baseView.frame.size.height / 2)
            }
            if center != nil {
                UIView.animate(withDuration: 0.2, delay: 0.0,
                               options: UIView.AnimationOptions.curveEaseOut,
                               animations: {() -> Void in
                                self.center = center
                                self.transform = CGAffineTransform(scaleX: scale,
                                                                   y: scale)
                }, completion: {(finished: Bool) -> Void in
                })
            }
            pinchStartImageCenter = nil
            pinchCenter = nil
            
            changeScale(scale)
            singleTouchFinish()     // touchedEnded() が呼ばれない場合に必要
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //-------------------------------------------------------------------------------------
    // １本指タッチ操作
    //-------------------------------------------------------------------------------------
    enum SingleTouchStatus {
        case standby
        case ready
        case active
    }
    private var handleOperation = HandleOperation(type: .noOperation)
    private var singleTouchTimer: Timer?
    private var singleTouchStatus: SingleTouchStatus = .standby
    private var isModified: Bool = false
    private var oldLocation: CGPoint = .zero
    private var touchedView: UIView?
    private var doubleTapPeriod: Bool = false
    private var doubleTapTimer: Timer?
    
    private func singleTouchCancel() {
        singleTouchTimer?.invalidate()
        singleTouchTimer = nil
        singleTouchStatus = .standby
    }
    
    private func singleTouchFinish() {
        var isDoubleTapped: Bool = false
        if singleTouchStatus == .ready {
            // Timerが発火する前にタッチが終了した場合はタップと判断する
            
            // ダブルタップかどうかの判断
            if doubleTapPeriod {
                // ダブルタップ判断期間内で同じアイテムだったらダブルタップと判断する
                if let currentItem = currentItem {
                    if touchedView == currentItem as UIView {
                        isDoubleTapped = true
                    }
                }
                doubleTapTimer?.invalidate()
                doubleTapTimer = nil
                doubleTapPeriod = false
            } else {
                // 一定期間ダブルタップとして判断できるようにする
                doubleTapPeriod = true
                doubleTapTimer = Timer.scheduledTimer(withTimeInterval: 0.35,
                                                      repeats: false,
                                                      block: {_ in
                                                        self.doubleTapPeriod = false
                                                        self.doubleTapTimer = nil
                                                      })
            }
            
            // タイマーをキャンセルし、タッチ操作を開始する必要がある
            singleTouchTimer?.invalidate()
            singleTouchTimer = nil
            _delayedTouchesBegan()
        }
        singleTouchStatus = .standby
        
        if let multiHandleView = multiHandleView {
            // 複数選択状態になっている場合
            let count = multiHandleView.touchesEnded()
            if count == 0 {
                // 一つも選択されていない場合
                deleteMultiHandle()
            } else if count == 1 {
                // 一つだけ選択された場合
                currentItem = selectedItem
                deleteMultiHandle()
                changeHandle(item: currentItem!)
            } else {
                // 複数選択された場合や、移動・階層移動された場合
                if multiHandleView.checkModified() {
                    autoSave()
                    registerUndoRedo(type: .modify)
                }
            }
            canvasStatusChanged()
            return
        }
        // ハンドルを描画アイテムに合わせて移動して再表示
        if handleOperation.type != .noOperation {
            handleOperation = HandleOperation(type: .noOperation)
            if isModified {
                isModified = false
                autoSave()
                registerUndoRedo(type: .modify)
            }
            completeHandle()
            if isDoubleTapped {
                delegate?.itemDidDoubleTap(item: currentItem!)
            }
        }
    }
    
    @objc private func delayedTouchesBegan(_ sender: Timer) {
        singleTouchTimer = nil
        _delayedTouchesBegan()
    }
    
    func _delayedTouchesBegan() {
        let point = oldLocation
        
        singleTouchStatus = .active
        
        if let multiHandleView = multiHandleView {
            // 複数選択状態になっている場合
            for item in drawItems {
                if touchedView == item as UIView {
                    multiHandleView.touchesBegan(item: item)
                    return
                }
            }
        } else {
            handleOperation = getHandleOperation(point: point)
            if handleOperation.type != .noOperation && handleOperation.type != .move {
                // 拡大、縮小、回転ハンドルをタッチした場合
                simplifyHandle()
                return
            }
            
            for item in drawItems {
                if touchedView == item as UIView {
                    if item != currentItem {
                        // これまでアイテムが選択されていない、または
                        // 別のアイテムが選択されていた場合
                        currentItem = item
                        changeHandle(item: item)
                        canvasStatusChanged()
                    }
                    // currentItemをタッチした場合はすでにmoveになっているはずだが
                    // 念のため設定する
                    handleOperation = HandleOperation(type: .move)
                    break
                }
            }
            if handleOperation.type == .move {
                // アイテムをタッチせずにハンドルmoveが検知される場合もある
                currentItem!.startStandOut()
                simplifyHandle()
                return
            }
            // 描画アイテムやハンドル以外をタッチした場合
            // ハンドルが表示されていたら削除する
            deleteHandle()
        }
        
        // 描画アイテムやハンドル以外をタッチした場合
        if multiSelection {
            // 複数選択の開始
            if let multiHandleView = multiHandleView {
                multiHandleView.removeFromSuperview()
            }
            multiHandleView = MultiHandleView(frame: bounds,
                                              begin: point,
                                              scale: currentScale,
                                              allItems: drawItems)
            self.addSubview(multiHandleView!)
        }
        
        canvasStatusChanged()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        oldLocation = touch.location(in: self)
        touchedView = touch.view
        
        // ピンチ、パン操作の開始をここで検知してキャンセル
        // UIPanGestureRecognizerの検知がちょっと遅れるため
        if event?.allTouches?.count == 2 {
            singleTouchCancel()
            return
        }
        
        // １本指操作をちょっと遅らせて起動する
        // ピンチ、パン操作の可能性があり、その場合アイテムの操作をキャンセルするため
        singleTouchStatus = .ready
        singleTouchTimer = Timer.scheduledTimer(timeInterval: 0.15,
                                                target: self,
                                                selector: #selector(delayedTouchesBegan(_:)),
                                                userInfo: nil,
                                                repeats: false)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        
        if isTouchDisabled && touch.type != .pencil {
            return
        }
        if singleTouchStatus != .active {
            return
        }
        
        let old = oldLocation
        let new = touch.location(in: self)
        oldLocation = new
        
        if let multiHandleView = multiHandleView {
            multiHandleView.touchesMoved(new: new, old: old)
            return
        }
        
        isModified = true
        if handleOperation.type == .move {
            currentItem!.stopStandOut()
            currentItem!.move(deltaX: new.x - old.x, deltaY: new.y - old.y,
                              bounds: canvasSize)
            redrawHandle()
        } else if (handleOperation.type == .scale)
                    || (handleOperation.type == .scaleX)
                    || (handleOperation.type == .scaleY) {
            if singleDirectionHandle {
                let direction = handleOperation.option as! Direction
                currentItem!.scale(direction: direction, point: new)
            } else {
                let scale: CGFloat = currentItem!.center.distance(new)
                    / currentItem!.center.distance(old)
                switch handleOperation.type {
                case .scale:
                    currentItem!.scale(x: scale, y: scale)
                case .scaleX:
                    currentItem!.scale(x: scale, y: 1.0)
                case .scaleY:
                    currentItem!.scale(x: 1.0, y: scale)
                default :
                    break
                }
            }
            redrawHandle()
        } else if handleOperation.type == .rotate {
            var x, y: CGFloat
            x = new.x - currentItem!.center.x
            y = new.y - currentItem!.center.y
            let newAngle = atan2(y, x) * 360 / (2 * CGFloat.pi)
            x = old.x - currentItem!.center.x
            y = old.y - currentItem!.center.y
            let oldAngle = atan2(y, x) * 360 / (2 * CGFloat.pi)
            currentItem!.rotate(angle: newAngle - oldAngle)
            redrawHandle()
        } else if handleOperation.type == .control {
            currentItem!.control(index: handleOperation.option as! Int, point: new)
        } else {
            /*
             if (touch.view! == canvasView) {    // キャンバスのパン操作
             let old = touch.previousLocation(in: self.superview)
             let new = touch.location(in: self.superview)
             canvasPan(delta: CGPoint(x: new.x - old.x, y: new.y - old.y))
             }
             */
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        singleTouchFinish()
    }
    
    //-------------------------------------------------------------------------------------
    // 手書きアイテムの結合
    //-------------------------------------------------------------------------------------
    func joinPenItems() -> JoinedPenItems? {
        if let multiHandleView = multiHandleView {
            var penItems: [PenItemView] = []
            for item in multiHandleView.selectedItems {
                if item.type == .pen {
                    penItems.append(item as! PenItemView)
                }
            }
            if penItems.count > 1 {
                return JoinedPenItems(penItems: penItems)
            }
        }
        return nil
    }
    
    func attachJoinedPenItems(_ joinedPenItems: JoinedPenItems) {
        // redoデータ作成時にペンアイテム以外を削除しないようにする
        multiHandleView!.selectedItems = joinedPenItems.penItems
        setupBaseUndoData()
        deleteMultiHandle()
        beginUndoGrouping()
        for item in joinedPenItems.penItems {
            deleteItem(item)
        }
        registerUndoRedo(type: .delete)
        
        let penItem = joinedPenItems.penItem
        addItem(penItem)
        endUndoGrouping()
    }
    
    //-------------------------------------------------------------------------------------
    // ペン入力モードの処理
    //-------------------------------------------------------------------------------------
    private var divideButtonSize: CGSize { CGSize(width: 32 / currentScale, height: 32 / currentScale) }
    private var pencilView: PKCanvasView!
    private var toolPicker: PKToolPicker?
    private var modifyingPenItem: PenItemView?
    private var boundView: BoundView?
    private var divideButtonView: UIButton!
    private var isAutoDivider: Bool = false
    private var isRulerActive: Bool {
        if let pencilView = pencilView {
            return pencilView.isRulerActive
        } else {
            return false
        }
    }
    
    private func createBoundView() {
        boundView = BoundView(frame: CGRect(origin: .zero, size: canvasSize),
                              item: currentItem!, scale: currentScale)
        boundView!.isHidden = true
        self.addSubview(boundView!)
        self.bringSubviewToFront(pencilView)
        
        divideButtonView = UIButton(frame: CGRect(origin: CGPoint.zero, size: divideButtonSize))
        divideButtonView.isHidden = true
        divideButtonView.addTarget(self, action: #selector(divideButton), for: .touchUpInside)
        divideButtonView.setImage(UIImage(named: "scissors"), for: .normal)
        divideButtonView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.6)
        divideButtonView.layer.cornerRadius = 10
        
        pencilView.addSubview(divideButtonView)
    }
    
    func penModeEdit(item: PenItemView) {
        penModeStart()
        
        modifyingPenItem = item
        currentItem = item  // penModeStart()でnilになるので再設定
        createBoundView()
        canvasStatusChanged()
        
        pencilView.drawing = item.resumeDrawing()
        // -> canvasViewDrawingDidChange()
    }
    
    func penModeStart() {
        deleteHandle()
        deleteMultiHandle()
//      isAutoDivider = UserDefaults.standard.bool(forKey: "auto_divider")
        
        pencilView = PKCanvasView(frame: CGRect(origin: CGPoint.zero, size: self.bounds.size))
        pencilView.backgroundColor = .clear
        pencilView.isOpaque = false
        pencilView.drawingPolicy = .default
        self.addSubview(pencilView)
        pencilView.tool = PKInkingTool(.pen, color: .black, width: 30)
        pencilView.delegate = self
        
        toolPicker = PKToolPicker()
        toolPicker!.showsDrawingPolicyControls = true
        if let toolPicker = toolPicker {
            toolPicker.addObserver(pencilView)
            toolPicker.addObserver(self)
            toolPicker.setVisible(true, forFirstResponder: pencilView)
            pencilView.becomeFirstResponder()
        }
        
        canvasStatusChanged()
        ruledLineView.penModeStart()
    }
    
    func penModeEnd() {
        if pencilView == nil {
            return
        }
        
        let rect = pencilView.drawing.bounds
        if rect.size.width != 0.0 {
            let penItem = currentItem as! PenItemView
            penItem.finishDrawing(drawing: pencilView.drawing)
            if penItem == modifyingPenItem {
                modifyItem(penItem)
            } else {
                addItem(penItem)
            }
            modifyingPenItem = nil
            
            boundView!.removeFromSuperview()
            if let timer = boundView!.timer {
                timer.invalidate()
            }
            boundView = nil
            divideButtonView.removeFromSuperview()
            divideButtonView = nil
        }
        if let toolPicker = toolPicker {
            toolPicker.removeObserver(pencilView)
        }
        pencilView.removeFromSuperview()
        pencilView = nil
        
        canvasStatusChanged()
        ruledLineView.penModeEnd()
    }
    
    @objc func divideButton(_ sender: UIButton) {
        divideDrawing()
    }
    
    func divideDrawing(drawing: PKDrawing? = nil) {
        let rect = pencilView.drawing.bounds
        if rect.size.width != 0.0 {
            let penItem = currentItem as! PenItemView
            penItem.finishDrawing(drawing: pencilView.drawing)
            if penItem == modifyingPenItem {
                modifyItem(penItem)
            } else {
                addItem(penItem)
                self.bringSubviewToFront(pencilView)
            }
            modifyingPenItem = nil
            deleteHandle()  // addItem(), modifyItem()で表示されるハンドルを削除
            canvasStatusChanged()
            
            boundView!.removeFromSuperview()
            boundView = nil
            divideButtonView.removeFromSuperview()
            divideButtonView = nil
            if let drawing = drawing {
                pencilView.drawing = drawing        // call canvasViewDrawingDidChange()
            } else {
                pencilView.drawing = PKDrawing()    // call canvasViewDrawingDidChange()
            }
        }
    }
    
    private func checkIndependence(_ stroke: CGRect, _ new: CGRect, _ old:CGRect) -> Bool {
        if old.intersects(stroke) { return false }
        var dX: CGFloat = 0
        var dY: CGFloat = 0
        if stroke.maxX < old.minX {
            dX = old.minX - stroke.maxX
        } else if stroke.minX > old.maxX {
            dX = stroke.minX - old.maxX
        }
        if stroke.maxY < old.minY {
            dY = old.minY - stroke.maxY
        } else if stroke.minY > old.maxY {
            dY = stroke.minY - old.maxY
        }
        let distance = (dX * dX + dY * dY).squareRoot()
        if distance > 400 {
            return true
        }
        let oldSize = (old.width * old.width + old.height * old.height).squareRoot()
        if (distance > 100) && (distance > oldSize * 0.8) {
            return true
        }
        return false
    }
    
    private var ignoreRecursiveCall = false
    
    func placeDividButton() {
        let rect: CGRect = boundView!.boundRect
        divideButtonView.frame.origin = CGPoint(x: rect.maxX + 10, y: rect.maxY + 5)
        
        if divideButtonView.frame.maxX > canvasSize.width {
            divideButtonView.frame.origin = CGPoint(x: canvasSize.width - divideButtonSize.width,
                                                    y: rect.maxY + 5)
        }
        if divideButtonView.frame.maxY > canvasSize.height {
            divideButtonView.frame.origin = CGPoint(x: rect.maxX + 10,
                                                    y: canvasSize.height - divideButtonSize.height)
            if divideButtonView.frame.maxX > canvasSize.width {
                divideButtonView.frame.origin = CGPoint(x: canvasSize.width - divideButtonSize.width,
                                                        y: rect.minY - divideButtonSize.height - 5)
            }
        }
    }
    
    func canvasViewDrawingDidChange(_ pkView: PKCanvasView) {
        if ignoreRecursiveCall {
            ignoreRecursiveCall = false
            return
        }
        let rect = pencilView.drawing.bounds
        if rect.size.width != 0.0 {
            if currentItem == nil {
                currentItem = PenItemView(frame: rect)
                
                createBoundView()
                canvasStatusChanged()
            } else {
                if isAutoDivider, #available(iOS 14.0, *) {
                    let bounds = pencilView.drawing.strokes.last!.renderBounds
                    if checkIndependence(bounds, rect, currentItem!.frame) {
                        ignoreRecursiveCall = true
                        let stroke = pencilView.drawing.strokes.removeLast()    // call canvasViewDrawingDidChange()
                        divideDrawing(drawing: PKDrawing(strokes: [stroke]))    // call canvasViewDrawingDidChange()
                        return
                    }
                }
                currentItem!.frame = rect
                boundView!.redraw()
            }
            placeDividButton()
            
            if let timer = boundView!.timer {
                timer.invalidate()
            }
            boundView!.timer = Timer.scheduledTimer(timeInterval: 0.35,
                                                    target: self,
                                                    selector: #selector(delayedBoundView),
                                                    userInfo: nil,
                                                    repeats: false)
            
        } else {            // undoか消しゴムで全部消した場合
            if let currentItem = self.currentItem {
                if currentItem == modifyingPenItem {
                    setupBaseUndoData()
                    deleteItem(currentItem)
                    registerUndoRedo(type: .delete)
                }
                modifyingPenItem = nil
                self.currentItem = nil
                canvasStatusChanged()
            }
            if let boundView = self.boundView {
                boundView.removeFromSuperview()
                if let timer = boundView.timer {
                    timer.invalidate()
                }
                self.boundView = nil
                divideButtonView.removeFromSuperview()
                divideButtonView = nil
            }
        }
    }
    
    @objc func delayedBoundView() {
        // boundViewがnilのことはありえないが念のため
        if let boundView = self.boundView {
            boundView.isHidden = false
            divideButtonView.isHidden = false
            boundView.timer = nil
        }
    }
    
    // ピクセル消しゴムの場合呼ばれないことがある：処理が重いため？
    // その場合、タイマーがここでキャンセルされないので、
    // canvasViewDrawingDidChange() の中でもキャンセルする処理を追加した
    func canvasViewDidBeginUsingTool(_ pkView: PKCanvasView) {
        if let boundView = self.boundView {
            boundView.isHidden = true
            divideButtonView.isHidden = true
            if let timer = boundView.timer {
                timer.invalidate()
                boundView.timer = nil
            }
        }
    }
    /*
     func canvasViewDidFinishRendering(_ pkView: PKCanvasView) {
     }
     func canvasViewDidEndUsingTool(_ pkView: PKCanvasView) {
     }
     */
}
